export const reverseObject = (ratings) => {
	var newObject = {};
	var keys = [];

	for (var k in ratings){
    	if (ratings.hasOwnProperty(k)) {
        	keys.push(
    			{
    				'rating':k,
    				'number':ratings[k]
    			}
        	);
        }
    }
    newObject = Object.assign({}, keys.reverse());

	return newObject;
}