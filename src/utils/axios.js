const Axios = require('axios');
Axios.defaults.headers.common = {
    'Content-Type': 'multipart/form-data;'
};

export default Axios;