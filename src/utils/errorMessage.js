export const errorConvert = (errorData) => {
    let error_converted = [];

    for (var key in errorData) {
        if (errorData[key] === 'ERROR_INVALID_LOGIN') {
            error_converted.push('Email or Password Invalid');
        }
        if(errorData[key] === 'ERROR_REQUIRED') {
            error_converted.push(key + ' field required');
        }
        if(errorData[key] === 'ERROR_EMAIL_FORMAT') {
            error_converted.push('email invalid format');
        }
        if(errorData[key] === 'ERROR_INVALID_MATCHES') {
            error_converted.push(key + ' field invalid match');
        }
        if(errorData[key] === 'ERROR_INVALID_SELECTION') {
            error_converted.push(key + ' field required');
        }
        if(errorData[key] === 'ERROR_INVALID_PRESCRIPTION') {
            error_converted.push('Prescription required');
        }
        if(errorData[key] === 'INVALID_CONFIRM_PASSWORD') {
            error_converted.push(key + ' field invalid match');
        }
        if(errorData[key] === '"SHIPPING_ADDRESS_EMPTY"') {
            error_converted.push('Shipping address empty');
        }
        if(errorData[key] === '"BILLING_ADDRESS_EMPTY"') {
            error_converted.push('Billing address empty');
        }
        if(errorData[key] === 'INVALID_POSTCODE') {
            error_converted.push('Invalid postcode for this state');
        }
        if(errorData[key] === 'INVALID_PASSWORD') {
            error_converted.push(key + ' invalid');
        }
        if(errorData[key] === 'INVALID_STAR_RATING') {
            error_converted.push('Invalid star rating');
        }
        if(errorData[key] === 'ERROR_INVALID_PHONE_NUMBER') {
            error_converted.push('Invalid phone number');
        }
        if(errorData[key] === 'ERROR_INVALID_IC') {
            error_converted.push('Invalid IC number');
        }
        if(errorData[key] === 'ERROR_INVALID_PASSWORD') {
            error_converted.push(key + ' invalid');
        }
        if(errorData[key] === 'ERROR_DOB_NOT_MATCH_IC') {
            error_converted.push('DOB and IC date do not match');
        }
        if(errorData[key] === 'ERROR_NOT_UNIQUE') {
            error_converted.push('Email been registered');
        }
    }

    return error_converted;
}
