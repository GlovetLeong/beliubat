export const setSession = (session) => {
    localStorage.setItem('patient-session', session);
}

export const getSession = () => {
    return localStorage.getItem('patient-session');
}

export const checkSession = () => {
    if(localStorage.getItem('patient-session')) {
        return true
    } 
    else {
        return false;
    }
}

export const destorySession = () => {
    localStorage.removeItem('patient-session');
    return false;
}

