import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as sampleAction from '../actions/sampleAction';

import Sample from '../../views/sample';

function mapStateToProps(state) {
    return {
        message: state.sampleReducer.message,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(sampleAction, dispatch);
}

const SampleContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Sample);

export default SampleContainer;
