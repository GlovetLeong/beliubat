import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as needDoctorAction from '../actions/needDoctorAction';
import needDoctorModal from '../../views/needDoctor/needDoctor-modal';

function mapStateToProps(state) {
    return {
        needDoctorStatus: state.needDoctorReducer.needDoctorStatus,
        needDoctorError: state.needDoctorReducer.needDoctorError,
        needDoctorMessage: state.needDoctorReducer.needDoctorMessage,

        symptomsStatus: state.needDoctorReducer.symptomsStatus,
        symptomsError: state.needDoctorReducer.symptomsError,
        symptomsMessage: state.needDoctorReducer.symptomsMessage,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(needDoctorAction, dispatch);
}

const NeedDoctorContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(needDoctorModal);

export default NeedDoctorContainer;
