import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as prescriptionRecordAction from '../actions/prescriptionRecordAction';
import prescriptionRecordMap from '../../views/prescriptionRecord/prescriptionRecord-map';

function mapStateToProps(state) {
    return {
        prescriptionRecordList: state.prescriptionRecordReducer.prescriptionRecordList,
        prescriptionRecordStatus: state.prescriptionRecordReducer.prescriptionRecordStatus,
        prescriptionRecordLoading: state.prescriptionRecordReducer.prescriptionRecordLoading,
        prescriptionRecordError: state.prescriptionRecordReducer.prescriptionRecordError,
        prescriptionRecordMessage: state.prescriptionRecordReducer.prescriptionRecordMessage,

        prescriptionRecordDetailList: state.prescriptionRecordReducer.prescriptionRecordDetailList,
        prescriptionRecordDetailStatus: state.prescriptionRecordReducer.prescriptionRecordDetailStatus,
        prescriptionRecordDetailLoading: state.prescriptionRecordReducer.prescriptionRecordDetailLoading,
        prescriptionRecordDetailError: state.prescriptionRecordReducer.prescriptionRecordDetailError,
        prescriptionRecordDetailMessage: state.prescriptionRecordReducer.prescriptionRecordDetailMessage,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(prescriptionRecordAction, dispatch);
}

const PrescriptionRecordContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(prescriptionRecordMap);

export default PrescriptionRecordContainer;
