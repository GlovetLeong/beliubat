import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as itemAction from '../actions/itemAction';
import ItemMapHorizontal from '../../views/item/item-map-horizontal';

function mapStateToProps(state) {
    return {
        hotItemListHorizontal: state.hotItemListHorizontalReducer.hotItemListHorizontal,
        hotItemListHorizontalStatus: state.hotItemListHorizontalReducer.hotItemListHorizontalStatus,
        hotItemListHorizontalLoading: state.hotItemListHorizontalReducer.hotItemListHorizontalLoading,
        hotItemListHorizontalError: state.hotItemListHorizontalReducer.hotItemListHorizontalError,
        hotItemListHorizontalMessage: state.hotItemListHorizontalReducer.hotItemListHorizontalMessage,

        itemDetail: state.itemListReducer.itemDetail,
        itemDetailStatus: state.itemListReducer.itemDetailStatus,
        itemDetailError: state.itemListReducer.itemDetailError,
        itemDetailMessage: state.itemListReducer.itemDetailMessage,

        itemReviewDetail: state.itemListReducer.itemReviewDetail,
        itemReviewStatus: state.itemListReducer.itemReviewStatus,
        itemReviewError: state.itemListReducer.itemReviewError,
        itemReviewMessage: state.itemListReducer.itemReviewMessage,

        itemReviewCreate: state.itemListReducer.itemReviewCreate,
        itemReviewCreateStatus: state.itemListReducer.itemReviewCreateStatus,
        itemReviewCreateError: state.itemListReducer.itemReviewCreateError,
        itemReviewCreateMessage: state.itemListReducer.itemReviewCreateMessage,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(itemAction, dispatch);
}

const HotItemListHorizontalContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ItemMapHorizontal);

export default HotItemListHorizontalContainer;
