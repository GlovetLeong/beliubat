import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as cartAction from '../actions/cartAction';

import CartList from '../../views/cart-2/cart-list';


function mapStateToProps(state) {
    return {
        cartList: state.cartReducer.cartList,
        cartStatus: state.cartReducer.cartStatus,
        cartLoading: state.cartReducer.cartLoading,
        cartError: state.cartReducer.cartError,
        cartMessage: state.cartReducer.cartMessage,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(cartAction, dispatch);
}

const CartListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CartList);

export default CartListContainer;
