import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as uploadPrescriptionAction from '../actions/uploadPrescriptionAction';
import uploadPrescriptionModal from '../../views/uploadPrescription/uploadPrescription-modal';

function mapStateToProps(state) {
    return {
        uploadPrescriptionStatus: state.uploadPrescriptionReducer.uploadPrescriptionStatus,
        uploadPrescriptionError: state.uploadPrescriptionReducer.uploadPrescriptionError,
        uploadPrescriptionMessage: state.uploadPrescriptionReducer.uploadPrescriptionMessage,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(uploadPrescriptionAction, dispatch);
}

const UploadPrescriptionContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(uploadPrescriptionModal);

export default UploadPrescriptionContainer;
