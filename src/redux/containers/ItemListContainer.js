import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as itemAction from '../actions/itemAction';
import ItemMap from '../../views/item/item-map';

function mapStateToProps(state) {
    return {
        itemList: state.itemListReducer.itemList,
        itemListStatus: state.itemListReducer.itemListStatus,
        itemListLoading: state.itemListReducer.itemListLoading,
        itemListError: state.itemListReducer.itemListError,
        itemListMessage: state.itemListReducer.itemListMessage,

        itemDetail: state.itemListReducer.itemDetail,
        itemDetailStatus: state.itemListReducer.itemDetailStatus,
        itemDetailError: state.itemListReducer.itemDetailError,
        itemDetailMessage: state.itemListReducer.itemDetailMessage,

        itemReviewDetail: state.itemListReducer.itemReviewDetail,
        itemReviewStatus: state.itemListReducer.itemReviewStatus,
        itemReviewError: state.itemListReducer.itemReviewError,
        itemReviewMessage: state.itemListReducer.itemReviewMessage,

        itemReviewCreate: state.itemListReducer.itemReviewCreate,
        itemReviewCreateStatus: state.itemListReducer.itemReviewCreateStatus,
        itemReviewCreateError: state.itemListReducer.itemReviewCreateError,
        itemReviewCreateMessage: state.itemListReducer.itemReviewCreateMessage,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(itemAction, dispatch);
}

const ItemListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ItemMap);

export default ItemListContainer;
