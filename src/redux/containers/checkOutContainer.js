import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as checkOutAction from '../actions/checkOutAction';
import CheckOutInfo from '../../views/checkOut-2/checkOut-info';

function mapStateToProps(state) {
    return {
    	stateList: state.checkOutReducer.stateList,
        stateStatus: state.checkOutReducer.stateStatus,
        stateError: state.checkOutReducer.stateError,
        stateMessage: state.checkOutReducer.stateMessage,

        checkOutList: state.checkOutReducer.checkOutList,
        checkOutStatus: state.checkOutReducer.checkOutStatus,
        checkOutLoading: state.checkOutReducer.checkOutLoading,
        checkOutError: state.checkOutReducer.checkOutError,
        checkOutMessage: state.checkOutReducer.checkOutMessage,

        proceedList: state.checkOutReducer.proceedList,
        proceedStatus: state.checkOutReducer.proceedStatus,
        proceedError: state.checkOutReducer.proceedError,
        proceedMessage: state.checkOutReducer.proceedMessage,

        addressStatus: state.checkOutReducer.addressStatus,

        couponStatus: state.checkOutReducer.couponStatus,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(checkOutAction, dispatch);
}

const CheckOutContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CheckOutInfo);

export default CheckOutContainer;
