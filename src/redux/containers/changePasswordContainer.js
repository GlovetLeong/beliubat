import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as changePasswordAction from '../actions/changePasswordAction';
import ChangePasswordModal from '../../views/changePassword/changePassword-modal';

function mapStateToProps(state) {
    return {
        changePasswordStatus: state.changePasswordReducer.changePasswordStatus,
        changePasswordError: state.changePasswordReducer.changePasswordError,
        changePasswordMessage: state.changePasswordReducer.changePasswordMessage,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(changePasswordAction, dispatch);
}

const ChangePasswordContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ChangePasswordModal);

export default ChangePasswordContainer;
