import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as repeatMedicalAction from '../actions/repeatMedicalAction';
import repeatMedicalMap from '../../views/repeatMedical/repeatMedical-map';

function mapStateToProps(state) {
    return {
        repeatMedicalList: state.repeatMedicalReducer.repeatMedicalList,
        repeatMedicalStatus: state.repeatMedicalReducer.repeatMedicalStatus,
        repeatMedicalLoading: state.repeatMedicalReducer.repeatMedicalLoading,
        repeatMedicalError: state.repeatMedicalReducer.repeatMedicalError,
        repeatMedicalMessage: state.repeatMedicalReducer.repeatMedicalMessage,

        itemDetail: state.itemListReducer.itemDetail,
        itemDetailStatus: state.itemListReducer.itemDetailStatus,
        itemDetailError: state.itemListReducer.itemDetailError,
        itemDetailMessage: state.itemListReducer.itemDetailMessage,

        itemReviewDetail: state.itemListReducer.itemReviewDetail,
        itemReviewStatus: state.itemListReducer.itemReviewStatus,
        itemReviewError: state.itemListReducer.itemReviewError,
        itemReviewMessage: state.itemListReducer.itemReviewMessage,

        itemReviewCreate: state.itemListReducer.itemReviewCreate,
        itemReviewCreateStatus: state.itemListReducer.itemReviewCreateStatus,
        itemReviewCreateError: state.itemListReducer.itemReviewCreateError,
        itemReviewCreateMessage: state.itemListReducer.itemReviewCreateMessage,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(repeatMedicalAction, dispatch);
}

const repeatMedicalContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(repeatMedicalMap);

export default repeatMedicalContainer;
