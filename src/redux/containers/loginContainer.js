import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as loginAction from '../actions/loginAction';

import LoginModal from '../../views/login/login-modal';

function mapStateToProps(state) {
    return {
        loginStatus: state.loginReducer.loginStatus,
        loginError: state.loginReducer.loginError,
        loginMessage: state.loginReducer.loginMessage,
        patientSession: state.loginReducer.patientSession,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(loginAction, dispatch);
}

const LoginContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginModal);

export default LoginContainer;
