import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as wrapperAction from '../actions/wrapperAction';

import Default from '../../views/app/default';

function mapStateToProps(state) {
    return {
        patientSession: state.wrapperReducer.patientSession,
        patientSessionStatus: state.wrapperReducer.patientSessionStatus,

        totalCart:  state.wrapperReducer.totalCart,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(wrapperAction, dispatch);
}

const WrapperContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Default);

export default WrapperContainer;
