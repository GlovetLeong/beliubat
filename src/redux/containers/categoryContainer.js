import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as categoryAction from '../actions/categoryAction';
import CategoryMap from '../../views/category/category-map';

function mapStateToProps(state) {
    return {
        categoryList: state.categoryReducer.categoryList,
        categoryStatus: state.categoryReducer.categoryStatus,
        categoryError: state.categoryReducer.categoryError,
        categoryMessage: state.categoryReducer.categoryMessage,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(categoryAction, dispatch);
}

const CategoryContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CategoryMap);

export default CategoryContainer;
