import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as consultantRecordAction from '../actions/consultantRecordAction';
import consultantRecordMap from '../../views/consultantRecord/consultantRecord-map';

function mapStateToProps(state) {
    return {
        consultantRecordList: state.consultantRecordReducer.consultantRecordList,
        consultantRecordStatus: state.consultantRecordReducer.consultantRecordStatus,
        consultantRecordLoading: state.consultantRecordReducer.consultantRecordLoading,
        consultantRecordError: state.consultantRecordReducer.consultantRecordError,
        consultantRecordMessage: state.consultantRecordReducer.consultantRecordMessage,

        consultantRecordDetailList: state.consultantRecordReducer.consultantRecordDetailList,
        consultantRecordDetailStatus: state.consultantRecordReducer.consultantRecordDetailStatus,
        consultantRecordDetailLoading: state.consultantRecordReducer.consultantRecordDetailLoading,
        consultantRecordDetailError: state.consultantRecordReducer.consultantRecordDetailError,
        consultantRecordDetailMessage: state.consultantRecordReducer.consultantRecordDetailMessage,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(consultantRecordAction, dispatch);
}

const ConsultantRecordContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(consultantRecordMap);

export default ConsultantRecordContainer;
