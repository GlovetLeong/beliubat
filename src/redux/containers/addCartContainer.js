import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as cartAction from '../actions/cartAction';

import AddCart from '../../views/cart/add-cart';


function mapStateToProps(state) {
    return {
        addCartStatus: state.cartReducer.addCartStatus,
        addCartError: state.cartReducer.addCartError,
        addCartMessage: state.cartReducer.addCartMessage,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(cartAction, dispatch);
}

const AddCartContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AddCart);

export default AddCartContainer;
