import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as paymentResultAction from '../actions/paymentResultAction';
import paymentResultMap from '../../views/paymentResult/paymentResult-map';

function mapStateToProps(state) {
    return {
        paymentResultList: state.paymentResultReducer.paymentResultList,
        paymentResultStatus: state.paymentResultReducer.paymentResultStatus,
        paymentResultLoading: state.paymentResultReducer.paymentResultLoading,
        paymentResultError: state.paymentResultReducer.paymentResultError,
        paymentResultMessage: state.paymentResultReducer.paymentResultMessage,

        proceedList: state.checkOutReducer.proceedList,
        proceedStatus: state.checkOutReducer.proceedStatus,
        proceedError: state.checkOutReducer.proceedError,
        proceedMessage: state.checkOutReducer.proceedMessage,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(paymentResultAction, dispatch);
}

const PaymentResultContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(paymentResultMap);

export default PaymentResultContainer;
