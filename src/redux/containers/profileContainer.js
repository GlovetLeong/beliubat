import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as profileAction from '../actions/profileAction';

// import ProfileModal from '../../views/profile/profile-modal';
import ProfileInfo from '../../views/profile-2/profile-info';


function mapStateToProps(state) {
    return {
        stateList: state.checkOutReducer.stateList,
        profileDetail: state.profileReducer.profileDetail,
        profileDetailStatus: state.profileReducer.profileDetailStatus,
        profileDetailLoading: state.profileReducer.profileDetailLoading,
        profileDetailError: state.profileReducer.profileDetailError,
        profileDetailMessage: state.profileReducer.profileDetailMessage,

        profileStatus: state.profileReducer.profileStatus,
        profileError: state.profileReducer.profileError,
        profileMessage: state.profileReducer.profileMessage,

        addressStatus: state.checkOutReducer.addressStatus,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(profileAction, dispatch);
}

const ProfileContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileInfo);

export default ProfileContainer;
