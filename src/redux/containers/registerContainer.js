import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as registerAction from '../actions/registerAction';

import RegisterModal from '../../views/register/register-modal';

function mapStateToProps(state) {
    return {
        registerStatus: state.registerReducer.registerStatus,
        registerError: state.registerReducer.registerError,
        registerMessage: state.registerReducer.registerMessage,
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(registerAction, dispatch);
}

const RegisterContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterModal);

export default RegisterContainer;
