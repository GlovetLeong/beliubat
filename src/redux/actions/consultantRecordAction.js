import Constant  from '../../utils/constant';
import Axios  from '../../utils/axios';
import { getSession }  from '../../utils/auth';
import { destoryPatientSession }  from './wrapperAction';

export const requestConsultantRecord = () => {
    return {
        type: 'REQUEST_CONSULTANT_RECORD',
        consultantLoading: true,
    }
}

export const requestConsultantRecordGet = (params) => {
    return function (dispatch) {
        dispatch(requestConsultantRecord());

        const session = getSession();

        const request = Axios({
            method: 'Get',
            url: Constant.api_url + 'member/medical_record?type=1&session=' + session,
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestConsultantRecordSuccess(response.data));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession());
                }
                else {
                    dispatch(requestConsultantRecordError(response.data));
                }
            }
        });
    }
}

export const requestConsultantRecordSuccess = (data) => {
    return {
        type: 'REQUEST_CONSULTANT_RECORD_SUCCESS',
        consultantRecordList: data,
        consultantRecordStatus: data.status,
        consultantRecordLoading: false,
        consultantRecordError: false,
        consultantRecordMessage: 'Success',
    }
}

export const requestConsultantRecordError = (data) => {
    return {
        type: 'REQUEST_CONSULTANT_RECORD_ERROR',
        consultantRecordList: [],
        consultantRecordStatus: data.status,
        consultantRecordLoading: true,
        consultantRecordError: true,
        consultantRecordMessage: 'Error',
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////


export const requestConsultantRecordDetail = () => {
    return {
        type: 'REQUEST_CONSULTANT_RECORD_DETAIL',
        consultantRecordDetailLoading: true,
    }
}

export const requestConsultantRecordDetailGet = (consultation_id) => {
    return function (dispatch) {

        dispatch(requestConsultantRecordDetail());

        const session = getSession();

        const request = Axios({
            method: 'Get',
            url: Constant.api_url + 'member/medical_record_detail?type=1&consultation_id=' + consultation_id + '&session=' + session,
        });

        return request.then(function(response){
            
            if(response.data.status === 1) {
                dispatch(requestConsultantRecordDetailSuccess(response.data));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession());
                }
                else {
                    dispatch(requestConsultantRecordDetailError(response.data));
                }
            }
        });
    }
}

export const requestConsultantRecordDetailSuccess = (data) => {
    return {
        type: 'REQUEST_CONSULTANT_RECORD_DETAIL_SUCCESS',
        consultantRecordDetailList: data,
        consultantRecordDetailStatus: data.status,
        consultantRecordDetailLoading: false,
        consultantRecordDetailError: false,
        consultantRecordDetailMessage: 'Success',
    }
}

export const requestConsultantRecordDetailError = (data) => {
    return {
        type: 'REQUEST_CONSULTANT_RECORD_DETAIL_ERROR',
        consultantRecordDetailList: [],
        consultantRecordDetailStatus: data.status,
        consultantRecordDetailLoading: true,
        consultantRecordDetailError: true,
        consultantRecordDetailMessage: 'Error',
    }
}