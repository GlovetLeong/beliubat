import Constant  from '../../utils/constant';
import Axios  from '../../utils/axios';
// import { requestLoginPost }  from './loginAction';

export const setStatus = (status) => {
    return {
        type: 'SET_STATUS',
        registerStatus: status
    }
}

export const requestRegister = () => {
    return {
        type: 'REQUEST_REGISTER',
    }
}

export const requestRegisterPost = (params) => {
    return function (dispatch) {
        dispatch(requestRegister());

        const formData = new FormData();

        var d = new Date(params.dob),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        var dob = [year, month, day].join('-');

        formData.append('email', params.email);
        formData.append('firstname', params.firstname);
        formData.append('lastname', params.lastname);
        formData.append('password', params.password);
        formData.append('confirm_password', params.confirm_password);
        formData.append('mobile', params.mobile);
        formData.append('dob', dob);
        formData.append('ic_no', params.ic_no);
        formData.append('gender', params.gender);
        
        const request = Axios({
            method: 'POST',
            url: Constant.api_url + 'member/register',
            data: formData
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestRegisterSuccess(response.data));
                //login when registered
                // const params_login = {
                //     email: params.email,
                //     password: params.password,
                // }
                // dispatch(requestLoginPost(params_login));
            }
            else if(response.data.status === 0) {
                dispatch(requestRegisterError(response.data));
            }
        });
    }
}

export const requestRegisterSuccess = (data) => {
    return {
        type: 'REQUEST_REGISTER_SUCCESS',
        registerStatus: data.status,
        registerError: false,
        registerMessage: 'Welcome',
    }
}

export const requestRegisterError = (data) => {
    return {
        type: 'REQUEST_REGISTER_ERROR',
        registerStatus: data.status,
        registerError: true,
        registerMessage: data.validation,
    }
}