import Constant  from '../../utils/constant';
import Axios  from '../../utils/axios';

export const setStatus = (status) => {
    return {
        type: 'SET_STATUS',
        loginStatus: status
    }
}

export const requestLogin = () => {
    return {
        type: 'REQUEST_LOGIN',
    }
}

export const requestLoginPost = (params) => {
    return function (dispatch) {
        dispatch(requestLogin());

        const formData = new FormData();

        formData.append('email', params.email);
        formData.append('password', params.password);
        
        const request = Axios({
            method: 'POST',
            url: Constant.api_url + 'member/login',
            data: formData
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestLoginSuccess(response.data));
            }
            else if(response.data.status === 0) {
                dispatch(requestLoginError(response.data));
            }
        });
    }
}

export const requestLoginSuccess = (data) => {
    return {
        type: 'REQUEST_LOGIN_SUCCESS',
        loginStatus: data.status,
        loginError: false,
        loginMessage: 'Welcome ' + data.result.firstname,
        patientSession: data.result.session,
    }
}

export const requestLoginError = (data) => {
    return {
        type: 'REQUEST_LOGIN_ERROR',
        loginStatus: data.status,
        loginError: true,
        loginMessage: data.validation,
        patientSession: '',
    }
}