import Constant  from '../../utils/constant';
import Axios  from '../../utils/axios';

export const requestCategory = () => {
    return {
        type: 'REQUEST_CATEGORY',
    }
}

export const requestCategoryGet = (params) => {
    return function (dispatch) {
        dispatch(requestCategory());

        const request = Axios({
            method: 'Get',
            url: Constant.api_url + 'general/get_category',
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                let newCategoryList = [];
                newCategoryList.push({
                    id: 0,
                    name_en: 'All',
                    active: true
                });

                response.data.result.map((data, index) =>
                    newCategoryList.push({
                        id: data.id,
                        name_en: data.name_en,
                        active: false
                    })
                );

                response.data.result = newCategoryList;

                dispatch(requestCategorySuccess(response.data));
            }
            else if(response.data.status === 0) {
                dispatch(requestCategoryError(response.data));
            }
        });
    }
}

export const requestCategorySuccess = (data) => {
    return {
        type: 'REQUEST_CATEGORY_SUCCESS',
        categoryList: data,
        categoryStatus: data.status,
        categoryError: false,
        categoryMessage: 'Success',
    }
}

export const requestCategoryError = (data) => {
    return {
        type: 'REQUEST_CATEGORY_ERROR',
        categoryList: [],
        categoryStatus: data.status,
        categoryError: true,
        categoryMessage: 'Error',
    }
}

export const categoryClickActive= (data, id) => {
    var obj = data.result;
    Object.keys(obj).forEach(function(key) {
        (obj[key].id === id) ?
            obj[key].active = true
        :
            obj[key].active = false
    });
    return {
        type: 'CLICK_ACTIVE',
        categoryList: data,
        categoryStatus: data.status,
        categoryError: false,
        categoryMessage: 'Success',
    }
}
