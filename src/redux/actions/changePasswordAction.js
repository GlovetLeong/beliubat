import Constant  from '../../utils/constant';
import Axios  from '../../utils/axios';
import { getSession }  from '../../utils/auth';

export const setStatus = (status) => {
    return {
        type: 'SET_STATUS',
        changePasswordStatus: status
    }
}

export const requestChangePasssword = (params) => {
    return function (dispatch) {

        const session = getSession();

        const formData = new FormData();

        formData.append('old_password', params.old_password);
        formData.append('new_password', params.new_password);
        formData.append('confirm_password', params.confirm_password);
        formData.append('session', session);

        const request = Axios({
            method: 'POST',
            url: Constant.api_url + 'member/change_password',
            data: formData
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestChangePassswordSuccess(response.data));
            }
            else if(response.data.status === 0) {
                dispatch(requestChangePassswordError(response.data));
            }
        });
    }
}

export const requestChangePassswordSuccess = (data) => {
    return {
        type: 'REQUEST_CHANGE_PASSWORD_SUCCESS',
        changePasswordStatus: data.status,
        changePasswordError: false,
        changePasswordMessage: 'Password Successful Updated',
    }
}

export const requestChangePassswordError = (data) => {
    return {
        type: 'REQUEST_CHANGE_PASSWORD_ERROR',
        changePasswordStatus: data.status,
        changePasswordError: true,
        changePasswordMessage: data.validation,
    }
}