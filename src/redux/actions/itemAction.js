import Constant  from '../../utils/constant';
import { reverseObject }  from '../../utils/helper';
import Axios  from '../../utils/axios';
import { getSession }  from '../../utils/auth';
import { destoryPatientSession }  from './wrapperAction';

const qs = require('qs');

export function setItemListData(data) {
    return { type: 'REQUEST_SET_ITEM_LIST_DATA', data: data }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const requestItemList = () => {
    return {
        type: 'REQUEST_ITEM_LIST',
        itemListLoading: true,
    }
}

export const requestItemListGet = (params) => {
    return function (dispatch) {
        dispatch(requestItemList());

        var query_string = qs.stringify(params);
        
        const request = Axios({
            method: 'Get',
            url: Constant.api_url + 'product/search?' + query_string
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestItemListSuccess(response.data));
            }
            else if(response.data.status === 0) {
                dispatch(requestItemListError(response.data));
            }
        });
    }
}

export const requestItemListSuccess = (data) => {
    return {
        type: 'REQUEST_ITEM_LIST_SUCCESS',
        itemList: data.result,
        itemListStatus: data.status,
        itemListLoading: false,
        itemListError: false,
        itemListMessage: 'Success',
    }
}

export const requestItemListError = (data) => {
    return {
        type: 'REQUEST_ITEM_LIST_ERROR',
        itemList: [],
        itemListStatus: data.status,
        itemListLoading: true,
        itemListError: true,
        itemListMessage: 'Error',
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const requestHotItemListHorizontal = () => {
    return {
        type: 'REQUEST_HOT_ITEM_LIST',
        itemListLoading: true,
    }
}

export const requestHotItemListHorizontalGet = (params) => {
    return function (dispatch) {
        dispatch(requestHotItemListHorizontal());

        var query_string = qs.stringify(params);
        
        const request = Axios({
            method: 'Get',
            url: Constant.api_url + 'product/search?' + query_string
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestHotItemListHorizontalSuccess(response.data));
            }
            else if(response.data.status === 0) {
                dispatch(requestHotItemListHorizontalError(response.data));
            }
        });
    }
}

export const requestHotItemListHorizontalSuccess = (data) => {
    return {
        type: 'REQUEST_HOT_ITEM_LIST_HORIZONTAL_SUCCESS',
        itemList: data.result,
        itemListStatus: data.status,
        itemListLoading: false,
        itemListError: false,
        itemListMessage: 'Success',
    }
}

export const requestHotItemListHorizontalError = (data) => {
    return {
        type: 'REQUEST_HOT_ITEM_LIST_HORIZONTAL_ERROR',
        itemList: [],
        itemListStatus: data.status,
        itemListLoading: true,
        itemListError: true,
        itemListMessage: 'Error',
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const requestLoadMore = (params, next_page, pervious_data) => {
    return function (dispatch) {

        var query_string = qs.stringify(params);

        const request = Axios({
            method: 'Get',
            url: Constant.api_url + 'product/search?page=' + next_page + '&' + query_string,
        });

        return request.then(function(response){
            if(response.data.status === 1) {                
                const currentItemList = response.data.result.products
                currentItemList.map((item, index) =>
                    pervious_data.push(item)
                )
                response.data.result.products = pervious_data;

                dispatch(requestLoadMoreSuccess(response.data));
            }
            else if(response.data.status === 0) {
                dispatch(requestLoadMoreError(response.data));
            }
        });
    }
}

export const requestLoadMoreSuccess = (data) => {
    return {
        type: 'REQUEST_LOAD_MORE_SUCCESS',
        itemList: data.result,
        itemListStatus: data.status,
        itemListError: false,
        itemListMessage: 'Success',
    }
}

export const requestLoadMoreError = (data) => {
    return {
        type: 'REQUEST_LOAD_MORE_ERROR',
        itemListStatus: data.status,
        itemListError: true,
        itemListMessage: 'Error',
    }
}


export const requestItemDetail = () => {
    return {
        type: 'REQUEST_ITEM_DETAIL',
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const requestItemDetailGet = (params) => {
    return function (dispatch) {

        var query_string = qs.stringify(params);

        const request = Axios({
            method: 'Get',
            url: Constant.api_url + 'product/detail?' + query_string,
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                response.data.result.quantity_price = response.data.result.price;
                response.data.result.quantity_market_price = response.data.result.market_price;
                dispatch(requestItemDetailSuccess(response.data));
            }
            else if(response.data.status === 0) {
                dispatch(requestItemDetailError(response.data));
            }
        });
    }
}

export const requestItemDetailSuccess = (data) => {
    return {
        type: 'REQUEST_ITEM_DETAIL_SUCCESS',
        itemDetail: data.result,
        itemDetailStatus: data.status,
        itemListError: false,
        itemDetailMessage: 'Success',
    }
}

export const requestItemDetailError = (data) => {
    return {
        type: 'REQUEST_ITEM_DETAIL_ERROR',
        itemDetailStatus: data.status,
        itemDetailError: true,
        itemDetailMessage: 'Error',
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const setQuantityPrice = (type, itemDetail, quantity) => {

    return function (dispatch) {

        if(type === 'more') {
            itemDetail.quantity_price = (itemDetail.price * (quantity + 1) );
            itemDetail.quantity_market_price = (itemDetail.market_price * (quantity + 1) );
            itemDetail.quantity = quantity + 1;
        }
        else {
            if((quantity - 1) > 0) {
                itemDetail.quantity_price = (itemDetail.price * (quantity - 1) );
                itemDetail.quantity_market_price = (itemDetail.market_price * (quantity - 1) );
                itemDetail.quantity = quantity - 1;
            }
            if((quantity - 1) === 0) {
                itemDetail.quantity = 0;
            }
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const requestItemReviewDetailGet = (params) => {
    return function (dispatch) {

        var query_string = qs.stringify(params);

        const request = Axios({
            method: 'Get',
            url: Constant.api_url + 'product/review_average?' + query_string,
        });

        return request.then(function(response){

            if(response.data.status === 1) {

                var review_average_rating = response.data.result.review_average_rating.toString();
                var total_on_star = 0;
                var total_half_on_star = 0;
                var total_off_star = 0;

                review_average_rating = review_average_rating.split('.');

                total_on_star = Number(review_average_rating[0]);

                if (review_average_rating[1]) {
                    total_half_on_star = 1;
                }
                total_off_star = 5 - (Number(total_on_star) + Number(total_half_on_star));

                response.data.result.review_average_star = {
                    total_on_star: total_on_star,
                    total_half_on_star: total_half_on_star,
                    total_off_star: total_off_star,
                };

                var review_ratings = response.data.result.review_ratings;
                var total_review_ratings = 0;
                var a_bar_value = 100;

                for (var k in review_ratings){
                    if (review_ratings.hasOwnProperty(k)) {
                        total_review_ratings = total_review_ratings + Number(review_ratings[k]);
                    }
                }

                if (total_review_ratings > 0) {
                    a_bar_value = Math.round(100 / total_review_ratings);
                }

                response.data.result.total_review_ratings = total_review_ratings;
                response.data.result.a_bar_value = a_bar_value;

                response.data.result.review_ratings = reverseObject(response.data.result.review_ratings);

                dispatch(requestItemReviewSuccess(response.data));
            }
            else if(response.data.status === 0) {
                dispatch(requestItemReviewError(response.data));
            }
        });
    }
}

export const requestItemReviewSuccess = (data) => {
    return {
        type: 'REQUEST_ITEM_REVIEW_SUCCESS',
        itemReviewDetail: data.result,
        itemReviewStatus: data.status,
        itemReviewError: false,
        itemReviewMessage: 'Success',
    }
}

export const requestItemReviewError = (data) => {
    return {
        type: 'REQUEST_ITEM_REVIEW_ERROR',
        itemReviewStatus: data.status,
        itemReviewError: true,
        itemReviewMessage: 'Error',
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const setItemReviewCreateStatus = (status) => {
    return {
        type: 'SET_ITEM_REVIEW_STATUS_STATUS',
        itemReviewCreateStatus: status
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const requestCreateItemReview = (params) => {
    return function (dispatch) {

        const session = getSession();

        const formData = new FormData();

        formData.append('id', params.id);
        formData.append('review_star', params.review_star);
        formData.append('review_text', params.review_text);
        formData.append('session', session);

        const request = Axios({
            method: 'Post',
            url: Constant.api_url + 'product/create_review',
            data: formData
        });

        return request.then(function(response){

            if(response.data.status === 1) {
                dispatch(requestCreateItemReviewSuccess(response.data));

                const params_2 = {
                    id: params.id
                }
                dispatch(requestItemReviewDetailGet(params_2));
            }
            else if(response.data.status === 0) {

                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession(1));
                }
                else {
                    dispatch(requestCreateItemReviewError(response.data));
                }
            }
        });
    }
}

export const requestCreateItemReviewSuccess = (data) => {
    return {
        type: 'REQUEST_ITEM_REVIEW_CREATE_SUCCESS',
        itemReviewCreate: data.result,
        itemReviewCreateStatus: data.status,
        itemReviewCreateError: false,
        itemReviewCreateMessage: 'Review Sent',
    }
}

export const requestCreateItemReviewError = (data) => {
    return {
        type: 'REQUEST_ITEM_REVIEW_CREATE_ERROR',
        itemReviewCreateStatus: data.status,
        itemReviewCreateError: true,
        itemReviewCreateMessage: data.validation,
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

