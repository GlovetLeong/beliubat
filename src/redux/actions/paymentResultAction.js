import Constant  from '../../utils/constant';
import Axios  from '../../utils/axios';
import { getTotalCart } from './wrapperAction';
import { getSession }  from '../../utils/auth';
import { destoryPatientSession }  from './wrapperAction';

export const requestPaymentResult = () => {
    return {
        type: 'REQUEST_PAYMENT_RESULT',
        paymentResultLoading: true,
    }
}

export const requestPaymentResultGet = (params) => {
    return function (dispatch) {
        dispatch(requestPaymentResult());

        const session = getSession();

        const request = Axios({
            method: 'Get',
            url: Constant.api_url + 'shopping/order_history?session=' + session,
        });

        return request.then(function(response){
            
            if(response.data.status === 1) {
                if (response.data.result) {
                    response.data.result.forEach(function(cart, index){
                        cart.order_details.forEach(function(product, index_2) {
                            if (product.product_id === '1') {
                                response.data.result[index].order.consultant_fee = product.price;
                                delete response.data.result[index].order_details[index_2];
                            }

                        });

                    // var d = new Date(data.order.created_date),
                    // month = '' + (d.getMonth() + 1),
                    // day = '' + d.getDate(),
                    // year = d.getFullYear();
                    // if (month.length < 2) month = '0' + month;
                    // if (day.length < 2) day = '0' + day;

                    // var new_date = [year, month, day].join('-');
                    // data.order.created_date = new_date;
                    });
                }

                dispatch(requestPaymentResultSuccess(response.data));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession());
                }
                else {
                    dispatch(requestPaymentResultError(response.data));
                }
            }
        });
    }
}

export const requestPaymentResultSuccess = (data) => {
    return {
        type: 'REQUEST_PAYMENT_RESULT_SUCCESS',
        paymentResultList: data,
        paymentResultStatus: data.status,
        paymentResultLoading: false,
        paymentResultError: false,
        paymentResultMessage: 'Success',
    }
}

export const requestPaymentResultError = (data) => {
    return {
        type: 'REQUEST_PAYMENT_RESULT_ERROR',
        paymentResultList: [],
        paymentResultStatus: data.status,
        paymentResultLoading: true,
        paymentResultError: true,
        paymentResultMessage: 'Error',
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const setProceedStatus = (status) => {
    return {
        type: 'SET_PROCEED_STATUS',
        proceedStatus: status
    }
}

export const requestProceedCheckOutSuccess = (data) => {
    return {
        type: 'REQUEST_PRPCEED_CHECKOUT_SUCCESS',
        proceedList: data,
        proceedStatus: data.status,
        message: 'Success',
    }
}

export const requestProceedCheckOutError = (data) => {
    var message = 'Error';
    if (data.message === 'SHIPPING_ADDRESS_EMPTY') {
        message = 'Shipping Address Empty';
    }
    if (data.message === 'BILLING_ADDRESS_EMPTY') {
        message = 'Billing Address Empty';
    }
    return {
        type: 'REQUEST_PRPCEED_CHECKOUT_ERROR',
        proceedList: [],
        proceedStatus: data.status,
        message: message,
    }
}

export const postProceedCheckOut = (order_id) => {
    return function (dispatch) {
        
        const session = getSession();

        const formData = new FormData();

        formData.append('order_id', order_id);
        formData.append('session', session);

        const request = Axios({
            method: 'POST',
            url: Constant.api_url + 'shopping/payment',
            data: formData
        });

        return request.then(function(response){

            if(response.data.status === 1) {
                dispatch(requestProceedCheckOutSuccess(response.data));
                dispatch(getTotalCart());
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession());
                }
                else {
                    dispatch(requestProceedCheckOutError(response.data));
                }
            }
        });
    
    }
}