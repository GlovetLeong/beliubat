import Constant  from '../../utils/constant';
import Axios  from '../../utils/axios';
import { getSession }  from '../../utils/auth';
import { destoryPatientSession }  from './wrapperAction';

export function setProfileDetailData(data) {
    return { type: 'REQUEST_SET_PROFILE_DETAIL_DATA', data: data }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const setAddressStatus = (status) => {
    return {
        type: 'SET_ADDRESS_STATUS',
        addressStatus: status
    }
}

export const requestGetState = () => {
    return {
        type: 'REQUEST_GET_STATE',
    }
}

export const requestGetStateSuccess = (data) => {
    return {
        type: 'REQUEST_GET_STATE_SUCCESS',
        stateList: data,
        status: data.status,
    }
}

export const requestGetStatetError = (data) => {
    return {
        type: 'REQUEST_GET_STATE_ERROR',
        stateList: [],
        status: data.status,
        message: 'Error',
    }
}

export const getState = () => {
    return function (dispatch) {
        dispatch(requestGetState());

        const request = Axios({
            method: 'GET',
            url: Constant.api_url + 'general/get_state',
        });
        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestGetStateSuccess(response.data));
            }
            else if(response.data.status === 0) {
                dispatch(requestGetStatetError(response.data));
            }
        });
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const requestProfileDetail = () => {
    return {
        type: 'REQUEST_PROFILE_DETAIL',
        profileDetailLoading: true
    }
}

export const requestProfileDetailGet = (params) => {
    return function (dispatch) {
        dispatch(requestProfileDetail());

        const session = getSession();

        const request = Axios({
            method: 'Get',
            url: Constant.api_url + 'member/details?session=' + session
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                response.data.result.readOnlys = response.data.result;
                dispatch(requestProfileDetailSuccess(response.data));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession(1));
                }
                else if (response.data.message === 'CART_EMPTY') {
                    // dispatch(destoryPatientSession(1));
                }
                else {
                    dispatch(requestProfileDetailError(response.data));
                }
            }
        });
    }
}

export const requestProfileDetailSuccess = (data) => {
    return {
        type: 'REQUEST_PROFILE_DETAIL_SUCCESS',
        profileDetail: data.result,
        profileDetailStatus: data.status,
        profileDetailLoading: false,
        profileDetailError: false,
        profileDetailMessage: 'Success',
    }
}

export const requestProfileDetailError = (data) => {
    return {
        type: 'REQUEST_PROFILE_DETAIL_ERROR',
        profileDetailStatus: data.status,
        profileDetailLoading: true,
        profileDetailError: true,
        profileDetailMessage: 'Error',
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const requestProfileUpdate = () => {
    return {
        type: 'REQUEST_PROFILE_UPDATE',
    }
}

export const requestProfileUpdatePost = (params) => {
    return function (dispatch) {

        const session = getSession();

        const formData = new FormData();

        formData.append('firstname', params.firstname);
        formData.append('lastname', params.lastname);
        formData.append('dob', params.dob);
        formData.append('mobile', params.mobile);
        formData.append('ic_no', params.ic_no);
        formData.append('gender', params.gender);
        formData.append('session', session);
        
        const request = Axios({
            method: 'POST',
            url: Constant.api_url + 'member/update',
            data: formData
        });

        return request.then(function(response){
            console.log(response.data);
            
            if(response.data.status === 1) {
                dispatch(requestProfileDetailGet());
                dispatch(requestProfileUpdateSuccess(response.data, params));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession(1));
                }
                else {
                    dispatch(requestProfileUpdateError(response.data, params));
                }
            }
        });
    }
}

export const requestProfileUpdateSuccess = (data, old_data) => {
    return {
        type: 'REQUEST_PROFILE_UPDATE_SUCCESS',
        profileDetail: old_data,
        profileDetailStatus: data.status,
        profileError: false,
        profileDetailMessage: 'Patient Details Successful Updated',
    }
}

export const requestProfileUpdateError = (data, old_data) => {
    return {
        type: 'REQUEST_PROFILE_UPDATE_ERROR',
        profileDetail: old_data,
        profileDetailStatus: data.status,
        profileError: true,
        profileDetailMessage: data.validation,
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function setAddressData(newAddress, data, type) {

    if(type === 1) {
        data.shipping_address = newAddress;
    }
    else {
        data.billing_address = newAddress;
    }

    return { type: 'REQUEST_SET_SHIPPING_ADDRESS', profileDetail: data }
}

export const requestChangeAddressSuccess = (data) => {
    return {
        type: 'REQUEST_CHANGE_ADDRESS_SUCCESS',
        addressStatus: data.status,
        message: 'Address Successful Updated',
    }
}

export const requestChangeAddressError = (data) => {
    return {
        type: 'REQUEST_CHANGE_ADDRESS_ERROR',
        addressStatus: data.status,
        message: data.validation,
    }
}

export const postChangeAddress = (params, type) => {

    return function (dispatch) {

        const session = getSession();

        const formData = new FormData();

        if(type === 1) {
            formData.append('type', 'shipping');
        }
        else {
            formData.append('type', 'billing');
        }
        
        formData.append('address_1', params.address_1 ? params.address_1 : '');
        formData.append('address_2', params.address_2 ? params.address_2 : '');
        formData.append('postcode', params.address_postcode ? params.address_postcode : '');
        formData.append('state', params.address_stateid ? params.address_stateid : '');
        formData.append('country', Constant.country);
        formData.append('session', session);

        const request = Axios({
            method: 'POST',
            url: Constant.api_url + 'shopping/add_address',
            data: formData
        });

        return request.then(function(response){
            if(response.data.status === 1) {        
                dispatch(requestProfileDetailGet());
                dispatch(requestChangeAddressSuccess(response.data));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession(1));
                }
                else {
                    dispatch(requestChangeAddressError(response.data));
                }
            }
        });
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////