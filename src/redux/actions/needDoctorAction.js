import Constant  from '../../utils/constant';
import Axios  from '../../utils/axios';
import { getSession }  from '../../utils/auth';
import { destoryPatientSession }  from './wrapperAction';

export const setSymptomsStatus = (status) => {
    return {
        type: 'SET_SYMPOMTS_STATUS',
        symptomsStatus: status
    }
}

export const requestnSymptomsPost = (params) => {
    return function (dispatch) {

        const session = getSession();

        const formData = new FormData();

        formData.append('type', 1); //request doctor 
        formData.append('message', params.message);
        formData.append('passcode', params.passcode);
        formData.append('session', session);
        
        const request = Axios({
            method: 'POST',
            url: Constant.api_url + 'member/consultation_request',
            data: formData
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestSymptomsSuccess(response.data));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession(1));
                }
                else {
                    dispatch(requestSymptomsError(response.data));
                }
            }
        });
    }
}

export const requestSymptomsSuccess = (data) => {
    return {
        type: 'REQUEST_SYMPTOMS_SUCCESS',
        symptomsStatus: data.status,
        symptomsError: false,
        symptomsMessage: 'Requested Successful',
    }
}

export const requestSymptomsError = (data) => {
    return {
        type: 'REQUEST_SYMPTOMS_ERROR',
        symptomsStatus: data.status,
        symptomsError: true,
        symptomsMessage: data.validation,
    }
}