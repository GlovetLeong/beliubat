import Constant  from '../../utils/constant';
import { getSession, checkSession, destorySession }  from '../../utils/auth';
import Axios  from '../../utils/axios';

export const getPatientSession = () => {
   	return {
        type: 'GET_PATIENT_SESSION',
        patientSession: getSession(),
    }
}

export const checkPatientSession = () => {
   	return {
        type: 'CHECK_PATIENT_SESSION',
        patientSessionStatus: checkSession(),
    }
}

export const destoryPatientSession = (message) => {

    if (message === 1) {
      message = 'Please login to continue';
    }
   	return {
        type: 'DESTORY_PATIENT_SESSION',
        patientSessionStatus: destorySession(),
        patientSession: getSession(),
        message: message ? message : 'Logout'
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const getTotalCartSuccess = (totalCart) => {
    return {
        type: 'GET_TOTAL_CART',
        totalCart: totalCart
    }
}

export const getTotalCart = () => {
    return function (dispatch) {
      
        const session = getSession();

        const request = Axios({
            method: 'Get',
            url: Constant.api_url + 'shopping/my_cart?session=' + session
        });

        return request.then(function(response){

            if(response.data.status === 1) {

              const cartList = response.data.result;
              
              if (cartList) {
                cartList.forEach(function(cart, index){
                    if (cart.product_id === '1') {
                        delete response.data.result[index];
                    }
                });
              }

              var cartRender = 0;
              var totalCart = 0;

              if (cartList) {
                cartRender = cartList.filter(cart => cart.name);
                totalCart = cartRender.length;
              }

              dispatch(getTotalCartSuccess(totalCart));
            }
        });
    }
}