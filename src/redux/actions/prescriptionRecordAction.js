import Constant  from '../../utils/constant';
import Axios  from '../../utils/axios';
import { getSession }  from '../../utils/auth';
import { destoryPatientSession }  from './wrapperAction';

export const requestPrescriptionRecord = () => {
    return {
        type: 'REQUEST_PRESCRIPTION_RECORD',
        paymentResultLoading: true,
    }
}

export const requestPrescriptionRecordGet = (params) => {
    return function (dispatch) {
        dispatch(requestPrescriptionRecord());

        const session = getSession();

        const request = Axios({
            method: 'Get',
            url: Constant.api_url + 'member/medical_record?type=2&session=' + session,
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestPrescriptionRecordSuccess(response.data));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession());
                }
                else {
                    dispatch(requestPrescriptionRecordError(response.data));
                }
            }
        });
    }
}

export const requestPrescriptionRecordSuccess = (data) => {
    return {
        type: 'REQUEST_PRESCRIPTION_RECORD_SUCCESS',
        prescriptionRecordList: data,
        prescriptionRecordStatus: data.status,
        prescriptionRecordLoading: false,
        prescriptionRecordError: false,
        prescriptionRecordMessage: 'Success',
    }
}

export const requestPrescriptionRecordError = (data) => {
    return {
        type: 'REQUEST_PRESCRIPTION_RECORD_ERROR',
        prescriptionRecordList: [],
        prescriptionRecordStatus: data.status,
        prescriptionRecordLoading: true,
        prescriptionRecordError: true,
        prescriptionRecordMessage: 'Error',
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const requestPrescriptionRecordDetailGet = (consultation_id) => {
    return function (dispatch) {

        const session = getSession();

        const request = Axios({
            method: 'Get',
            url: Constant.api_url + 'member/medical_record_detail?type=2&consultation_id=' + consultation_id + '&session=' + session,
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestPrescriptionRecordDetailSuccess(response.data));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession());
                }
                else {
                    dispatch(requestPrescriptionRecordDetailError(response.data));
                }
            }
        });
    }
}

export const requestPrescriptionRecordDetailSuccess = (data) => {
    return {
        type: 'REQUEST_PRESCRIPTION_RECORD_DETAIL_SUCCESS',
        prescriptionRecordDetailList: data,
        prescriptionRecordDetailStatus: data.status,
        prescriptionRecordDetailLoading: false,
        prescriptionRecordDetailError: false,
        prescriptionRecordDetailMessage: 'Success',
    }
}

export const requestPrescriptionRecordDetailError = (data) => {
    return {
        type: 'REQUEST_PRESCRIPTION_RECORD_DETAIL_ERROR',
        prescriptionRecordDetailList: [],
        prescriptionRecordDetailStatus: data.status,
        prescriptionRecordDetailLoading: true,
        prescriptionRecordDetailError: true,
        prescriptionRecordDetailMessage: 'Error',
    }
}