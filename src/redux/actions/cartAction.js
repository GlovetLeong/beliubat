import Constant  from '../../utils/constant';
import Axios  from '../../utils/axios';
import { getSession }  from '../../utils/auth';
import { getTotalCart } from './wrapperAction';
import { requestCheckOutGet } from './checkOutAction';
import { destoryPatientSession }  from './wrapperAction';

export const requestAddCartByDetailSuccess = (data) => {
    return {
        type: 'REQUEST_ADD_CART_BY_DETAIL_SUCCESS',
        addCartStatus: data.status,
        addCartMessage: 'Successful add to cart',
        itemDetail: data,
    }
}

export const requestAddCartByDetailError = (data) => {
    return {
        type: 'REQUEST_ADD_CART_BY_DETAIL_ERROR',
        addCartStatus: data.status,
        addCartMessage: 'Error',
        itemDetail: [],
    }
}

export const addCartByDetail = (item) => {
    return function (dispatch) {
        if (item.quantity === undefined) {
            item.quantity = 1;
        }

        const session = getSession();

        const formData = new FormData();

        formData.append('product_id', item.product_id);
        formData.append('quantity', item.quantity);
        formData.append('session', session);
    
        const request = Axios({
            method: 'POST',
            url: Constant.api_url + 'shopping/add_cart',
            data: formData
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestAddCartByDetailSuccess(response.data));
                dispatch(requestCartListGet());
                dispatch(getTotalCart());
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession(1));
                }
                else {
                    dispatch(requestAddCartByDetailError(response.data));
                }
            }
        });
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const requestAddCartByClickSuccess = (data) => {
    return {
        type: 'REQUEST_ADD_CART_BY_CLICK_SUCCESS',
        addCartStatus: data.status,
        addCartMessage: 'Successful add to cart',
        itemDetail: data,
    }
}

export const requestAddCartByClickError = (data) => {
    return {
        type: 'REQUEST_ADD_CART_BY_CLICK_ERROR',
        addCartStatus: data.status,
        addCartMessage: 'Error',
        itemDetail: '',
    }
}

export const addCartByClick = (item) => {
    return function (dispatch) {
        if (item.quantity === undefined) {
            item.quantity = 1;
        }

        const session = getSession();

        const formData = new FormData();

        formData.append('product_id', item.product_id);
        formData.append('quantity', item.quantity);
        formData.append('session', session);
    
        const request = Axios({
            method: 'POST',
            url: Constant.api_url + 'shopping/add_cart',
            data: formData
        });

        return request.then(function(response){

            if(response.data.status === 1) {
                dispatch(requestAddCartByClickSuccess(response.data));
                // if (item.quantity > 0) {
                    // dispatch(requestCheckOutGet());
                // }
                dispatch(getTotalCart());
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession(1));
                }
                else {
                    dispatch(requestAddCartByClickError(response.data));
                }
            }
        });
    }
}

export const requestClickCheckOutGet = () => {
    return function (dispatch) {
        dispatch(requestCheckOutGet());
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const requestGetCartList = () => {
    return {
        type: 'REQUEST_GET_CART_LIST',
        cartLoading: true
    }
}

export const requestGetCartListSuccess = (data) => {
    return {
        type: 'REQUEST_GET_CART_LIST_SUCCESS',
        cartList: data,
        cartListStatus: data.status,
        cartLoading: false,
        cartListError: false,
        cartListMessage: 'Success',
    }
}

export const requestGetCartListError = (data) => {
    return {
        type: 'REQUEST_GET_CART_LIST_ERROR',
        cartList: [],
        cartListStatus: data.status,
        cartLoading: true,
        cartListError: true,
        cartListMessage: 'Error',
    }
}


export const requestCartListGet = () => {
    return function (dispatch) {

        dispatch(requestGetCartList());

        const session = getSession();

        const request = Axios({
            method: 'GET',
            url: Constant.api_url + 'shopping/my_cart?session=' + session,
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                var total_amount = 0;
                if(response.data.result) {

                    response.data.result.forEach(function(cart, index){
                        if (cart.product_id === '1') {
                            response.data.result.consultant_fee = cart.price;
                            delete response.data.result[index];
                        }
                        cart.quantity = Number(cart.quantity);
                        cart.quantity_price = (cart.price * cart.quantity);
                        cart.quantity_market_price = (cart.market_price * cart.quantity);
                        total_amount = total_amount + cart.quantity_price;
                    });
                    response.data.result.total_amount = total_amount;
                }
                
                dispatch(requestGetCartListSuccess(response.data));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession(1));
                }
                else {
                    dispatch(requestGetCartListError(response.data));
                }
            }
        });

    }
}

export const setCartQuantityPrice = (type, product_id, cartList) => {
    return function (dispatch) {

        var total_amount = 0;
        cartList.result.forEach(function(cart, index){
            var quantity = 1;
            if(cart.product_id === product_id) {
                if(type === 'more') {
                    cart.quantity_price = (cart.price * (cart.quantity + 1));
                    cart.quantity_market_price = (cart.market_price * (cart.quantity + 1));
                    cart.quantity = (cart.quantity + 1);
                    quantity = '1';
                }
                else if(type === 'delete') {
                    cart.quantity = 0;
                    quantity = 0;
                    cart.quantity_price = 0;
                    delete cartList.result[index];
                }
                else {
                    if((cart.quantity - 1) > 0) {
                        cart.quantity_price = (cart.price * (cart.quantity - 1));
                        cart.quantity_market_price = (cart.market_price * (cart.quantity - 1));
                        cart.quantity = (cart.quantity - 1);

                        quantity = '-1';
                    }
                }

                //update cart
                var item = {};
                item={
                    'product_id': product_id,
                    'quantity': quantity,
                }
                dispatch(addCartByClick(item));
            }

            // if(type !== 'delete') {
                total_amount = total_amount + cart.quantity_price;
            // }
        });

        cartList.result.total_amount = total_amount;

        dispatch(setCartList(cartList));
    }
}

export function setCartList(cartList) {
    return { type: 'REQUEST_SET_CART_LIST', cartList: cartList }
}