import Constant  from '../../utils/constant';
import Axios  from '../../utils/axios';
import { getSession }  from '../../utils/auth';
import { destoryPatientSession }  from './wrapperAction';

export const setuploadPrescriptionStatus = (status) => {
    return {
        type: 'SET_UPLOAD_PRESCRIPTION_STATUS',
        uploadPrescriptionStatus: status
    }
}

export const requestUploadPrescriptionPost = (params) => {
    return function (dispatch) {

        const session = getSession();

        const formData = new FormData();

        formData.append('type', 2); //request prescription 
        formData.append('session', session);
        formData.append('prescription', params.files);
        formData.append('message', params.message);

        const request = Axios({
            method: 'POST',
            url: Constant.api_url + 'member/consultation_request',
            data: formData
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestUploadPrescriptionSuccess(response.data));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession(1));
                }
                else {
                    dispatch(requestUploadPrescriptionError(response.data));
                }
            }
        });
    }
}

export const requestUploadPrescriptionSuccess = (data) => {
    return {
        type: 'REQUEST_UPLOAD_PRESCRIPTION_SUCCESS',
        uploadPrescriptionStatus: data.status,
        uploadPrescriptionError: false,
        uploadPrescriptionMessage: 'Requested Successful',
    }
}

export const requestUploadPrescriptionError = (data) => {
    return {
        type: 'REQUEST_UPLOAD_PRESCRIPTION_ERROR',
        uploadPrescriptionStatus: data.status,
        uploadPrescriptionError: true,
        uploadPrescriptionMessage: data.validation,
    }
}