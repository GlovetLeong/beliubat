import Constant  from '../../utils/constant';
import Axios  from '../../utils/axios';
import { getSession }  from '../../utils/auth';
import { getTotalCart } from './wrapperAction';
import { destoryPatientSession }  from './wrapperAction';

export const setStatus = (status) => {
    return {
        type: 'SET_STATUS',
        checkOutStatus: status
    }
}

export const setAddressStatus = (status) => {
    return {
        type: 'SET_ADDRESS_STATUS',
        addressStatus: status
    }
}

export const requestCheckOut = () => {
    return {
        type: 'REQUEST_CHECKOUT',
        checkOutLoading: true
    }
}

export const requestCheckOutGet = (params) => {
    return function (dispatch) {
        dispatch(requestCheckOut());
        
        const session = getSession();

        const request = Axios({
            method: 'GET',
            url: Constant.api_url + 'shopping/checkout?session=' + session,
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                response.data.result.readOnlys = {
                    'shipping_address' : response.data.result.shipping_address,
                    'billing_address' : response.data.result.billing_address
                };

                response.data.result.carts.forEach(function(checkOut, index){
                    if (checkOut.product_id === '1') {
                        response.data.result.consultant_fee = checkOut.price;
                        delete response.data.result.carts[index];
                    }
                    checkOut.quantity = Number(checkOut.quantity);
                    checkOut.quantity_price = (checkOut.price * checkOut.quantity);
                    checkOut.quantity_market_price = (checkOut.market_price * checkOut.quantity);
                });

                dispatch(requestCheckOutSuccess(response.data));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession());
                }
                else {
                    dispatch(requestCheckOutError(response.data));
                }
            }
        });
    }
}

export const requestCheckOutSuccess = (data) => {
    return {
        type: 'REQUEST_CHECKOUT_SUCCESS',
        checkOutList: data,
        checkOutStatus: data.status,
        checkOutLoading: false,
        checkOutError: false,
        checkOutMessage: 'Welcome',
    }
}

export const requestCheckOutError = (data) => {
    return {
        type: 'REQUEST_CHECKOUT_ERROR',
        checkOutList: [],
        checkOutStatus: data.status,
        checkOutLoading: true,
        checkOutError: true,
        checkOutMessage: 'Error',
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const setCouponStatus = (status) => {
    return {
        type: 'SET_COUPON_STATUS',
        couponStatus: status
    }
}

export const requestApplyCoupon = (applied_code, new_code, type) => {
    return function (dispatch) {

        const session = getSession();

        if (new_code !== '') {
            applied_code = applied_code.concat(new_code);
        }

        const request = Axios({
            method: 'GET',
            url: Constant.api_url + 'shopping/checkout?session=' + session + '&coupon=' + JSON.stringify(applied_code),
        });

        return request.then(function(response){
            if(response.data.status === 1) {

                response.data.result.readOnlys = {
                    'shipping_address' : response.data.result.shipping_address,
                    'billing_address' : response.data.result.billing_address
                };

                var coupon = false;

                if (type === 'remove') {
                    response.data.result.coupon_remove = true;
                }

                if (response.data.result.coupon) {
                    response.data.result.coupon.forEach(function(data, index){
                        if (data.coupon_error === '') {
                            coupon = true;
                        }
                        else {
                            coupon = false;
                            delete response.data.result.coupon[index];
                        }
                    });
                }

                response.data.result.coupon_apply = coupon;

                response.data.result.carts.forEach(function(checkOut, index){
                    if (checkOut.product_id === '1') {
                        response.data.result.consultant_fee = checkOut.price;
                        delete response.data.result.carts[index];
                    }
                    checkOut.quantity = Number(checkOut.quantity);
                    checkOut.quantity_price = (checkOut.price * checkOut.quantity);
                    checkOut.quantity_market_price = (checkOut.market_price * checkOut.quantity);
                });


                dispatch(requestApplyCouponSuccess(response.data));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession());
                }
                else {
                    dispatch(requestApplyCouponError(response.data));
                }
            }
        });
    }
}

export const requestApplyCouponSuccess = (data) => {
    return {
        type: 'REQUEST_APPLY_COUPON_SUCCESS',
        checkOutList: data,
        checkOutStatus: data.status,
        checkOutError: false,
        couponApply: data.result.coupon_apply,
        couponRemove: data.result.coupon_remove
    }
}

export const requestApplyCouponError = (data) => {
    return {
        type: 'REQUEST_APPLY_COUPON_ERROR',
        checkOutList: [],
        checkOutStatus: data.status,
        checkOutError: true,
        checkOutMessage: 'Error',
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const requestGetState = () => {
    return {
        type: 'REQUEST_GET_STATE',
    }
}

export const requestGetStateSuccess = (data) => {
    return {
        type: 'REQUEST_GET_STATE_SUCCESS',
        stateList: data,
        status: data.status,
    }
}

export const requestGetStatetError = (data) => {
    return {
        type: 'REQUEST_GET_STATE_ERROR',
        stateList: [],
        status: data.status,
        message: 'Error',
    }
}

export const getState = () => {
    return function (dispatch) {
        dispatch(requestGetState());

        const request = Axios({
            method: 'GET',
            url: Constant.api_url + 'general/get_state',
        });
        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestGetStateSuccess(response.data));
            }
            else if(response.data.status === 0) {
                dispatch(requestGetStatetError(response.data));
            }
        });
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export function setAddressData(newAddress, data, type) {

    if(type === 1) {
        data.result.shipping_address = newAddress;
    }
    else {
        data.result.billing_address = newAddress;
    }

    return { type: 'REQUEST_SET_SHIPPING_ADDRESS', checkOutList: data }
}

export const requestChangeAddressSuccess = (data) => {
    return {
        type: 'REQUEST_CHANGE_ADDRESS_SUCCESS',
        addressStatus: data.status,
        message: 'Address Successful Updated',
    }
}

export const requestChangeAddressError = (data) => {
    return {
        type: 'REQUEST_CHANGE_ADDRESS_ERROR',
        addressStatus: data.status,
        message: data.validation,
    }
}

export const postChangeAddress = (params, type) => {

    return function (dispatch) {

        const session = getSession();

        const formData = new FormData();

        if(type === 1) {
            formData.append('type', 'shipping');
        }
        else {
            formData.append('type', 'billing');
        }

        formData.append('address_1', params.address_1 ? params.address_1 : '');
        formData.append('address_2', params.address_2 ? params.address_2 : '');
        formData.append('postcode', params.address_postcode ? params.address_postcode : '');
        formData.append('state', params.address_stateid ? params.address_stateid : '');
        formData.append('country', Constant.country);
        formData.append('session', session);

        const request = Axios({
            method: 'POST',
            url: Constant.api_url + 'shopping/add_address',
            data: formData
        });

        return request.then(function(response){
            if(response.data.status === 1) {              
                dispatch(requestCheckOutGet());
                dispatch(requestChangeAddressSuccess(response.data));
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession(1));
                }
                else {
                    dispatch(requestChangeAddressError(response.data));
                }
            }
        });
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const setProceedStatus = (status) => {
    return {
        type: 'SET_PROCEED_STATUS',
        proceedStatus: status
    }
}

export const requestProceedCheckOutSuccess = (data) => {
    return {
        type: 'REQUEST_PRPCEED_CHECKOUT_SUCCESS',
        proceedList: data,
        proceedStatus: data.status,
        message: 'Success',
    }
}

export const requestProceedCheckOutError = (data) => {
    var message = 'Error';
    if (data.message === 'SHIPPING_ADDRESS_EMPTY') {
        message = 'Shipping Address Empty';
    }
    if (data.message === 'BILLING_ADDRESS_EMPTY') {
        message = 'Billing Address Empty';
    }
    return {
        type: 'REQUEST_PRPCEED_CHECKOUT_ERROR',
        proceedList: [],
        proceedStatus: data.status,
        message: message,
    }
}

export const postProceedCheckOut = (code) => {
    return function (dispatch) {
        
        const session = getSession();

        const formData = new FormData();

        formData.append('confirm', 1);
        formData.append('session', session);

        if (code !== '') {
            formData.append('coupon', JSON.stringify(code));
        }

        const request = Axios({
            method: 'POST',
            url: Constant.api_url + 'shopping/checkout_confirm',
            data: formData
        });

        return request.then(function(response){
            if(response.data.status === 1) {
                dispatch(requestProceedCheckOutSuccess(response.data));
                dispatch(getTotalCart());
            }
            else if(response.data.status === 0) {
                if (response.data.message === 'SESSION_EXPIRED') {
                    dispatch(destoryPatientSession());
                }
                else {
                    dispatch(requestProceedCheckOutError(response.data));
                }
            }
        });
    
    }
}