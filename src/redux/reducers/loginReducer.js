import { toast } from 'react-toastify';
import { setSession }  from '../../utils/auth';
import { errorConvert }  from '../../utils/errorMessage';

const initState = {
    loginStatus: false,
    loginError: false,
    loginMessage: '',
    patientSession: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'SET_STATUS':
            return {
                ...state,
                loginStatus: action.loginStatus,
            };
        case 'REQUEST_LOGIN':
            return {
                ...state
            };
        case 'REQUEST_LOGIN_SUCCESS':
            toast.success(action.loginMessage, {
                position: toast.POSITION.TOP_CENTER
            });

            setSession(action.patientSession);

            return {
                ...state,
                loginStatus: action.loginStatus,
                loginError: action.loginError,
                loginMessage: action.loginMessage
            };
        case 'REQUEST_LOGIN_ERROR':
            let error = errorConvert(action.loginMessage);
            error.forEach(function(data, index){
                toast.error(data, {
                    position: toast.POSITION.TOP_CENTER
                });
            });

            return {
                ...state,
                loginStatus: action.loginStatus,
                loginError: action.loginError,
                loginMessage: action.loginMessage
            };
        default:
            return state;
    }
}