import { toast } from 'react-toastify';

const initState = {
    prescriptionRecordList: [],
    prescriptionRecordStatus: false,
    prescriptionRecordLoading: true,
    prescriptionRecordError: false,
    prescriptionRecordMessage: '',

    prescriptionRecordDetailList: [],
    prescriptionRecordDetailStatus: false,
    prescriptionRecordDetailLoading: true,
    prescriptionRecordDetailError: false,
    prescriptionRecordDetailMessage: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'REQUEST_PRESCRIPTION_RECORD':
            return {
                ...state,
            };
        case 'REQUEST_PRESCRIPTION_RECORD_SUCCESS':
            return {
                ...state,
                prescriptionRecordList: action.prescriptionRecordList,
                prescriptionRecordStatus: action.prescriptionRecordStatus,
                prescriptionRecordLoading: action.prescriptionRecordLoading,    
                prescriptionRecordError: action.prescriptionRecordError,
                prescriptionRecordMessage: action.prescriptionRecordMessage
            };
        case 'REQUEST_PRESCRIPTION_RECORD_ERROR':
            toast.error(action.prescriptionRecordMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                prescriptionRecordList: action.prescriptionRecordList,
                prescriptionRecordStatus: action.prescriptionRecordStatus,
                prescriptionRecordLoading: action.prescriptionRecordLoading,    
                prescriptionRecordError: action.prescriptionRecordError,
                prescriptionRecordMessage: action.prescriptionRecordMessage
            };
        case 'REQUEST_PRESCRIPTION_RECORD_DETAIL_SUCCESS':
            return {
                ...state,
                prescriptionRecordDetailList: action.prescriptionRecordList,
                prescriptionRecordDetailStatus: action.prescriptionRecordStatus,
                prescriptionRecordDetailLoading: action.prescriptionRecordLoading,    
                prescriptionRecordDetailError: action.prescriptionRecordError,
                prescriptionRecordDetailMessage: action.prescriptionRecordMessage
            };
        case 'REQUEST_PRESCRIPTION_RECORD_DETAIL_ERROR':
            toast.error(action.prescriptionRecordMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                prescriptionRecordDetailList: action.prescriptionRecordDetailList,
                prescriptionRecordDetailStatus: action.prescriptionRecordDetailStatus,
                prescriptionRecordDetailLoading: action.prescriptionRecordDetailLoading,    
                prescriptionRecordDetailError: action.prescriptionRecordDetailError,
                prescriptionRecordDetailMessage: action.prescriptionRecordDetailMessage
            };
        default:
            return state;
    }
}