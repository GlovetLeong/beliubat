import { toast } from 'react-toastify';

const initState = {
    addCartStatus: false,
    cartLoading: false,
    addCartError: false,
    addCartMessage: '',

    cartList: [],
    cartStatus: false,
    cartError: false,
    cartMessage: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'REQUEST_ADD_CART_BY_DETAIL_SUCCESS':
            toast.success(action.addCartMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                addCartStatus: action.addCartStatus,
                addCartError: action.addCartError,
                addCartMessage: action.addCartMessage
            };
        case 'REQUEST_ADD_CART_BY_DETAIL_ERROR':
            toast.error(action.addCartMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                addCartStatus: action.addCartStatus,
                addCartError: action.addCartError,
                addCartMessage: action.addCartMessage
            };
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'REQUEST_ADD_CART_BY_CLICK_SUCCESS':
            return {
                ...state,
                addCartStatus: action.addCartStatus,
                addCartError: action.addCartError,
                addCartMessage: action.addCartMessage
            };
        case 'REQUEST_ADD_CART_BY_CLICK_ERROR':
            toast.error(action.addCartMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                addCartStatus: action.addCartStatus,
                addCartError: action.addCartError,
                addCartMessage: action.addCartMessage
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'REQUEST_GET_CART_LIST':
            return {
                ...state,
                cartList: [],
                cartLoading: action.cartLoading
            };
        case 'REQUEST_GET_CART_LIST_SUCCESS':
            return {
                ...state,
                cartList: action.cartList,
                cartListStatus: action.cartListStatus,
                cartLoading: action.cartLoading,
                cartListError: action.cartListError,
                cartListMessage: action.cartListMessage
            };
        case 'REQUEST_GET_CART_LIST_ERROR':
            toast.error(action.cartListMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                cartList: action.cartList,
                cartListStatus: action.cartListStatus,
                cartLoading: action.cartLoading,
                cartListError: action.cartListError,
                cartListMessage: action.cartListMessage
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'REQUEST_SET_CART_LIST':  
            return {
                ...state,
                cartList: {
                    ...action.cartList,
                    
                }
            };
        default:
            return state;
    }
}