import { toast } from 'react-toastify';

const initState = {
    patientSession: '',
    patientSessionStatus: false,
    totalCart: 0,
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'GET_PATIENT_SESSION':
            return {
                ...state,
                patientSession: action.patientSession
            };
        case 'CHECK_PATIENT_SESSION':
            return {
                ...state,
                patientSessionStatus: action.patientSessionStatus
            };
         case 'DESTORY_PATIENT_SESSION':

            if (action.message === 'Logout') {
                toast.success(action.message, {
                    position: toast.POSITION.TOP_CENTER
                });
            }
            else {
                toast.error(action.message, {
                    position: toast.POSITION.TOP_CENTER
                });
            }
            
            return {
                ...state,
                patientSessionStatus: action.patientSessionStatus,
                patientSession: action.patientSession
            };
         case 'GET_TOTAL_CART':
            return {
                ...state,
                totalCart: action.totalCart
            };
        default:
            return state;
    }
}