import { toast } from 'react-toastify';

const initState = {
    categoryList: [],
    categoryStatus: false,
    categoryError: false,
    categoryMessage: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'REQUEST_CATEGORY':
            return {
                ...state,
            };
        case 'CLICK_ACTIVE':

            return {
                ...state,
                categoryList: {
                    ...action.categoryList,
                }
            };
         case 'REQUEST_CATEGORY_SUCCESS':
            return {
                ...state,
                categoryList: action.categoryList,
                categoryStatus: action.categoryStatus,
                categoryError: action.categoryError,
                categoryMessage: action.categoryMessage
            };
        case 'REQUEST_CATEGORY_ERROR':
            toast.error(action.categoryMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                categoryList: action.categoryList,
                categoryStatus: action.categoryStatus,
                categoryError: action.categoryError,
                categoryMessage: action.categoryMessage
            };
        default:
            return state;
    }
}