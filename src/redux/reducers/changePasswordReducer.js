import { toast } from 'react-toastify';
import { errorConvert }  from '../../utils/errorMessage';

const initState = {
    changePasswordStatus: false,
    changePasswordError: false,
    changePasswordMessage: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'SET_STATUS':
            return {
                ...state,
                changePasswordStatus: action.changePasswordStatus,
            };
        case 'REQUEST_CHANGE_PASSWORD':
            return {
                ...state,
            };
        case 'REQUEST_CHANGE_PASSWORD_SUCCESS':
            toast.success(action.changePasswordMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                changePasswordStatus: action.changePasswordStatus,
                changePasswordError: action.changePasswordError,
                changePasswordMessage: action.changePasswordMessage
            };
        case 'REQUEST_CHANGE_PASSWORD_ERROR':
            let error = errorConvert(action.changePasswordMessage);
            error.forEach(function(data, index){
                toast.error(data, {
                    position: toast.POSITION.TOP_CENTER
                });
            });
            return {
                ...state,
                changePasswordStatus: action.changePasswordStatus,
                changePasswordError: action.changePasswordError,
                changePasswordMessage: action.changePasswordMessage
            };
        default:
            return state;
    }
}