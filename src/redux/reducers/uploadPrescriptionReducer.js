import { toast } from 'react-toastify';
import { errorConvert }  from '../../utils/errorMessage';

const initState = {
    uploadPrescriptionStatus: false,
    uploadPrescriptionError: false,
    uploadPrescriptionMessage: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'SET_UPLOAD_PRESCRIPTION_STATUS':
            return {
                ...state,
                uploadPrescriptionStatus: action.uploadPrescriptionStatus,
            };
        case 'REQUEST_UPLOAD_PRESCRIPTION_SUCCESS':
            toast.success(action.uploadPrescriptionMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                uploadPrescriptionStatus: action.uploadPrescriptionStatus,
                uploadPrescriptionError: action.uploadPrescriptionError,
                uploadPrescriptionMessage: action.uploadPrescriptionMessage
            };
        case 'REQUEST_UPLOAD_PRESCRIPTION_ERROR':
            let error = errorConvert(action.uploadPrescriptionMessage);
            error.forEach(function(data, index){
                toast.error(data, {
                    position: toast.POSITION.TOP_CENTER
                });
            });
            return {
                ...state,
                uploadPrescriptionStatus: action.uploadPrescriptionStatus,
                uploadPrescriptionError: action.uploadPrescriptionError,
                uploadPrescriptionMessage: action.uploadPrescriptionMessage
            };
        default:
            return state;
    }
}