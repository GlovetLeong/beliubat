import { toast } from 'react-toastify';

const initState = {
    hotItemListHorizontal: [],
    hotItemListHorizontalStatus: false,
    hotItemListHorizontalLoading: true,
    hotItemListHorizontalError: false,
    hotItemListHorizontalMessage: '',

    itemDetail: [],
    itemDetailStatus: false,
    itemDetailError: false,
    itemDetailMessage: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'REQUEST_SET_ITEM_LIST_DATA':  
            return {
                ...state,
                itemList: action.data,
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'REQUEST_HOT_ITEM_LIST':
            return {
                ...state,
                itemList: [],
                itemListLoading: action.itemListLoading,
            };
        case 'REQUEST_HOT_ITEM_LIST_HORIZONTAL_SUCCESS':
            return {
                ...state,
                hotItemListHorizontal: action.itemList,
                hotItemListHorizontalStatus: action.itemListStatus,
                hotItemListHorizontalLoading: action.itemListLoading,
                hotItemListHorizontalError: action.itemListError,
                hotItemListHorizontalMessage: action.itemListMessage
            };
        case 'REQUEST_HOT_ITEM_LIST_HORIZONTAL_ERROR':
            toast.error(action.profileMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                hotItemListHorizontal: action.itemList,
                hotItemListHorizontalStatus: action.itemListStatus,
                hotItemListHorizontalLoading: action.itemListLoading,
                hotItemListHorizontalError: action.itemListError,
                hotItemListHorizontalMessage: action.itemListMessage
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        default:
            return state;
    }
}
