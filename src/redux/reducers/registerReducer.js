import { toast } from 'react-toastify';
import { errorConvert }  from '../../utils/errorMessage';

const initState = {
    registerStatus: false,
    registerError: false,
    registerMessage: '',
    registerntSession: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'SET_STATUS':
            return {
                ...state,
                registerStatus: action.registerStatus,
            };
        case 'REQUEST_REGISTER':
            return {
                ...state,
            };
        case 'REQUEST_REGISTER_SUCCESS':
            return {
                ...state,
                registerStatus: action.registerStatus,
                registerError: action.registerError,
                registerMessage: action.registerMessage
            };
        case 'REQUEST_REGISTER_ERROR':
            let error = errorConvert(action.registerMessage);
            error.forEach(function(data, index){
                toast.error(data, {
                    position: toast.POSITION.TOP_CENTER
                });
            });
            return {
                ...state,
                registerStatus: action.registerStatus,
                registerError: action.registerError,
                registerMessage: action.registerMessage
            };
        default:
            return state;
    }
}