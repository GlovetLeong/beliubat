const initState = {
    message: 'Sample Reducer'
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'SET_SAMPLE':
            return {
                ...state,
                message: action.message
            };
        default:
            return state;
    }
}