import { toast } from 'react-toastify';

const initState = {
    paymentResultList: [],
    paymentResultStatus: false,
    paymentResultLoading: true,
    paymentResultError: false,
    paymentResultMessage: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'REQUEST_PAYMENT_RESULT':
            return {
                ...state,
            };
        case 'REQUEST_PAYMENT_RESULT_SUCCESS':
            return {
                ...state,
                paymentResultList: action.paymentResultList,
                paymentResultStatus: action.paymentResultStatus,
                paymentResultLoading: action.paymentResultLoading,    
                paymentResultError: action.paymentResultError,
                paymentResultMessage: action.paymentResultMessage
            };
        case 'REQUEST_PAYMENT_RESULT_ERROR':
            toast.error(action.paymentResultMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                paymentResultList: action.paymentResultList,
                paymentResultStatus: action.paymentResultStatus,
                paymentResultLoading: action.paymentResultLoading,    
                paymentResultError: action.paymentResultError,
                paymentResultMessage: action.paymentResultMessage
            };
        default:
            return state;
    }
}