import { toast } from 'react-toastify';
import { errorConvert }  from '../../utils/errorMessage';

const initState = {
    stateList: [],
    stateStatus: false,
    stateError: false,
    stateMessage: '',
    couponApply: false,

    checkOutList: [],
    checkOutStatus: false,
    checkOutLoading: false,
    checkOutError: false,
    checkOutMessage: '',

    proceedList: [],
    proceedStatus: false,
    proceedError: false,
    proceedMessage: '',

    addressStatus: false,
    couponStatus: false
};

export default (state = initState, action) => {
    switch (action.type) {

        case 'REQUEST_GET_STATE':  
            return {
                ...state,
                stateList: []
            };

        case 'REQUEST_GET_STATE_SUCCESS':  
            return {
                ...state,
                stateList: action.stateList
            };
        
        case 'REQUEST_GET_STATE_ERROR':  
            return {
                ...state,
                stateList: {
                    ...action.stateList,
                }
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'SET_STATUS':
            return {
                ...state,
                checkOutStatus: action.checkOutStatus,
            };
        case 'REQUEST_CHECKOUT':
            return {
                ...state,
                checkOutLoading: action.checkOutLoading
            };
        case 'REQUEST_CHECKOUT_SUCCESS':
            return {
                ...state,
                checkOutList: action.checkOutList,
                checkOutStatus: action.checkOutStatus,
                checkOutLoading: action.checkOutLoading,
                checkOutError: action.checkOutError,
                checkOutMessage: action.checkOutMessage
            };
        case 'REQUEST_CHECKOUT_ERROR':
            toast.error(action.checkOutMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                checkOutList: action.checkOutList,
                checkOutStatus: action.checkOutStatus,
                checkOutLoading: action.checkOutLoading,
                checkOutError: action.checkOutError,
                checkOutMessage: action.checkOutMessage
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        case 'SET_COUPON_STATUS':
            return {
                ...state,
                couponStatus: action.couponStatus,
            };

        case 'REQUEST_APPLY_COUPON_SUCCESS':
            if (action.couponRemove === true) {
                toast.success('Coupon Removed', {
                    position: toast.POSITION.TOP_CENTER
                });
            }
            else if (action.couponApply === true) {
                toast.success('Coupon Applied', {
                    position: toast.POSITION.TOP_CENTER
                });
            }
            else {
                toast.error('Coupon Code Not Valid', {
                    position: toast.POSITION.TOP_CENTER
                });
            }
            return {
                ...state,
                checkOutList: action.checkOutList,
                checkOutStatus: action.checkOutStatus,
                couponStatus: action.couponApply,

                checkOutError: action.checkOutError,
                checkOutMessage: action.checkOutMessage
            };
        case 'REQUEST_APPLY_COUPON_ERROR':
            toast.error(action.checkOutMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                checkOutList: action.checkOutList,
                checkOutStatus: action.checkOutStatus,
                couponStatus: false,
                checkOutError: action.checkOutError,
                checkOutMessage: action.checkOutMessage
            };


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'SET_ADDRESS_STATUS':
            return {
                ...state,
                addressStatus: action.addressStatus,
            };

        case 'REQUEST_SET_SHIPPING_ADDRESS':  
            return {
                ...state,
                checkOutList: {
                    ...action.checkOutList,
                }
            };

        case 'REQUEST_CHANGE_ADDRESS_SUCCESS':  
            toast.success(action.message, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                addressStatus: action.addressStatus
            };
        
        case 'REQUEST_CHANGE_ADDRESS_ERROR':  

            let error = errorConvert(action.message);
            error.forEach(function(data, index){
                toast.error(data, {
                    position: toast.POSITION.TOP_CENTER
                });
            });

            return {
                ...state,
                addressStatus: action.addressStatus
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'SET_PROCEED_STATUS':
            return {
                ...state,
                proceedStatus: action.proceedStatus,
            };

        case 'REQUEST_PRPCEED_CHECKOUT_SUCCESS':  
            toast.success(action.message, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                proceedList: action.proceedList,
                proceedStatus: action.proceedStatus,
            };
        
        case 'REQUEST_PRPCEED_CHECKOUT_ERROR':  
            toast.error(action.message, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                proceedList: action.proceedList,
                proceedStatus: '',
            };
        default:
            return state;
    }
}