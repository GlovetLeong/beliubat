import { toast } from 'react-toastify';
import { errorConvert }  from '../../utils/errorMessage';

const initState = {
    itemList: [],
    itemListStatus: false,
    itemListLoading: true,
    itemListError: false,
    itemListMessage: '',

    itemDetail: [],
    itemDetailStatus: false,
    itemDetailError: false,
    itemDetailMessage: '',

    itemReviewDetail: [],
    itemReviewStatus: false,
    itemReviewError: false,
    itemReviewMessage: '',

    itemReviewCreate: [],
    itemReviewCreateStatus: false,
    itemReviewCreateError: false,
    itemReviewCreateMessage: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'REQUEST_SET_ITEM_LIST_DATA':  
            return {
                ...state,
                itemList: action.data,
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'REQUEST_ITEM_LIST':
            return {
                ...state,
                itemList: [],
                itemListLoading: action.itemListLoading,
            };
        case 'REQUEST_ITEM_LIST_SUCCESS':
            return {
                ...state,
                itemList: action.itemList,
                itemListStatus: action.itemListStatus,
                itemListLoading: action.itemListLoading,
                itemListError: action.itemListError,
                itemListMessage: action.itemListMessage
            };
        case 'REQUEST_ITEM_LIST_ERROR':
            toast.error(action.profileMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                itemList: action.itemList,
                itemListStatus: action.itemListStatus,
                itemListLoading: action.itemListLoading,
                itemListError: action.itemListError,
                itemListMessage: action.itemListMessage
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'REQUEST_LOAD_MORE_SUCCESS':
            return {
                ...state,
                itemList: {
                    ...action.itemList
                }
            };
        case 'REQUEST_LOAD_MORE_ERROR':
            return {
                ...state,
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'REQUEST_ITEM_DETAIL':
            return {
                ...state,
                itemDetail: []
            };
        case 'REQUEST_ITEM_DETAIL_SUCCESS':
            return {
                ...state,
                itemDetail: action.itemDetail,
                itemDetailStatus: action.itemDetailStatus,
                itemDetailError: action.itemDetailError,
                itemDetailMessage: action.itemDetailMessage
            };
        case 'REQUEST_ITEM_DETAIL_ERROR':
            toast.error(action.itemDetailMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                itemDetail: action.itemDetail,
                itemDetailStatus: action.itemDetailStatus,
                itemDetailError: action.itemDetailError,
                itemDetailMessage: action.itemDetailMessage
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'REQUEST_ITEM_REVIEW_SUCCESS':
            return {
                ...state,
                itemReviewDetail: action.itemReviewDetail,
                itemReviewStatus: action.itemReviewStatus,
                itemReviewError: action.itemReviewError,
                itemReviewMessage: action.itemReviewMessage
            };
        case 'REQUEST_ITEM_REVIEW_ERROR':
            toast.error(action.itemReviewMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                itemReviewStatus: action.itemReviewStatus,
                itemReviewError: action.itemReviewError,
                itemReviewMessage: action.itemReviewMessage
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'SET_ITEM_REVIEW_STATUS_STATUS':
            return {
                ...state,
                itemReviewCreateStatus: action.itemReviewCreateStatus,
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'REQUEST_ITEM_REVIEW_CREATE_SUCCESS':
            toast.success(action.itemReviewCreateMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                itemReviewCreate: action.itemReviewCreateCreate,
                itemReviewCreateStatus: action.itemReviewCreateStatus,
                itemReviewCreateError: action.itemReviewCreateError,
                itemReviewCreateMessage: action.itemReviewCreateMessage
            };
        case 'REQUEST_ITEM_REVIEW_CREATE_ERROR':
            let error = errorConvert(action.itemReviewCreateMessage);
            error.forEach(function(data, index){
                toast.error(data, {
                    position: toast.POSITION.TOP_CENTER
                });
            });
            return {
                ...state,
                itemReviewCreateStatus: action.itemReviewCreateStatus,
                itemReviewCreateError: action.itemReviewCreateError,
                itemReviewCreateMessage: action.itemReviewCreateMessage
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        default:
            return state;
    }
}