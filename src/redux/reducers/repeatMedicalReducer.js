import { toast } from 'react-toastify';

const initState = {
    repeatMedicalList: [],
    repeatMedicalStatus: false,
    repeatMedicalLoading: true,
    repeatMedicalError: false,
    repeatMedicalMessage: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'REQUEST_REPEAT_MEDICAL':
            return {
                ...state,
            };
        case 'REQUEST_REPEAT_MEDICAL_SUCCESS':
            return {
                ...state,
                repeatMedicalList: action.repeatMedicalList,
                repeatMedicalStatus: action.repeatMedicalStatus,
                repeatMedicalLoading: action.repeatMedicalLoading,    
                repeatMedicalError: action.repeatMedicalError,
                repeatMedicalMessage: action.repeatMedicalMessage
            };
        case 'REQUEST_REPEAT_MEDICAL_ERROR':
            toast.error(action.repeatMedicalMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                repeatMedicalList: action.repeatMedicalList,
                repeatMedicalStatus: action.repeatMedicalStatus,
                repeatMedicalLoading: action.repeatMedicalLoading,    
                repeatMedicalError: action.repeatMedicalError,
                repeatMedicalMessage: action.repeatMedicalMessage
            };
        default:
            return state;
    }
}