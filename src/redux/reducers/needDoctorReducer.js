import { toast } from 'react-toastify';
import { errorConvert }  from '../../utils/errorMessage';

const initState = {
    needDoctorStatus: false,
    needDoctorError: false,
    needDoctorMessage: '',

    symptomsStatus: false,
    symptomsError: false,
    symptomsMessage: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'SET_SYMPOMTS_STATUS':
            return {
                ...state,
                symptomsStatus: action.symptomsStatus,
            };
        case 'REQUEST_SYMPTOMS_SUCCESS':
            toast.success(action.symptomsMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                symptomsStatus: action.symptomsStatus,
                symptomsError: action.symptomsError,
                symptomsMessage: action.symptomsMessage
            };
        case 'REQUEST_SYMPTOMS_ERROR':
            let error = errorConvert(action.symptomsMessage);
            error.forEach(function(data, index){
                toast.error(data, {
                    position: toast.POSITION.TOP_CENTER
                });
            });
            return {
                ...state,
                symptomsStatus: action.symptomsStatus,
                symptomsError: action.symptomsError,
                symptomsMessage: action.symptomsMessage
            };
        default:
            return state;
    }
}