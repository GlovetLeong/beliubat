import { toast } from 'react-toastify';

const initState = {
    consultantRecordList: [],
    consultantRecordStatus: false,
    consultantRecordLoading: true,
    consultantRecordError: false,
    consultantRecordMessage: '',

    consultantRecordDetailList: [],
    consultantRecordDetailStatus: false,
    consultantRecordDetailLoading: true,
    consultantRecordDetailError: false,
    consultantRecordDetailMessage: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'REQUEST_CONSULTANT_RECORD':
            return {
                ...state,
            };
        case 'REQUEST_CONSULTANT_RECORD_SUCCESS':
            return {
                ...state,
                consultantRecordList: action.consultantRecordList,
                consultantRecordStatus: action.consultantRecordStatus,
                consultantRecordLoading: action.consultantRecordLoading,    
                consultantRecordError: action.consultantRecordError,
                consultantRecordMessage: action.consultantRecordMessage
            };
        case 'REQUEST_CONSULTANT_RECORD_ERROR':
            toast.error(action.consultantRecordMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                consultantRecordList: action.consultantRecordList,
                consultantRecordStatus: action.consultantRecordStatus,
                consultantRecordLoading: action.consultantRecordLoading,    
                consultantRecordError: action.consultantRecordError,
                consultantRecordMessage: action.consultantRecordMessage
            };
        case 'REQUEST_CONSULTANT_RECORD_DETAIL':
            return {
                ...state,
                consultantRecordLoading: action.consultantRecordLoading,    
            };
        case 'REQUEST_CONSULTANT_RECORD_DETAIL_SUCCESS':
            return {
                ...state,
                consultantRecordDetailList: action.consultantRecordDetailList,
                consultantRecordDetailStatus: action.consultantRecordDetailStatus,
                consultantRecordDetailLoading: action.consultantRecordDetailLoading,    
                consultantRecordDetailError: action.consultantRecordDetailError,
                consultantRecordDetailMessage: action.consultantRecordDetailMessage
            };
        case 'REQUEST_CONSULTANT_RECORD_DETAIL_ERROR':
            toast.error(action.consultantRecordMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                consultantRecordDetailList: action.consultantRecordDetailList,
                consultantRecordDetailStatus: action.consultantRecordDetailStatus,
                consultantRecordDetailLoading: action.consultantRecordDetailLoading,    
                consultantRecordDetailError: action.consultantRecordDetailError,
                consultantRecordDetailMessage: action.consultantRecordDetailMessage
            };
        default:
            return state;
    }
}