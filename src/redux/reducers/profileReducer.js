import { toast } from 'react-toastify';
import { errorConvert }  from '../../utils/errorMessage';

const initState = {
    stateList: [],
    profileDetail: [],
    profileDetailStatus: false,
    profileDetailLoading: true,
    profileDetailError: false,
    profileDetailMessage: '',

    profileStatus: false,
    profileError: false,
    profileMessage: '',
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'REQUEST_SET_PROFILE_DETAIL_DATA':  
            return {
                ...state,
                profileDetail: action.data,
            };

        case 'REQUEST_GET_STATE':  
            return {
                ...state,
                stateList: []
            };

        case 'REQUEST_GET_STATE_SUCCESS':  
            return {
                ...state,
                stateList: action.stateList
            };
        
        case 'REQUEST_GET_STATE_ERROR':  
            return {
                ...state,
                stateList: {
                    ...action.stateList,
                }
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'REQUEST_PROFILE_DETAIL':
            return {
                ...state,
                profileDetail: [],
                profileDetailLoading: action.profileDetailLoading
            };
        case 'REQUEST_PROFILE_DETAIL_SUCCESS':
            return {
                ...state,
                profileDetail: action.profileDetail,
                profileDetailLoading: action.profileDetailLoading,
                // profileDetailStatus: action.profileDetailStatus,
                profileDetailError: action.profileDetailError,
                profileDetailMessage: action.profileDetailMessage
            };
        case 'REQUEST_PROFILE_DETAIL_ERROR':
            toast.error(action.profileDetailMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                profileDetail: action.profileDetail,
                profileDetailLoading: action.profileDetailLoading,
                // profileDetailStatus: action.profileDetailStatus,
                profileDetailError: action.profileDetailError,
                profileDetailMessage: action.profileDetailMessage
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'REQUEST_PROFILE_UPDATE_SUCCESS':
            toast.success(action.profileDetailMessage, {
                position: toast.POSITION.TOP_CENTER
            });
            return {
                ...state,
                profileDetail: action.profileDetail,
                profileDetailStatus: action.profileDetailStatus,
                profileDetailError: action.profileDetailError,
                profileDetailMessage: action.profileDetailMessage
            };
        case 'REQUEST_PROFILE_UPDATE_ERROR':
            let error = errorConvert(action.profileDetailMessage);
            error.forEach(function(data, index){
                toast.error(data, {
                    position: toast.POSITION.TOP_CENTER
                });
            });
            return {
                ...state,
                profileDetail: action.profileDetail,
                profileDetailStatus: action.profileDetailStatus,
                profileDetailError: action.profileDetailError,
                profileDetailMessage: action.profileDetailMessage
            };

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        case 'REQUEST_SET_SHIPPING_ADDRESS':  
            return {
                ...state,
                profileDetail: {
                    ...action.profileDetail,
                }
            };

        default:
            return state;
    }
}