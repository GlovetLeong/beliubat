import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import WrapperContainer from './redux/containers/wrapperContainer';

class App extends Component {
  render() {
    return (
		<div>
			<WrapperContainer />
		</div>
    );
  }
}

export default App;
