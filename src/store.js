import { createStore, combineReducers, applyMiddleware } from 'redux';
import sampleReducer from './redux/reducers/sampleReducer';
import wrapperReducer  from './redux/reducers/wrapperReducer';
import loginReducer  from './redux/reducers/loginReducer';
import registerReducer  from './redux/reducers/registerReducer';
import profileReducer  from './redux/reducers/profileReducer';
import changePasswordReducer  from './redux/reducers/changePasswordReducer';
import needDoctorReducer  from './redux/reducers/needDoctorReducer';
import uploadPrescriptionReducer  from './redux/reducers/uploadPrescriptionReducer';
import itemListReducer  from './redux/reducers/itemListReducer';
import hotItemListHorizontalReducer  from './redux/reducers/hotItemListHorizontalReducer';
import cartReducer  from './redux/reducers/cartReducer';
import checkOutReducer  from './redux/reducers/checkOutReducer';
import paymentResultReducer  from './redux/reducers/paymentResultReducer';
import categoryReducer  from './redux/reducers/categoryReducer';
import consultantRecordReducer  from './redux/reducers/consultantRecordReducer';
import prescriptionRecordReducer  from './redux/reducers/prescriptionRecordReducer';
import repeatMedicalReducer  from './redux/reducers/repeatMedicalReducer';

import thunk from 'redux-thunk';

const reducer = combineReducers({
    sampleReducer,
    wrapperReducer,
    loginReducer,
    registerReducer,
    profileReducer,
    changePasswordReducer,
    needDoctorReducer,
    uploadPrescriptionReducer,
    itemListReducer,
    hotItemListHorizontalReducer,
    cartReducer,
    checkOutReducer,
    paymentResultReducer,
    categoryReducer,
    consultantRecordReducer,
    prescriptionRecordReducer,
    repeatMedicalReducer,
    
});

const store = createStore(
    reducer,
    applyMiddleware(thunk)
);

export default store;
