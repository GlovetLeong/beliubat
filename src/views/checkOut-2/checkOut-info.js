import React, { Component } from 'react';
import { Redirect } from 'react-router';
import {
	Row
	} from 'reactstrap';
import CheckOutMap from './checkOut-map';
import CheckOutPriceMap from './checkOutPrice-map';
import AddressModal from '../profile-2/address-modal';

const lodash = require('lodash');

class CheckOutInfo extends Component {

	constructor(props) {
		super(props);

		this.state = {
			proceedStatus: false,
            modalAddress: false,
			code: []
		};

        this.handleChange = this.handleChange.bind(this);
        this.setShippingAddress = this.setShippingAddress.bind(this);
        this.setBillingAddress = this.setBillingAddress.bind(this);
        this.onSubmitChangeAddress = this.onSubmitChangeAddress.bind(this);
        this.copyShipping = this.copyShipping.bind(this);
        this.toggleModalAddress = this.toggleModalAddress.bind(this);
        this.forceToggle = this.forceToggle.bind(this);
        this.onSubmitCoupon = this.onSubmitCoupon.bind(this);
        this.appliedCoupon = this.appliedCoupon.bind(this);
        this.deleteCoupon = this.deleteCoupon.bind(this);
        this.onSubmitProceedCheckOut = this.onSubmitProceedCheckOut.bind(this);

        this.props.getState();
        this.props.requestCheckOutGet();
	}

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

	setShippingAddress(e) {
        let shippingAddress = Object.assign({}, this.props.checkOutList.result.shipping_address);
        shippingAddress[e.target.name] = e.target.value;
        this.props.setAddressData(shippingAddress, this.props.checkOutList, 1);
    }

    setBillingAddress(e) {
        let billingAddress = Object.assign({}, this.props.checkOutList.result.billing_address);
        billingAddress[e.target.name] = e.target.value;
        this.props.setAddressData(billingAddress, this.props.checkOutList, 2);
    }

    toggleModalAddress() {
        this.setState({modalAddress: !this.state.modalAddress});
    }

    forceToggle() {
        this.setState({modalAddress: false});
    }

    copyShipping() {
        var params = this.props.checkOutList.result.shipping_address;
        this.props.postChangeAddress(params, 2);
    }

	onSubmitChangeAddress(type){
        var params = {};
        if(type === 'shipping') {
            params = this.props.checkOutList.result.shipping_address;
            this.props.postChangeAddress(params, 1);
        }
        else if(type === 'billing') {
            params = this.props.checkOutList.result.billing_address;
            this.props.postChangeAddress(params, 2);
        }
    }

    onSubmitCoupon(code){
    	this.props.requestApplyCoupon(this.state.code, code);
    }

    appliedCoupon(code) {
        let coupon_code = this.state.code.concat(code);
        this.setState({ code: coupon_code })
    }

    deleteCoupon(index) {
        let coupon_code = this.state.code;
        coupon_code.splice(index, 1);
        this.setState({ code: coupon_code })
        this.props.requestApplyCoupon(coupon_code, '', 'remove');
    }

    onSubmitProceedCheckOut(){
    	const coupon = this.state.code;
        this.props.postProceedCheckOut(coupon);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.proceedStatus) {
            nextProps.setProceedStatus(0);
        }
        return null;
    }

	render() {
        const checkOutLoading = this.props.checkOutLoading;
        const checkOutList = lodash.get(this.props.checkOutList, 'result', []);

    	if (this.props.proceedStatus) {
    		const order_id = lodash.get(this.props.proceedList.result, 'order_id', '');
    		const payment_url = lodash.get(this.props.proceedList.result, 'payment_url', '');
    		return <Redirect
			to={{
				pathname: '/proceed-pay',
				search: '?order_id=' + order_id + '&payment_url=' + encodeURIComponent(payment_url),
			}}
			/>
		}

		return (
            <div>
                {
                    (checkOutLoading) ? 
                        <div className='text-center'>
                            <div className="lds-ripple">
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    :
        			<Row>
        				<CheckOutMap 
        					checkOutList={checkOutList}
                            couponStatus={this.props.couponStatus}
                            setCouponStatus={this.props.setCouponStatus}
        					onSubmitCoupon={this.onSubmitCoupon}
                            appliedCoupon={this.appliedCoupon}
                            deleteCoupon={this.deleteCoupon}
        				/>

                        <CheckOutPriceMap 
                            checkOutList={this.props.checkOutList}
                            toggleModalAddress={this.toggleModalAddress}
                            onSubmitProceedCheckOut={this.onSubmitProceedCheckOut}
                            copyShipping={this.copyShipping}
                        />

                        <AddressModal 
                            wrapper={this.props.wrapper}
                            modal={this.state.modalAddress}
                            toggle={this.toggleModalAddress}
                            forceToggle={this.forceToggle}
                            stateList={this.props.stateList}
                            shipping_address={checkOutList.shipping_address}
                            billing_address={checkOutList.billing_address}
                            addressStatus={this.props.addressStatus}
                            setAddressStatus={this.props.setAddressStatus}
                            setShippingAddress={this.setShippingAddress}
                            setBillingAddress={this.setBillingAddress}
                            onSubmitChangeAddress={this.onSubmitChangeAddress}
                        />

        			</Row>
                }
            </div>
		)
	}
}

export default CheckOutInfo;