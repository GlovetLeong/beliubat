import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt} from '@fortawesome/free-solid-svg-icons';

import {
	Row, Col,
Button} from 'reactstrap';

var lodash = require('lodash');

class CheckOutMap extends Component {
	constructor(props) {
		super(props);
		this.state = {
			coupon_code: ''
		};
        this.handleChange = this.handleChange.bind(this);
	}

	handleChange(e) {
    	this.setState({ [e.target.name]: e.target.value });
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.couponStatus) {
            nextProps.setCouponStatus(0);
            nextProps.appliedCoupon(prevState.coupon_code);
            return { coupon_code: ''};
        }
        return null;
    }

	render() {
        const checkOutList = lodash.get(this.props.checkOutList, 'carts', []);
		const coupon_list = lodash.get(this.props.checkOutList, 'coupon', []); 

		const shipping_cost = lodash.get(this.props.checkOutList, 'shipping_cost', 0); 

		return (
			<Col className='cart-list-box' md='9'>
				<Col className='cart-list-header' md='12'>
					<Row>
						<Col md='7'>
							Item
						</Col>
						<Col md='3'>
							Quantity
						</Col>
						<Col md='2'>
							Price
						</Col>
					</Row>
				</Col>
				{
					checkOutList.map((checkOut, index) =>
						<Col className='cart-list-content white my-2' md='12' key={index}>
							<Row>
		       					<Col md='7'>
		       						<Row>
		       							<Col md='4' className='text-center'>
											<img className='img-fluid cart-img' alt='' src={checkOut.image} />
		       							</Col>
		       							<Col md='8'>
	       									<Col md='12' className='product-name-box mb-2'>
												<span className='product-name'>
													{checkOut.name}
												</span>
											</Col>
											<Col md='12' className='product-category mb-3'>
												<div className='category'>
													<span className='spacing-right'>{checkOut.type}</span>
													{
														(checkOut.market_price !==  checkOut.price) ?
				                                            <span className='product-discount'>- {checkOut.discount}</span>
														:
															<span></span>
													}
												</div>
											</Col>
											<Col md='12' className='line'></Col>
											<div className='mb-3'>
												<Row>
													<Col className='mb-2' md='12'>
														<div className='volume-code'>
															Dosage : <span className='volume-value'>{checkOut.sub_uom}</span>
														</div>
													</Col>
													<Col className='mb-2' md='12'>
														<div className='volume-code'>
															Strength: <span className='volume-value'>{checkOut.uom}</span>
														</div>
													</Col>
												</Row>
											</div>
		       							</Col>
		       						</Row>
								</Col>
								<Col className='mb-3' md='3'>
									<Row>
										<Col md='12' xs='12'>
											<input name='quantity' type='text' className='quantity-txt form-control' value={checkOut.quantity} disabled/>
										</Col>
									</Row>
								</Col>
								<Col className='text-center' md='2'>
									{
										(checkOut.market_price !==  checkOut.price) ?
											<div className='market-price'>RM {checkOut.quantity_market_price.toFixed(2)}</div>
										:
											<div></div>
									}
									<div className='current-price'>RM {checkOut.quantity_price.toFixed(2)}</div>
								</Col>
							</Row>
						</Col>
					)
				}

				<Col className='cart-list-content white my-2' md='12'>
					<Row>
						<Col md='6'></Col>
						<Col className='check-total' md='3'>
							Shipping Cost
						</Col>
						<Col md='1'></Col>
						<Col className='check-total' md='2'>
	                    	RM {Number(shipping_cost).toFixed(2)}
						</Col>
					</Row>
				</Col>

				<div>
					{
						(this.props.checkOutList.consultant_fee) ?
							<Col className='cart-list-content white my-2' md='12'>
								<Row>
									<Col md='6'></Col>
									<Col className='check-total' md='3'>
										Consultant Fee
									</Col>
									<Col md='1'></Col>
									<Col className='check-total' md='2'>
				                    	RM {this.props.checkOutList.consultant_fee}
									</Col>
								</Row>
							</Col>
						:
						<span></span>
					}
				</div>

				{
					(this.props.checkOutList.coupon) ?
						coupon_list.map((coupon, index) =>
							<Col className='cart-list-content white my-2' md='12' key={index}>
								<Row>
									<Col md='6'></Col>
									<Col className='check-total' md='3'>
										{coupon.coupon_code}
									</Col>
									<Col md='1'
										 className='btn btn-danger'
										 onClick={() => this.props.deleteCoupon(index)}
									>
										<FontAwesomeIcon icon={faTrashAlt} />
									</Col>
									<Col className='check-total' md='2'>
										-RM {Number(coupon.coupon_discount_total).toFixed(2)}
									</Col>
								</Row>
							</Col>
						)
				:
					<div></div>
				}

				<Col className='cart-list-content white my-2' md='12'>
					<Row>
						<Col md='6'></Col>
						<Col className='check-total' md='3'>
							<div className='form-group'>
								<input name='coupon_code' type='text' className='form-control' placeholder='Coupon Code' value={this.state.coupon_code} onChange={this.handleChange}/>
							</div>
						</Col>
						<Col md='1'></Col>
						<Col className='check-total' md='2'>
							<Button color='primary' onClick={() => this.props.onSubmitCoupon(this.state.coupon_code)} >Apply</Button>
						</Col>
					</Row>
				</Col>

				<Col className='cart-list-content white my-2' md='12'>
					<Row>
						<Col md='6'></Col>
						<Col className='check-total' md='3'>
							Total
						</Col>
						<Col md='1'></Col>
						<Col className='check-total' md='2'>
	                    	RM {Number(this.props.checkOutList.order_total).toFixed(2)}
						</Col>
					</Row>
				</Col>
			</Col>
		)
	}
}

export default CheckOutMap;