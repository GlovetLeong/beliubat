import React, { Component } from 'react';
import CheckOutContainer from '../../redux/containers/checkOutContainer';

class CheckOut extends Component {
	render() {
		return (
			<div className="mb-3">
				<div className='small-title mb-3'>
					CheckOut
				</div>
				<CheckOutContainer
					wrapper={this.props.wrapper}
				/>
			</div>
		)
	}
}

export default CheckOut;