import React, { Component } from 'react';
import {
	Row, Col,
Button} from 'reactstrap';

var lodash = require('lodash');

class CartPriceMap extends Component {

	constructor(props) {
		super(props);
		this.state = {
			modalCheckOut: false
		};
		this.toggleCheckOut = this.toggleCheckOut.bind(this);
	}

	toggleCheckOut() {
		this.props.requestClickCheckOutGet();
		this.setState({
			modalCheckOut: !this.state.modalCheckOut
		});
	}

	render() {

		const checkOutList = lodash.get(this.props.checkOutList, 'result', '');
		const readOnly = lodash.get(checkOutList, 'readOnlys', '');

		const shippingAddress1 = lodash.get(readOnly.shipping_address, 'address_1', '');
        const shippingAddress2 = lodash.get(readOnly.shipping_address, 'address_2', '');
        const shippingPostcode = lodash.get(readOnly.shipping_address, 'address_postcode', '');
        const shippingState = lodash.get(readOnly.shipping_address, 'address_state', '');

        const billingAddress1 = lodash.get(readOnly.billing_address, 'address_1', '');
        const billingAddress2 = lodash.get(readOnly.billing_address, 'address_2', '');
        const billingPostcode = lodash.get(readOnly.billing_address, 'address_postcode', '');
        const billingState = lodash.get(readOnly.billing_address, 'address_state', '');

		return (
			<Col className='checkout-box' md='3'>
				<div className='checkout-header mb-2'>Place Order</div>
				<div className="cart-list-content white with-radius">

					<Col className='my-2' md='12'>
						<div className="mb-2"><b>Shipping & Billing</b> | <small><button className='link-button' onClick={this.props.toggleModalAddress}>Edit</button ></small></div>
							<div>
								<span className="checkout-name">Ship To</span>
							</div>
							<div>
								{
									(shippingAddress1) ?
										<span>{shippingAddress1}, {shippingAddress2}, {shippingPostcode}, {shippingState}</span>
									:
										<span>-</span>
								}
							</div>
							<br/>
							<div>
								<span className="checkout-name">Bill To | <small><button className='link-button' onClick={this.props.copyShipping}>copy shipping</button></small></span>
							</div>
							<div>
								{
									(billingAddress1) ?
										<span>{billingAddress1}, {billingAddress2}, {billingPostcode}, {billingState}</span>
									:
										<span>-</span>
								}
							</div>
						<Col md='12' className='line'></Col>
					</Col>

					<Col className='my-2' md='12'>
						<Row>
							<Col className='checkout-name' md='7'>
								Total
							</Col>
							<Col className='checkout-price' md='5'>
								RM {checkOutList.order_total}
							</Col>
						</Row>
						<Col md='12' className='line'></Col>
					</Col>

					<Col className='my-2' md='12'>
						<Button color='primary' block onClick={this.props.onSubmitProceedCheckOut}>Proceed</Button>
					</Col>
				</div>
			</Col>
		)
	}
}

export default CartPriceMap;