import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingBag  } from '@fortawesome/free-solid-svg-icons';
import {
    Row, Col} from 'reactstrap';
import RepeatMedicalModal from './repeatMedical-modal';
import NeedDoctor from '../needDoctor/needDoctor';
import iconDOC from '../../images/doc.png';

var lodash = require('lodash');
var moment = require('moment');


class repeatMedicalMap extends Component {

   constructor(props) {
        super(props);
        this.state = {
            page: 1,
            modal: false,
            params: this.props.params,
            expried_date: ''
        }
        this.itemOpen = this.itemOpen.bind(this);
        this.toggle = this.toggle.bind(this);   
        this.dateConvert = this.dateConvert.bind(this);       

        this.props.requestRepeatMedicalGet(this.props.params);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    dateConvert(date) {
        // const dateformat = require('dateformat');
        // date = new Date();  
        // date.format('dd-m-yy');
        return date;
    }

    itemOpen(product_id, expried_date) {
        this.toggle();

        const params = {
            product_id: product_id
        }

        const params_2 = {
            id: product_id
        }

        this.setState({
            expried_date: expried_date
        });

        this.props.requestItemDetailGet(params);
        this.props.requestItemReviewDetailGet(params_2);
    }

    static getDerivedStateFromProps(nextProps, preState) {
        if (nextProps.params !== preState.params) {
            nextProps.requestItemListGet(nextProps.params);

             return {
                params: nextProps.params
            };
        }
        return null;
    }

    map(itemList) {
        return (
           itemList.map((item, index) =>
                <Col className='mb-3 item-outside-padding' xs='6' key={index}>
                {
                    (item.market_price !==  item.price && item.market_price > 0) ?
                        <span className='product-discount-mobile' xs='3'>- {item.discount}</span>
                    :
                        <span></span>
                }
                <div className="item-min-height white with-radius">
                    <Row>
                        <Col className='text-center' md='5'>
                            <img className='product-img img-fluid pointer' alt='' src={item.images[0].path} 
                                onClick={() => this.itemOpen(item.product_id, item.prescription_expiry_date)}
                            />
                        </Col>
                        <Col md='7'>
                            <Col md='12'>
                                <Row>
                                    <Col className='mb-3' md='12'>
                                        <Row>
                                            <Col md='8'>
                                                
                                                <div className='product-name-box mb-2'>
                                                    <span className='product-name pointer'
                                                        onClick={() => this.itemOpen(item.product_id, item.prescription_expiry_date)}
                                                    >
                                                        {item.name}
                                                    </span> 
                                                </div>
                                            </Col>
                                            <Col className='doc-box item-ask' md='4'>
                                                {
                                                    (item.type === 'Rx Product') ?
                                                        <NeedDoctor
                                                            img={iconDOC}
                                                            wrapper={this.props.wrapper}
                                                        />
                                                    :
                                                        <span></span>
                                                }
                                                
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col className='price-con' md='12'>
                                        <Row>
                                            <Col className='price-box' md='8' xs='12'>
                                                <Row>
                                                    {
                                                        (item.market_price !==  item.price && item.market_price > 0) ?
                                                            <Col className='market-price' md='6'>
                                                                RM {item.market_price}
                                                            </Col>
                                                        :
                                                            <div></div>
                                                    }
                                                    {
                                                        (item.market_price !==  item.price && item.market_price > 0) ?
                                                            <Col className='product-discount text-center' md='4'>- {item.discount}</Col>
                                                        :
                                                            <div></div>
                                                    }
                                                </Row>
                                                <div className='current-price'>RM {item.price}</div>
                                            </Col>
                                            <Col className='cart-out-box text-center' md='4' xs='12'>
                                                <div className='item-detail-cart btn btn-primary'
                                                    onClick={() => this.itemOpen(item.product_id, item.prescription_expiry_date)}
                                                >
                                                    <FontAwesomeIcon icon={faShoppingBag} />
                                                </div>
                                            </Col>
                                            <Col xs='12' className='expried-date red'><b>Expried in { moment(item.prescription_expiry_date).format('YYYY-MMM-DD') }</b></Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </Col>
                        </Col>
                    </Row>
                </div>
                </Col>
            )
        );
    }

    render() {
        const repeatMedicalLoading = this.props.repeatMedicalLoading;
        const repeatMedicalList = lodash.get(this.props.repeatMedicalList, 'result', []);

        return (
            <div>
                {
                    (repeatMedicalLoading) ?
                        <div className='text-center'>
                            <div className="lds-ripple">
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    :
                    (repeatMedicalList.length > 0) ?
                    <Row>
                        {this.map(repeatMedicalList)}
                        <RepeatMedicalModal
                            wrapper={this.props.wrapper}
                            modal={this.state.modal}
                            toggle={this.toggle}
                            itemDetail={this.props.itemDetail}
                            ExpriedDate={this.state.expried_date}
                            itemReviewDetail={this.props.itemReviewDetail}
                            requestCreateItemReview={this.props.requestCreateItemReview}
                            itemReviewCreateStatus={this.props.itemReviewCreateStatus}
                            setItemReviewCreateStatus={this.props.setItemReviewCreateStatus}
                            setQuantityPrice={this.props.setQuantityPrice}
                        />
                    </Row>
                    :
                    <div className='small-title m-3 text-center'>No Result Found</div>
                }
            </div>
        )
    }
}

export default repeatMedicalMap;