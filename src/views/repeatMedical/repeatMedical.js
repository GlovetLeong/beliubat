import React, { Component } from 'react';
import RepeatMedicalContainer from '../../redux/containers/repeatMedicalContainer';

class RepeatMedical extends Component {
	render() {
		return (
			<RepeatMedicalContainer
				wrapper={this.props.wrapper}
			/>
		)
	}
}

export default RepeatMedical;