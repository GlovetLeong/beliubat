import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faMinus } from '@fortawesome/free-solid-svg-icons';
import { faCartPlus } from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col,
	UncontrolledCarousel,
	Modal, 
	ModalHeader, 
	ModalBody} from 'reactstrap';
import AddCartContainer from '../../redux/containers/addCartContainer';
import ReviewMap from '../item/review-map';
import CountDown from '../count-down';

import NeedDoctor from '../needDoctor/needDoctor';
import iconDOC from '../../images/doc.png';

const lodash = require('lodash');

class RepeatMedicalModal extends Component {

	constructor(props) {
		super(props);
		this.state = {
			quantity: 1,
			count: ''
		};

        this.props.wrapper.getTotalCart();

        this.setQuantity = this.setQuantity.bind(this);
        this.setDefault = this.setDefault.bind(this);

        this.handleStart = this.handleStart.bind(this);
        this.handleStop = this.handleStop.bind(this);

        this.format = this.format.bind(this);

	}

	setQuantity(type) {
        if(type === 'more'){
            this.setState({quantity: this.state.quantity + 1});
        }
        else {
            if((this.state.quantity - 1) > 0) {
                this.setState({quantity: this.state.quantity - 1});
            }
        }
        this.props.setQuantityPrice(type, this.props.itemDetail, this.state.quantity);
    }

    setDefault() {
        this.setState({quantity: 1});
    }

	format(time) {

		let countDownDate = new Date(time).getTime();
		let now = new Date().getTime();
		let distance = countDownDate - now;

		let days = Math.floor(distance / (1000 * 60 * 60 * 24));
		let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		let seconds = Math.floor((distance % (1000 * 60)) / 1000);
		
		return days + 'days ' + hours + ':' + minutes + ':' + seconds;
	}

	handleStart(time) {
		let countDownDate = new Date(time).getTime();
		let now = new Date().getTime();
		let distance = countDownDate - now;

		setInterval(() => {
			const newCount = distance - 1;

			console.log(newCount);

			this.setState({ 
				count: newCount >= 0 ? newCount : 0 
			});
		}, 1000);
	}

	handleStop() {
		clearInterval();
	}



	render() {

        const itemDetail = this.props.itemDetail;
        const itemReviewDetail = this.props.itemReviewDetail;
        const ExpriedDate = this.props.ExpriedDate;

        // if (this.props.modal) {
        // 	this.handleStart(ExpriedDate);
        // }
        // else {
        // 	this.handleStop();
        // }

        const images = lodash.get(itemDetail, 'images', []);
        const items = []
        if (images !== [] ) {
			images.map((image, index) =>
				items.push({
					src: image.path,
					captionText: '',
					altText: '',
					caption: '',
					header: ''
				})
			)
        }

		return (
			<div>
				<Modal className='modal-lg' isOpen={this.props.modal} toggle={this.props.toggle}>
					<ModalHeader toggle={this.props.toggle}>
						<FontAwesomeIcon className='spacing-right' icon={faShoppingBag} /> 
						Products
					</ModalHeader>
					<ModalBody>
						<Row>
							<Col md='4'>
								<UncontrolledCarousel items={items} />
								{
									(ExpriedDate) ?
										<CountDown
										date={ExpriedDate}
									 />
									:
									<div></div>
								}
							</Col>
							<Col md='8'>
								<Col md='12'>
									<Row>
										<Col className='product-name-big my-3' md='12'>{itemDetail.name}</Col>
										<Col className='product-price mb-3' md='4'>
											<div className='category spacing-right'>{itemDetail.type}</div>
										</Col>
										<Col md='8'>
											<Row>
												<Col className='mb-2' md='6'>
													<div className='volume-code'>
														Dosage : <span className='volume-value'>{itemDetail.sub_uom}</span>
													</div>
												</Col>

												<Col className='mb-2' md='6'>
													<div className='volume-code'>
														Strength: <span className='volume-value'>{itemDetail.uom}</span>
													</div>
												</Col>
											</Row>
										</Col>
									</Row>
								</Col>
								<Col md='12' className='line'></Col>
								<Col md='12'>
									<Row>
										<Col className='mb-3' md='6'>
											<Row>
												<Col className='px-0 text-center' md='3' xs='3'>
													<div className='quantity-icon btn btn-primary'
														onClick={() => this.setQuantity('less')}
													>
														<FontAwesomeIcon icon={faMinus} />
													</div>
												</Col>
												<Col md='6' xs='6'>
													<input name='quantity' type='text' className='quantity-txt form-control' value={this.state.quantity} disabled/>
												</Col>
												<Col className='px-0 text-center' md='3' xs=''>
													<div className='quantity-icon btn btn-primary'
														onClick={() => this.setQuantity('more')}
													>
														<FontAwesomeIcon icon={faPlus} />
													</div>
												</Col>
											</Row>
										</Col>
										<Col className='cart-box text-center' md='6'>
											<Row>
												<Col className='mb-3' md='6'>
													{
														(itemDetail.market_price !==  itemDetail.price && itemDetail.market_price > 0) ?
															<div className='market-price'>RM {itemDetail.quantity_market_price}</div>
														:
															<div></div>
													}
													<div className='current-price'>RM {itemDetail.quantity_price}</div>
												</Col>
												<Col md='6'>
													<AddCartContainer
														wrapper={this.props.wrapper}
														icon={faCartPlus}
														itemDetail={itemDetail}
														toggle={this.props.toggle}
														setDefault={this.setDefault}
													/>
													<div><small>Add To Cart</small></div>
												</Col>
											</Row>
										</Col>
									</Row>
								</Col>
								<Col md='12' className='line'></Col>
								<Col md='12'>
									<ReviewMap 
										wrapper={this.props.wrapper}
										product_id={itemDetail.product_id}
										itemReviewDetail={itemReviewDetail}
										requestCreateItemReview={this.props.requestCreateItemReview}
										itemReviewCreateStatus={this.props.itemReviewCreateStatus}
										setItemReviewCreateStatus={this.props.setItemReviewCreateStatus}
									/>
								</Col>
								<Col md='12' className='line'></Col>
									{
										(itemDetail.type_id === '3') ?
											<div>
												<Col md='12'>
													<Row>
														<Col className='item-ask' md='2' xs='4'>
															<NeedDoctor
																img={iconDOC}
																wrapper={this.props.wrapper}
															/>
														</Col>

														<Col md='10' xs='8'>
															<div>This product requires a valid prescription.</div>
															<div>Upload your prescription or consult our doctor online if you don't have a prescription.</div>
														</Col>
													</Row>
												</Col>
												<Col md='12' className='line'></Col>
											</div>
										:
										<span></span>
									}
							</Col>
						</Row>

					</ModalBody>
				</Modal>
				
			</div>
		)
	}
}

export default RepeatMedicalModal;