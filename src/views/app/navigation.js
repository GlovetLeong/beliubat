import React, { Component } from 'react';
import { NavLink as RouteLink }  from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag  } from '@fortawesome/free-solid-svg-icons';
import { faNotesMedical } from '@fortawesome/free-solid-svg-icons';

import {
	Row, Col,
	Collapse,
	Navbar,
	NavbarToggler,
	Nav,
	NavItem,
	UncontrolledButtonDropdown, 
	DropdownMenu, 
	DropdownItem, 
	DropdownToggle
} from 'reactstrap';
import Login from '../login/login';

import logo_web from '../../images/logo_web.png';
import logo_mobile from '../../images/logo_mobile.png';

import icon_facebook from '../../images/icon_facebook.png';
import icon_instagram from '../../images/icon_instagram.png';


class Navigation extends Component {

	constructor(props) {
		super(props);

		this.state = {
			isOpen: false,
			onSearch: false,
			search_key: '/search'
		};

		this.handleChange = this.handleChange.bind(this);
		this.onSubmitSearch = this.onSubmitSearch.bind(this);
		this.handleKeyPress = this.handleKeyPress.bind(this);
		this.toggle = this.toggle.bind(this);
		this.toggleFalse = this.toggleFalse.bind(this);
        this.logOut = this.logOut.bind(this);

        this.props.wrapper.getTotalCart();
	}

	handleChange(e) {
    	this.setState({ [e.target.name]: '/search/' + e.target.value });
    }

    onSubmitSearch() {
    	this.props.onSearchHandle()
    }

	handleKeyPress(e) {
		if(e.key === 'Enter')
		{
			if(e.key === 'Enter')
			{
	    		window.location = this.state.search_key;
			}
		}
	}

	logOut() {
     	this.props.wrapper.destoryPatientSession();
    }


	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}

	toggleFalse() {
		this.setState({
			isOpen: false
		});
	}

	render() {

		return (
			<div>
				<Col className='upperHeader' xs='12'>
					<Row>
						<Col className='logo-box' sm='4' xs='4'>
							<RouteLink exact to='/'>
								<img className='logo-web img-fluid' alt='' src={logo_web} onClick={this.toggleFalse} />
								<img className='logo-mobile img-fluid' alt='' src={logo_mobile} onClick={this.toggleFalse} />
							</RouteLink>
						</Col>
						<Col className='search-box' sm='5' xs='8'>
							<RouteLink exact to={this.state.search_key} onClick={this.onSubmitSearch}>
								<span className='search-icon pointer'><FontAwesomeIcon icon={faSearch} /></span>
							</RouteLink>
							<input name='search_key' type='text' className='search-input form-control' placeholder='Search...' 
								onChange={this.handleChange} 
								onKeyPress={this.handleKeyPress}/>
						</Col>
						<Col className='icon-box' sm='3' xs='3'>
							{
								(!this.props.wrapper.patientSessionStatus) ?
									<Login
										text="Login"
										wrapper={this.props.wrapper}
									/>
								:
								<div>
									<UncontrolledButtonDropdown className="drop-down-menu icon-item">
										<DropdownToggle>
											<FontAwesomeIcon className='pointer' icon={faUserAlt} />
										</DropdownToggle>
										<DropdownMenu>
											<DropdownItem header>
												<RouteLink exact to='/manage-account'>
													Profile Info
												</RouteLink>
											</DropdownItem>
											<DropdownItem header>
												<RouteLink exact to='/payment/result'>
													Payment Result
												</RouteLink>
											</DropdownItem>
										</DropdownMenu>
									</UncontrolledButtonDropdown>
									<UncontrolledButtonDropdown className="drop-down-menu icon-item">
										<DropdownToggle>
											<FontAwesomeIcon className='pointer' icon={faNotesMedical} />
										</DropdownToggle>
										<DropdownMenu>
											<DropdownItem header>
												<RouteLink exact to='/consultant-record'>
													Consultant Record
												</RouteLink>
											</DropdownItem>
											<DropdownItem header>
												<RouteLink exact to='/prescription-record'>
													Prescription Record
												</RouteLink>
											</DropdownItem>
										</DropdownMenu>
									</UncontrolledButtonDropdown>
									<div className="icon-item">
										<RouteLink exact to='/cart'>
											<FontAwesomeIcon className='pointer' icon={faShoppingBag} />
										</RouteLink>
										<div className='cart-notification'>
											{this.props.wrapper.totalCart}
										</div>
									</div>
									<div className='icon-item'>
										<span className="pointer small" onClick={this.logOut}>Logout</span>
									</div>
								</div>
							}
						</Col>
					</Row>
				</Col>
				<Navbar className='lowerHeader' light expand='md'>
					<NavbarToggler onClick={this.toggle} />
					<Collapse isOpen={this.state.isOpen} navbar>
						<Nav className='mr-auto' navbar>
							<NavItem onClick={this.toggleFalse}>
								<RouteLink className='nav-link' exact to='/'>Home</RouteLink>
							</NavItem>
							<NavItem onClick={this.toggleFalse}>
								<RouteLink className='nav-link' exact to='/hot-items'>HotItems</RouteLink>
							</NavItem>
							<NavItem onClick={this.toggleFalse}>
								<RouteLink className='nav-link' exact to='/category'>Category</RouteLink>
							</NavItem>
							<NavItem onClick={this.toggleFalse}>
								<RouteLink className='nav-link' exact to='/about-us'>About Us</RouteLink>
							</NavItem>
						</Nav>

						<Nav className='ml-auto' navbar>
							{
								(!this.props.wrapper.patientSessionStatus) ?
								<div className='icon-box-mobile'>
									<Login
										text="Login"
										wrapper={this.props.wrapper}
									/>
								</div>
								:
								<div className='icon-box-mobile'>
									<UncontrolledButtonDropdown className="drop-down-menu icon-item">
										<DropdownToggle>
											<FontAwesomeIcon className='pointer' icon={faUserAlt} />
										</DropdownToggle>
										<DropdownMenu>
											<DropdownItem header>
												<RouteLink exact to='/manage-account' onClick={this.toggleFalse}>
													Profile Info
												</RouteLink>
											</DropdownItem>
											<DropdownItem header>
												<RouteLink exact to='/payment/result' onClick={this.toggleFalse}>
													Payment Result
												</RouteLink>
											</DropdownItem>
										</DropdownMenu>
									</UncontrolledButtonDropdown>
									<UncontrolledButtonDropdown className="drop-down-menu icon-item">
										<DropdownToggle>
											<FontAwesomeIcon className='pointer' icon={faNotesMedical} />
										</DropdownToggle>
										<DropdownMenu>
											<DropdownItem header>
												<RouteLink exact to='/consultant-record' onClick={this.toggleFalse}>
													Consultant Record
												</RouteLink>
											</DropdownItem>
											<DropdownItem header>
												<RouteLink exact to='/prescription-record' onClick={this.toggleFalse}>
													Prescription Record
												</RouteLink>
											</DropdownItem>
										</DropdownMenu>
									</UncontrolledButtonDropdown>
									<div className='icon-item'>
										<span className="pointer small" onClick={this.logOut}>Logout</span>
									</div>
									<div className="icon-item">
										<RouteLink exact to='/cart'>
											<FontAwesomeIcon className='pointer' icon={faShoppingBag} />
										</RouteLink>
										<div className='cart-notification'>
											{this.props.wrapper.totalCart}
										</div>
									</div>
								</div>
							}
							<NavItem>
								<img className='socal_icon pointer img-fluid' alt=''  src={icon_instagram} />
							</NavItem>
							<NavItem>
								<img className='socal_icon pointer img-fluid' alt=''  src={icon_facebook} />
							</NavItem>
							
						</Nav>
					</Collapse>
				</Navbar>
			</div>
		)
	}

}

export default Navigation;