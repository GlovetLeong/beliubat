import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import { Container } from 'reactstrap';

import Notification from './notification';
import Navigation from './navigation';
import AssistBar from './assist-bar'
import Content from './content';
import Footer from './footer';


class Default extends Component {

	constructor(props) {
		super(props);

		this.state = {
			onSearch: false
		};

		this.props.getPatientSession();
		this.props.checkPatientSession();
		this.onSearchHandle = this.onSearchHandle.bind(this);
	}


	onSearchHandle() {
		this.setState({
			onSearch: !this.state.onSearch
		});
	}

	render() {
		return (
			<BrowserRouter>
				<div>
					<Notification />
					<Navigation
						onSearchHandle={this.onSearchHandle}
						wrapper={this.props}
					/>

					<AssistBar
						wrapper={this.props}
					/>

					<Container>
						<Content 
							onSearchHandle={this.state.onSearchHandle}
							onSearch={this.state.onSearch}
							wrapper={this.props}
						/>
					</Container>

					<Footer 
						wrapper={this.props}
					/>

				</div>
			</BrowserRouter>
		)
	}
	
}

export default Default;