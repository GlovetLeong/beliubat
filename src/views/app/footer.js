import React, { Component } from 'react';
import { NavLink as RouteLink }  from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhoneVolume  } from '@fortawesome/free-solid-svg-icons';
import { faPrescription } from '@fortawesome/free-solid-svg-icons';
import { faSyncAlt} from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col,
} from 'reactstrap';
import NeedDoctor from '../needDoctor/needDoctor';
import UploadPrescription from '../uploadPrescription/uploadPrescription';
import logo_web from '../../images/logo_web.png';
import icon_facebook from '../../images/icon_facebook.png';
import icon_instagram from '../../images/icon_instagram.png';

class Footer extends Component {

	render() {
		return (
			<div>
				<div className='upper-footer-box' md='12'>
					<Col className='logo-box py-3' md='4' xs='4'>
						<img className='logo-web img-fluid' alt='' src={logo_web} />
					</Col>

					<Col className='footer-bar' md='8'>
						<Row>
							<Col className='py-3' md='12'>
								<div className='footer-call-doctor p-3'>
									<Row>
										<Col md='2'>
											<NeedDoctor
												icon={faPhoneVolume}
												wrapper={this.props.wrapper}
											/>
										</Col>
										<Col md='7'>
											<div>Need a Doctor?</div>
											<div>call us and solve your problem</div>
										</Col>
										<Col md='3'>
											<NeedDoctor
												text="Call Now"
												wrapper={this.props.wrapper}
											/>
										</Col>	
									</Row>
								</div>
							</Col>
							<Col className='py-3' md='6'>
								<div className='footer-upload-prescription p-3'>
									<Row>
										<Col md='2'>
											<UploadPrescription
												icon={faPrescription}
												wrapper={this.props.wrapper}
											/>
										</Col>
										<Col md='7'>
											<UploadPrescription
												text="Upload a prescription"
												wrapper={this.props.wrapper}
											/>
											
										</Col>
									</Row>
								</div>
							</Col>
							<Col className='py-3' md='6'>
								<div className='footer-repeat p-3'>
									<Row>
										<Col md='2'>
											<RouteLink exact to='/repeat-medical'>
												<div className='assist-icon pointer need-repeat'>
													<FontAwesomeIcon icon={faSyncAlt} />
												</div>
											</RouteLink>
										</Col>
										<Col md='7'>
											<RouteLink exact to='/repeat-medical'>
												Repeat your medical
											</RouteLink>
										</Col>
									</Row>
								</div>
							</Col>
						</Row>
					</Col>
				</div>
				<div className='bottom-footer-box p-3' md='12'>

					<Row>
						<Col className='footer-description text-center mb-3' md='12'>
							BeliUbat provides this medical information service in accordance with these terms and conditions.
							Please note that medical information found on this website is designed to support, 
							not to replace the relationship between patient and physician/doctor and the medical advice they may provide
						</Col>

						<Col className='footer-description text-center mb-3' md='12'>
							<RouteLink className='spacing-right' exact to='/return-refund-policy'>
								Return & Refund Policy
							</RouteLink>
							|
							<RouteLink className='spacing-left' exact to='/delivery-policy'>
								Delivery Policy
							</RouteLink>
						</Col>

						<Col className='footer-description-bottom text-center mb-3' md='6'>
							@2018 Owned and operated by BeliUbat
						</Col>

						<Col className='footer-description-bottom text-center mb-3' md='6'>
							<img className='socal_icon pointer img-fluid' alt=''  src={icon_instagram} />
							<img className='socal_icon pointer img-fluid' alt=''  src={icon_facebook} />
						</Col>
					</Row>
				</div>
			</div>
		)
	}
}

export default Footer;