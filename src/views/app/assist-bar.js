import React, { Component } from 'react';
import { NavLink as RouteLink }  from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhoneVolume  } from '@fortawesome/free-solid-svg-icons';
import { faPrescription } from '@fortawesome/free-solid-svg-icons';
import { faSyncAlt} from '@fortawesome/free-solid-svg-icons';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons';
import { faHome } from '@fortawesome/free-solid-svg-icons';

import {
	Row, Col,
	UncontrolledButtonDropdown,
	DropdownMenu, 
	DropdownItem,
	DropdownToggle
}from 'reactstrap';
import NeedDoctor from '../needDoctor/needDoctor';
import UploadPrescription from '../uploadPrescription/uploadPrescription';

class AssistBar extends Component {

	constructor(props) {
		super(props);
        this.scrollTop = this.scrollTop.bind(this);
	}

	scrollTop() {
		window.scrollTo(0, 0);
	}

	render() {
		return (
			<div>
				<div className='assist-bar text-center'>
					<Row>
						<Col md='12'>
							<Row>
								<Col className='assist-item' xs='4'>
									<NeedDoctor
										icon={faPhoneVolume}
										wrapper={this.props.wrapper}
									/>
									Need a Doctor?
								</Col>
								<Col className='assist-item' xs='4'>
									<UploadPrescription
										icon={faPrescription}
										wrapper={this.props.wrapper}
									/>
									Upload a Prescription
								</Col>
								<Col className='assist-item' xs='4'>
									<div className='assist-icon pointer need-repeat'>
										<RouteLink exact to='/repeat-medical'>
											<FontAwesomeIcon icon={faSyncAlt} />
										</RouteLink>
									</div>
									Repeat your Medical
								</Col>
							</Row>
						</Col>
					</Row>
				</div>

				<div className='mobile-assist-bar'>
					<div className="float-left mobile-assist-item">
						<RouteLink className='nav-link' exact to='/' onClick={this.scrollTop}><FontAwesomeIcon className='pointer' icon={faHome} /></RouteLink>
					</div>
					<div className="float-left mobile-assist-item">
						<NeedDoctor
							icon={faPhoneVolume}
							wrapper={this.props.wrapper}
						/>
					</div>
					<div className="float-left mobile-assist-item">
						<UploadPrescription
							icon={faPrescription}
							wrapper={this.props.wrapper}
						/>
					</div>
					<div className="float-left mobile-assist-item">
						<RouteLink exact to='/repeat-medical'>
							<FontAwesomeIcon icon={faSyncAlt} />
						</RouteLink>
					</div>
					<div className="float-left mobile-assist-item">
						<UncontrolledButtonDropdown tabIndex='1' className="drop-down-menu icon-item assist-icon">
							<DropdownToggle>
								<FontAwesomeIcon className='pointer' icon={faUserAlt} />
							</DropdownToggle>
							<DropdownMenu>
								<DropdownItem header>
									<RouteLink exact to='/manage-account' onClick={this.scrollTop}>
										Profile Info
									</RouteLink>
								</DropdownItem>
								<DropdownItem header>
									<RouteLink exact to='/payment/result' onClick={this.scrollTop}>
										Payment Result
									</RouteLink>
								</DropdownItem>
							</DropdownMenu>
						</UncontrolledButtonDropdown>
					</div>
				</div>
			</div>
		)
	}
}

export default AssistBar;