import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Home from '../home';
import HotItem from '../hot-item';
import Category from '../category/category';
import AboutUs from '../about-us';

import ProceedPay from '../proceed-pay';
import Search from '../search';
import Profile from '../profile-2/profile';

import ConsultantRecord from '../consultantRecord/consultantRecord';
import PrescriptionRecord from '../prescriptionRecord/prescriptionRecord';
import RepeatMedical from '../repeatMedical/repeatMedical';

import Cart from '../cart-2/cart';
import CheckOut from '../checkOut-2/checkOut';

import PaymentResult from '../paymentResult/paymentResult';

import ReturnRefund from '../return-refund';
import Delivery from '../delivery';
import Verify from '../verify';


class Content extends Component {

    constructor(props) {
        super(props);
        this.state = {
            homeItemCall: false,
            hotItemHorizontal: false,
            hotItemCall: false,
            categoryCall: false,
            searchCall: false,
        };
        this.toggleCall = this.toggleCall.bind(this);       
    }

    toggleCall(type) {

        if (type === 'home') {
            this.setState({
                homeItemCall: true,
                hotItemHorizontal: false,
                hotItemCall: false,
                categoryCall: false,
                search: false
            });
        }
        else if (type === 'hotHorizontal') {
            this.setState({
                hotItemHorizontal: true,
                // homeItemCall: false,
                hotItemCall: false,
                categoryCall: false,
            });
        }
        else if (type === 'hot') {
            this.setState({
                hotItemCall: true,
                homeItemCall: false,
                hotItemHorizontal: false,
                categoryCall: false,
                search: false
            });
        }
        else if (type === 'category') {
            this.setState({
                categoryCall: true,
                homeItemCall: false,
                hotItemHorizontal: false,
                hotItemCall: false,
                search: false
            });
        }
        else if (type === 'search') {
            this.setState({
                search: true,
                categoryCall: false,
                homeItemCall: false,
                hotItemHorizontal: false,
                hotItemCall: false,
            });
        }
    }

	render() {
        
		return (
			<Switch>
                <Route exact path='/' component={() => 
                	<Home 
                		wrapper={this.props.wrapper} 
                        itemCall={this.state.homeItemCall}
                        itemCall2={this.state.homeItemCall}
                        toggleCall={this.toggleCall}
                        type='home'
                        type2='hotHorizontal'
                    />}
                />

                <Route exact path='/hot-items' component={() => 
                    <HotItem 
                        wrapper={this.props.wrapper} 
                        itemCall={this.state.hotItemCall}
                        toggleCall={this.toggleCall}
                        type='hot'
                    />} 
                />

                <Route exact path='/category' component={() => 
                    <Category 
                        wrapper={this.props.wrapper} 
                        itemCall={this.state.categoryCall}
                        toggleCall={this.toggleCall}
                        type='category'
                    />} 

                />

                <Route exact path='/about-us' component={AboutUs} />

                <Route path='/proceed-pay' component={ProceedPay} />

                <Route exact strict path='/search/:search_key'  render={
                    ({match}) => (
                        (<Search search_key={match.params.search_key}
                            onSearchHandle={this.props.onSearchHandle}
                            onSearch={this.props.onSearch}
                            wrapper={this.props.wrapper}
                            itemCall={this.state.searchCall}
                            toggleCall={this.toggleCall}
                            type='search'
                         />
                         )
                    )
                } />

                <Route path='/manage-account' component={() =>
                    (!localStorage.getItem('patient-session')) ?
                        <Redirect to="/" />
                    :
                    <Profile 
                        wrapper={this.props.wrapper} />
                }/>

                <Route path='/payment/result' component={() =>
                    (!localStorage.getItem('patient-session')) ?
                        <Redirect to="/" />
                    :
                    <PaymentResult 
                        wrapper={this.props.wrapper} />
                }/>

                <Route path='/consultant-record' component={() =>
                    (!localStorage.getItem('patient-session')) ?
                        <Redirect to="/" />
                    :
                    <ConsultantRecord 
                        wrapper={this.props.wrapper} />
                }/>

                <Route path='/prescription-record' component={() =>
                    (!localStorage.getItem('patient-session')) ?
                        <Redirect to="/" />
                    :
                    <PrescriptionRecord 
                        wrapper={this.props.wrapper} />
                }/>

                <Route path='/repeat-medical' component={() =>
                    (!localStorage.getItem('patient-session')) ?
                        <Redirect to="/" />
                    :
                    <RepeatMedical 
                        wrapper={this.props.wrapper} />
                }/>

                <Route path='/cart' component={() =>
                    (!localStorage.getItem('patient-session')) ?
                        <Redirect to="/" />
                    :
                    <Cart 
                        wrapper={this.props.wrapper} />
                }/>

                <Route path='/checkout' component={() =>
                    (!localStorage.getItem('patient-session')) ?
                        <Redirect to="/" />
                    :
                    <CheckOut 
                        wrapper={this.props.wrapper} />
                }/>

                <Route path='/verify' component={Verify} />
                

               {/* <Route path='/verify'  render={
                    ({match}) => (
                        (<Verify 
                            match = {this.props}
                            code={match.params.code}
                            email={match.params.email}
                            wrapper={this.props.wrapper}
                         />
                         )
                    )
                } />*/}



                <Route exact path='/return-refund-policy' component={ReturnRefund} />
                <Route exact path='/delivery-policy' component={Delivery} />

            </Switch>
		)
	}
	
}

export default Content;