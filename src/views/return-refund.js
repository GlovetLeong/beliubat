import React, { Component } from 'react';
import {
	Row, Col,
} from 'reactstrap';

class Sample extends Component {

	render() {
		return (
			<Row>
				<Col className='mission text-center' md='12'>
					<div className='small-title mb-3'>Return & Refund Policy</div>
					This Return and Refund policy is only be applicable for BeliUbat Online.
					<br/>
					<br/>

					Should there be any discrepancy of products delivered and customer wishes to exchange, 
					return or refund, please notify our Customer Service either by phone <b>03-74506603</b> 
					or email <b>info.beliubat@google.com</b> to report on the products within 3 days after receiving the goods.
					<br/>
					<br/>
					Please allow up to <b>7 -10 working days</b> for your inquiry to be processed.
				</Col>

				<Col className='mission text-center' md='12'>
					<b>Return/Refund of Products can be arranged under the following reasons:</b>

					<ul>
						<li>If the product delivered is in damaged or defective.</li>
						<li>If the product is near expiry or expired.</li>
						<li>
							If the product is different from the order made. 
							<br/>
							Products for return or refund shall be returned in its original condition,
							<br/>
							quantity and packaging as it first delivered to customer together with proof of purchase.
						</li>
					</ul>
				</Col>

				<Col className='mission text-center' md='12'>
					The Customer shall be responsible for the risks and condition of the product to be returned until it reaches BeliUbat warehouse.
					<br/>
					BeliUbat will not be liable for any loss or damage to the product prior to its receiving by us. 
					<br/>
					Customers are advised to pack the product with care to prevent any loss or damage to the product or its box / original packaging.
				</Col>
			</Row>
		)
	}
}

export default Sample;