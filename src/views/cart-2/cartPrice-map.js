import React, { Component } from 'react';
import { NavLink as RouteLink }  from 'react-router-dom';
import {
	Row, Col} from 'reactstrap';

var lodash = require('lodash');

class CartPriceMap extends Component {

	constructor(props) {
		super(props);
		this.state = {
			modalCheckOut: false
		};
		this.toggleCheckOut = this.toggleCheckOut.bind(this);
	}

	toggleCheckOut() {
		this.props.requestClickCheckOutGet();
		this.setState({
			modalCheckOut: !this.state.modalCheckOut
		});
	}

	render() {
        const cartList = lodash.get(this.props.cartList, 'result', []);

		return (
			<Col className='checkout-box' md='3'>
				<div className='checkout-header mb-2'>Items List</div>
				<div className="cart-list-content white with-radius">
					{
						cartList.map((cart, index) =>
							<Col className='my-2' md='12' key={index}>
								<Row>
									<Col className='checkout-name' md='7'>
										{cart.name}
									</Col>
									<Col className='checkout-price' md='5'>
										RM {cart.quantity_price.toFixed(2)}
									</Col>
								</Row>
								<Col md='12' className='line'></Col>
							</Col>
						)
					}

					<div>
						{
							(cartList.consultant_fee) ?
								<Col className='my-2' md='12'>
									<Row>
										<Col className='checkout-name' md='7'>
											Consultant Fee
										</Col>
										<Col className='checkout-price' md='5'>
											RM {cartList.consultant_fee}
										</Col>
									</Row>
									<Col md='12' className='line'></Col>
								</Col>

							:
								<span></span>
						}
					</div>

					<Col className='my-2' md='12'>
						<Row>
							<Col className='checkout-name' md='7'>
								Total
							</Col>
							<Col className='checkout-price' md='5'>
								RM {Number(cartList.total_amount).toFixed(2)}
							</Col>
						</Row>
						<Col md='12' className='line'></Col>
					</Col>

					<Col className='my-2' md='12'>
						<RouteLink className='nav-link btn btn-primary' exact to='/checkout'>Check Out</RouteLink>
					</Col>
				</div>
			</Col>
		)
	}
}

export default CartPriceMap;