import React, { Component } from 'react';
import {
	Row, Col,
} from 'reactstrap';
// import classnames from 'classnames';
import CartMap from './cart-map';
import CartPriceMap from './cartPrice-map';


class CartListModal extends Component {

	constructor(props) {
		super(props);
		this.state = {
			activeTab: '1'
		};
        this.props.requestCartListGet();
	}

	render() {
        const cartLoading = this.props.cartLoading;
        const cartList = this.props.cartList;
        
		return (
			<div>

			{
				(cartLoading) ? 
					<div className='text-center'>
						<div className="lds-ripple">
							<div></div>
							<div></div>
						</div>
					</div>
				:
				<div>
				{
					(this.props.wrapper.totalCart > 0) ?
						<Row>
							<CartMap 
								cartList={cartList}
								setCartQuantityPrice={this.props.setCartQuantityPrice}
							/>
							<CartPriceMap
								cartList={cartList}
								cartToggle={this.props.toggle}
								requestClickCheckOutGet={this.props.requestClickCheckOutGet}
							/>
						</Row>
					:
					<Row>
						<Col className='text-center'>
							Empty
						</Col>
					</Row>

				}
				</div>
			}
			</div>
		)
	}
}

export default CartListModal;