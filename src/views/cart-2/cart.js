import React, { Component } from 'react';
import CartListContainer from '../../redux/containers/cartListContainer';

class Cart extends Component {

	render() {
		return (
			<div className="mb-3">
				<div className='small-title mb-3'>
					Cart List
				</div>
				<CartListContainer
					wrapper={this.props.wrapper}
				/>
			</div>
		)
	}
}

export default Cart;