import React, { Component } from 'react';
import CheckOutContainer from '../../redux/containers/checkOutContainer';

class CheckOut extends Component {
	render() {
		return (
			<CheckOutContainer
				wrapper={this.props.wrapper}
				modal={this.props.modal}
				toggle={this.props.toggle}
				cartToggle={this.props.cartToggle}
			/>
		)
	}
}

export default CheckOut;