import React, { Component } from 'react';

import {
	Row, Col, Button} from 'reactstrap';

var lodash = require('lodash');

class CheckOutMap extends Component {
	constructor(props) {
		super(props);
		this.state = {
			coupon_code: ''
		};
        this.handleChange = this.handleChange.bind(this);
	}

	handleChange(e) {
    	this.setState({ [e.target.name]: e.target.value });
    }

	render() {
        const checkOutList = lodash.get(this.props.checkOutList, 'carts', []);

		return (
			<Col className='cart-list-box' md='12'>
				<Col className='cart-list-header' md='12'>
					<Row>
						<Col md='7'>
							Item
						</Col>
						<Col md='3'>
							Quantity
						</Col>
						<Col md='2'>
							Price
						</Col>
					</Row>
				</Col>
				{
					checkOutList.map((checkOut, index) =>
						<Col className='cart-list-content my-2' md='12' key={index}>
							<Row>
		       					<Col md='7'>
		       						<Row>
		       							<Col md='4' className='text-center'>
											<img className='img-fluid cart-img' alt='' src={checkOut.image} />
		       							</Col>
		       							<Col md='8'>
	       									<Col md='12' className='product-name-box mb-2'>
												<span className='product-name'>
													{checkOut.name}
												</span>
											</Col>
											<Col md='12' className='product-category mb-3'>
												<div className='category'>
													<span className='spacing-right'>{checkOut.type}</span>
													{
														(checkOut.market_price !==  checkOut.price) ?
				                                            <span className='product-discount'>- {checkOut.discount}</span>
														:
															<span></span>
													}
												</div>
											</Col>
											<Col md='12' className='line'></Col>
											<div className='mb-3'>
												<Row>
													<Col className='mb-2' md='12'>
														<div className='volume-code'>
															Volume: <span className='volume-value'>{checkOut.sub_uom}</span>
														</div>
													</Col>
													<Col className='mb-2' md='12'>
														<div className='volume-code'>
															Code: <span className='volume-value'>{checkOut.uom}</span>
														</div>
													</Col>
												</Row>
											</div>
		       							</Col>
		       						</Row>
								</Col>
								<Col className='mb-3' md='3'>
									<Row>
										<Col md='12' xs='12'>
											<input name='quantity' type='text' className='quantity-txt form-control' value={checkOut.quantity} disabled/>
										</Col>
									</Row>
								</Col>
								<Col className='text-center' md='2'>
									{
										(checkOut.market_price !==  checkOut.price) ?
											<div className='market-price'>RM {checkOut.quantity_market_price}</div>
										:
											<div></div>
									}
									<div className='current-price'>RM {checkOut.quantity_price}</div>
								</Col>
							</Row>
						</Col>
					)
				}

				<Col className='cart-list-content my-2' md='12'>

					{
						(this.props.checkOutList.coupon[0] && this.props.checkOutList.coupon[0].coupon_discount_total > 0) ?
							<Row>
								<Col md='7'></Col>
								<Col className='check-total' md='3'>
									{this.props.checkOutList.coupon[0].coupon_code}
								</Col>
								<Col className='check-total' md='2'>
									-RM {this.props.checkOutList.coupon[0].coupon_discount_total}
								</Col>
							</Row>
						:
							<Row>
								<Col md='7'></Col>
								<Col className='check-total' md='3'>
									<div className='form-group'>
										<input name='coupon_code' type='text' className='form-control' placeholder='Coupon Code' onChange={this.handleChange}/>
									</div>
								</Col>
								<Col className='check-total' md='2'>
									<Button color='primary' onClick={() => this.props.onSubmitCoupon(this.state.coupon_code)} >Apply</Button>
								</Col>
							</Row>
					}
				</Col>

				<Col className='cart-list-content my-2' md='12'>
					<Row>
						<Col md='7'></Col>
						<Col className='check-total' md='3'>
							Shipping Cost
						</Col>
						<Col className='check-total' md='2'>
	                    	RM {this.props.checkOutList.shipping_cost}
						</Col>
					</Row>
				</Col>

				<Col className='cart-list-content my-2' md='12'>
					<Row>
						<Col md='7'></Col>
						<Col className='check-total' md='3'>
							Total
						</Col>
						<Col className='check-total' md='2'>
	                    	RM {this.props.checkOutList.order_total}
						</Col>
					</Row>
				</Col>
			</Col>
		)
	}
}

export default CheckOutMap;