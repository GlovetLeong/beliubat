import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col,
	Button, 
	Modal, 
	ModalHeader, 
	ModalBody, 
	ModalFooter} from 'reactstrap';
import CheckOutMap from './checkOut-map';

const lodash = require('lodash');

class CheckOutModal extends Component {

	constructor(props) {
		super(props);

		this.state = {
			proceedStatus: false,
			code: ''
		};

        this.handleChange = this.handleChange.bind(this);
        this.setShippingAddress = this.setShippingAddress.bind(this);
        this.setBillingAddress = this.setBillingAddress.bind(this);
        this.onSubmitChangeAddress = this.onSubmitChangeAddress.bind(this);
        this.onSubmitCoupon = this.onSubmitCoupon.bind(this);
        this.onSubmitProceedCheckOut = this.onSubmitProceedCheckOut.bind(this);

        this.props.getState();
        this.props.requestCheckOutGet();
	}

	setShippingAddress(e) {
        let shippingAddress = Object.assign({}, this.props.checkOutList.result.shipping_address);
        shippingAddress[e.target.name] = e.target.value;
        this.props.setAddressData(shippingAddress, this.props.checkOutList, 1);
    }

    setBillingAddress(e) {
        let billingAddress = Object.assign({}, this.props.checkOutList.result.billing_address);
        billingAddress[e.target.name] = e.target.value;
        this.props.setAddressData(billingAddress, this.props.checkOutList, 2);
    }

	handleChange(e) {
    	this.setState({ [e.target.name]: e.target.value });
    }

	onSubmitChangeAddress(type){
        var params = {};
        if(type === 'shipping') {
            params = this.props.checkOutList.result.shipping_address;
            this.props.postChangeAddress(params, 1);
        }
        else if(type === 'billing') {
            params = this.props.checkOutList.result.billing_address;
            this.props.postChangeAddress(params, 2);
        }
    }

    onSubmitCoupon(code){
    	this.setState({
			code: code
		});
    	this.props.requestApplyCoupon(code);
    }

    onSubmitProceedCheckOut(){
    	const coupon = this.state.code;
        this.props.postProceedCheckOut(coupon);
    }
	
	static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.proceedStatus) {
            nextProps.toggle();
            nextProps.cartToggle();
            nextProps.setProceedStatus(0);
        }
        return null;
    }

	render() {

        const stateList = lodash.get(this.props.stateList, 'result', []);
        const checkOutList = lodash.get(this.props.checkOutList, 'result', []);
        const shippingAddress1 = lodash.get(checkOutList.shipping_address, 'address_1', '');
        const shippingAddress2 = lodash.get(checkOutList.shipping_address, 'address_2', '');
        const shippingPostcode = lodash.get(checkOutList.shipping_address, 'address_postcode', '');
        const shippingState= lodash.get(checkOutList.shipping_address, 'address_stateid', '');

        const billingAddress1 = lodash.get(checkOutList.billing_address, 'address_1', '');
        const billingAddress2 = lodash.get(checkOutList.billing_address, 'address_2', '');
        const billingPostcode = lodash.get(checkOutList.billing_address, 'address_postcode', '');
        const billingState = lodash.get(checkOutList.billing_address, 'address_stateid', '');

    	if (this.props.proceedStatus) {

    		const order_id = lodash.get(this.props.proceedList.result, 'order_id', '');
    		const payment_url = lodash.get(this.props.proceedList.result, 'payment_url', '');

    		return <Redirect
			to={{
				pathname: '/proceed-pay',
				search: '?order_id=' + order_id + '&payment_url=' + encodeURIComponent(payment_url),
				
			}}
			/>

		}

		return (
			<div>
				<Modal className='modal-lg' isOpen={this.props.modal} toggle={this.props.toggle}>
					<ModalHeader>
						<FontAwesomeIcon className='spacing-right' icon={faShoppingBag} /> 
						CheckOut
					</ModalHeader>
					<ModalBody>
						<Row>
							<Col sm='6' xs='12'>
								<h4>Shipping Address</h4>
								<div className='form-group'>
									<input name='address_1' type='text' className='form-control' placeholder='Address 1' value={shippingAddress1} onChange={this.setShippingAddress}/>
								</div>
								<div className='form-group'>
									<input name='address_2' type='text' className='form-control' placeholder='Address 2' value={shippingAddress2} onChange={this.setShippingAddress}/>
								</div>
								<div className='form-group'>
									<input name='address_postcode' type='text' className='form-control' placeholder='Postcode' value={shippingPostcode} onChange={this.setShippingAddress}/>
								</div>
								<div className='form-group'>
                                    <select className='form-control' name='address_stateid' value={shippingState} onChange={this.setShippingAddress}>
                                		<option value=''>--Select A State--</option>
		                                {
		                                    stateList.map((state, index) =>
		                                    <option key={index} value={state.state_id}>{state.state_name}</option>
		                                    )
		                                }
									</select>
								</div>
								<div className='form-group'>
									<Button color='primary' block onClick={() => this.onSubmitChangeAddress('shipping')}>Update</Button>
								</div>
							</Col>

							<Col sm='6' xs='12'>
								<h4>Billing Address</h4>
								<div className='form-group'>
									<input name='address_1' type='text' className='form-control' placeholder='Address 1' value={billingAddress1} onChange={this.setBillingAddress}/>
								</div>
								<div className='form-group'>
									<input name='address_2' type='text' className='form-control' placeholder='Address 2' value={billingAddress2} onChange={this.setBillingAddress}/>
								</div>
								<div className='form-group'>
									<input name='address_postcode' type='text' className='form-control' placeholder='Postcode' value={billingPostcode} onChange={this.setBillingAddress}/>
								</div>
								<div className='form-group'>
                                    <select className='form-control' name='address_stateid' value={billingState} onChange={this.setBillingAddress}>
                                		<option value=''>--Select A State--</option>
                                        {
		                                    stateList.map((state, index) =>
		                                    <option key={index} value={state.state_id}>{state.state_name}</option>
		                                    )
		                                }
									</select>
								</div>
								<div className='form-group'>
									<Button color='primary' block onClick={() => this.onSubmitChangeAddress('billing')}>Update</Button>
								</div>
							</Col>

							<CheckOutMap 
								checkOutList={checkOutList}
								onSubmitCoupon={this.onSubmitCoupon}
							/>
						</Row>
					</ModalBody>
					<ModalFooter>
						<Button color='primary' onClick={this.onSubmitProceedCheckOut}>Proceed</Button>
					</ModalFooter>
				</Modal>
			</div>
		)
	}
}

export default CheckOutModal;