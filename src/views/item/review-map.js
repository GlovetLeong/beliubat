import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar as farFaStar } from '@fortawesome/free-regular-svg-icons';
import { faStarHalfAlt } from '@fortawesome/free-solid-svg-icons';
import { faStar as fasFaStar } from '@fortawesome/free-solid-svg-icons';

import {
	Row, Col,
	Progress} from 'reactstrap';
import ReviewModal from './review-modal';

class ReviewMap extends Component {

	constructor(props) {
		super(props);
		this.state = {
            page: 1,
            modal: false,
            params: this.props.params
		}
		this.toggle = this.toggle.bind(this);		
	}

	toggle() {
		this.setState({
			modal: !this.state.modal
		});
	}

	star(review_star) {
		if (review_star) {
			let star = [];
			let index = 0;
			for (let i = 0; i < review_star.total_on_star; i++) { 
				star.push(<FontAwesomeIcon className='yellow-text' key={index++} icon={fasFaStar} />);
			}
			for (let i = 0; i < review_star.total_half_on_star; i++) { 
				star.push(<FontAwesomeIcon className='yellow-text' key={index++} icon={faStarHalfAlt} />);
			}
			for (let i = 0; i < review_star.total_off_star; i++) { 
				star.push(<FontAwesomeIcon key={index++} icon={farFaStar} />);
			}
			return star;
		}
	}

	bar(ratings, a_bar_value) {
		let bar = [];
		let index = 0;

		if (ratings) {
	        for (var k in ratings){
	        	if (ratings.hasOwnProperty(k)) {
		        	bar.push(
		    			<div className='mb-1' key={index++}> 
							<div className="float-left spacing-right">{ratings[k].rating}</div> 
							<div className="float-left yellow-text spacing-right"><FontAwesomeIcon icon={fasFaStar} /></div>
							<div className="float-left review-bar"><Progress value={a_bar_value * ratings[k].number} /></div> 
							<div className='float-left'>({ratings[k].number})</div>
							<br />
						</div>
		        	);
		        }
	        }
    	}

        return bar;
	}

	render() {
        const itemReviewDetail = this.props.itemReviewDetail;

		return (
			<Row>
				<Col className="small-title mb-2" md='12'>Reviews</Col>
				<Col className='text-center' md='3'>
					<div className='review-box'>
						<div className='total-review'>{ itemReviewDetail.review_average_rating }</div>
						<div className='review-text'>REVIEWS</div>
					</div>
				</Col>
				<Col className='text-center' md='3'>
					<div className='review-star mb-3'>
						{this.star(itemReviewDetail.review_average_star)}
					</div>
					<div className='review-create-box'>
						<div className='btn btn-primary btn-sm'
							onClick={() => this.toggle()}
						>
							Review it
						</div>
					</div>
				</Col>
				<Col className='text-center' md='6'>
					{this.bar(itemReviewDetail.review_ratings, itemReviewDetail.a_bar_value)}
				</Col>

				<ReviewModal
					wrapper={this.props.wrapper}
					modal={this.state.modal}
					toggle={this.toggle}
					product_id={this.props.product_id}
					itemReviewDetail={this.props.itemReviewDetail}
					requestCreateItemReview={this.props.requestCreateItemReview}
					itemReviewCreateStatus={this.props.itemReviewCreateStatus}
					setItemReviewCreateStatus={this.props.setItemReviewCreateStatus}
				/>
			</Row>
		)
	}
}

export default ReviewMap;