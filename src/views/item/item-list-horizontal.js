import React, { Component } from 'react';
import HotItemListHorizontalContainer from '../../redux/containers/hotItemListHorizontalContainer';

class HotItemListHorizontal extends Component {

	render() {
		return (
			<HotItemListHorizontalContainer 
				wrapper={this.props.wrapper}
				params={this.props.params}
				toggleCall={this.props.toggleCall}
				itemCall={this.props.itemCall}
				type={this.props.type}
			/>
		)
	}
}

export default HotItemListHorizontal;