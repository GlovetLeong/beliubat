import React, { Component } from 'react';
import ItemListContainer from '../../redux/containers/ItemListContainer';

class ItemList extends Component {

	render() {
		return (
			<ItemListContainer 
				wrapper={this.props.wrapper}
				params={this.props.params}
				toggleCall={this.props.toggleCall}
				itemCall={this.props.itemCall}
				type={this.props.type}
			/>
		)
	}
}

export default ItemList;