import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingBag  } from '@fortawesome/free-solid-svg-icons';
import { faAngleDoubleDown   } from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col} from 'reactstrap';
import ItemModal from './item-modal';
import NeedDoctor from '../needDoctor/needDoctor';
import iconDOC from '../../images/doc.png';

const lodash = require('lodash');

class ItemMap extends Component {

	constructor(props) {
		super(props);
		this.state = {
            page: 1,
            modal: false,
            params: this.props.params
		}
        this.loadMore = this.loadMore.bind(this);
		this.itemOpen = this.itemOpen.bind(this);
		this.toggle = this.toggle.bind(this);		

		if(this.props.itemCall === false) {
			this.props.toggleCall(this.props.type);
			this.props.requestItemListGet(this.props.params);
		}

	}

	toggle() {
		this.setState({
			modal: !this.state.modal
		});
	}

	itemOpen(product_id) {
		this.toggle();

		const params = {
			product_id: product_id
		}

		const params_2 = {
			id: product_id
		}
		this.props.requestItemDetailGet(params);
		this.props.requestItemReviewDetailGet(params_2);
	}

	static getDerivedStateFromProps(nextProps, preState) {
		if (nextProps.params !== preState.params) {
			nextProps.requestItemListGet(nextProps.params);

			 return {
            	params: nextProps.params
            };
		}
		return null;
	}

	map(itemList) {
        return (
           itemList.map((item, index) =>
                <Col className='mb-3 item-outside-padding' xs='6' key={index}>
				{
					(item.market_price !==  item.price && item.market_price > 0) ?
                        <span className='product-discount-mobile' xs='3'>- {item.discount}</span>
					:
						<span></span>
				}
                <div className="item-min-height white with-radius">
					<Row>
						<Col className='text-center' md='5'>
							<img className='product-img img-fluid pointer' alt='' src={item.image} 
								onClick={() => this.itemOpen(item.product_id)}
							/>
						</Col>
						<Col md='7'>
							<Col md='12'>
								<Row>
									<Col className='mb-3' md='12'>
										<Row>
											<Col md='8'>
												
												<div className='product-name-box mb-2'>
													<span className='product-name pointer'
														onClick={() => this.itemOpen(item.product_id)}
													>
														{item.name}
													</span> 
												</div>
											</Col>
											<Col className='doc-box item-ask' md='4'>
												{
													(item.type === 'Rx Product') ?
														<NeedDoctor
															img={iconDOC}
															wrapper={this.props.wrapper}
														/>
													:
														<span></span>
												}
												
											</Col>
										</Row>
									</Col>
									<Col className='price-con' md='12'>
										<Row>
											<Col className='price-box' md='8' xs='12'>
												<Row>
													{
														(item.market_price !==  item.price && item.market_price > 0) ?
															<Col className='market-price' md='6'>
																RM {item.market_price}
															</Col>
														:
															<div></div>
													}
													{
														(item.market_price !==  item.price && item.market_price > 0) ?
		                                                    <Col className='product-discount text-center' md='4'>- {item.discount}</Col>
														:
															<div></div>
													}
												</Row>
												<div className='current-price'>RM {item.price}</div>
											</Col>
											<Col className='cart-out-box text-center' md='4' xs='12'>
												<div className='item-detail-cart btn btn-primary'
													onClick={() => this.itemOpen(item.product_id)}
												>
													<FontAwesomeIcon icon={faShoppingBag} />
												</div>
											</Col>
										</Row>
									</Col>
								</Row>
							</Col>
						</Col>
					</Row>
				</div>
				</Col>
            )
        );
    }

    loadMore() {
        const pervious_data = lodash.get(this.props.itemList, 'products', []);
    	const total_page =  lodash.get(this.props.itemList, 'total_page', []);
        const params = lodash.get(this.props.itemList, 'params', []);
        const next_page = this.state.page + 1;

        if(next_page <= total_page) {
    		this.props.requestLoadMore(params, next_page, pervious_data);
    		this.setState({page: this.state.page + 1});
    	}
    }

	render() {
        const itemListLoading = this.props.itemListLoading;
        const itemList = lodash.get(this.props.itemList, 'products', []);
        const total_page =  lodash.get(this.props.itemList, 'total_page', []);

		return (

			<div>
				{
					(itemListLoading) ?
						<div className='text-center'>
							<div className="lds-ripple">
								<div></div>
								<div></div>
							</div>
						</div>
					:
					(itemList.length > 0) ?
					<Row>
						{this.map(itemList)}
		                {
							(this.state.page < total_page) ? 
						        <Col className='load-more' sm='12'>
						        	<span className='pointer' onClick={this.loadMore}>
										<FontAwesomeIcon className='spacing-right' icon={faAngleDoubleDown} />
						        		Load More
									</span>

						        </Col>
						    :
						    <div></div>
						}
						<ItemModal
							wrapper={this.props.wrapper}
							modal={this.state.modal}
							toggle={this.toggle}
							itemDetail={this.props.itemDetail}
							itemReviewDetail={this.props.itemReviewDetail}
							requestCreateItemReview={this.props.requestCreateItemReview}
							itemReviewCreateStatus={this.props.itemReviewCreateStatus}
							setItemReviewCreateStatus={this.props.setItemReviewCreateStatus}
							setQuantityPrice={this.props.setQuantityPrice}
						/>
					</Row>
					:
					<div className='small-title m-3 text-center'>No Result Found</div>
				}
			</div>
		)
	}
}

export default ItemMap;