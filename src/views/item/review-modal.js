import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar as farFaStar } from '@fortawesome/free-regular-svg-icons';
import { faStar as fasFaStar } from '@fortawesome/free-solid-svg-icons';

import {
	Row, Col,
	Button,
	Modal, 
	ModalHeader, 
	ModalBody,
	ModalFooter,
	Input} from 'reactstrap';

class ReviewModal extends Component {

	constructor(props) {
		super(props);
		this.state = {
			star: [],
			rating: 0,
			review_message: ''
		};

		this.star = this.star.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.ratingSelect = this.ratingSelect.bind(this);
        this.onSubmitReview = this.onSubmitReview.bind(this);
	}

	handleChange(e) {
    	this.setState({ [e.target.name]: e.target.value });
    }

    ratingSelect(value) {
    	this.setState({ rating: value });
    	this.star(value);
    }

	onSubmitReview() {
		const params = {
			id: this.props.product_id,
			review_star: this.state.rating,
			review_text: this.state.review_message,
		}

		this.props.requestCreateItemReview(params);
	}

	static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.itemReviewCreateStatus) {
            nextProps.setItemReviewCreateStatus(0);
            nextProps.toggle();
            return { star: [], rating: 0, review_message: ''};
        }
        return null;
    }

	star(star_on = 0) {
		let star = [];
		let index = 1;
		for (let i = 1; i <= 5; i++) { 
			if (Number(star_on) > 0) {
				star.push(<FontAwesomeIcon className='pointer yellow-text' key={index++} icon={fasFaStar} onClick={() => this.ratingSelect(i)} />);
				star_on--;
			}
			else {
				star.push(<FontAwesomeIcon className='pointer' some={i} key={index++} icon={farFaStar} onClick={() => this.ratingSelect(i)} />);
			}
		}
		return star;
	}

	render() {

		return (
			<div>
				<Modal className='modal-sm' isOpen={this.props.modal} toggle={this.props.toggle}>
					<ModalHeader toggle={this.props.toggle}>
						Review
					</ModalHeader>
					<ModalBody>
						<Row className='text-center'>
							<Col className='mb-3' md='12'>
								{this.star(this.state.rating)}
							</Col>
							<Col md='12'>
								<Input type="textarea" name="review_message" placeholder='Tell our you feedback' onChange={this.handleChange} />
							</Col>
						</Row>
					</ModalBody>
					<ModalFooter>
						<Button color='primary' block onClick={this.onSubmitReview}>Submit</Button>
					</ModalFooter>
				</Modal>
			</div>
		)
	}
}

export default ReviewModal;