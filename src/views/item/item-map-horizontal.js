import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingBag  } from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col} from 'reactstrap';
import Swiper from 'react-id-swiper';
import ItemModal from './item-modal';
import NeedDoctor from '../needDoctor/needDoctor';
import iconDOC from '../../images/doc.png';

const lodash = require('lodash');

class ItemMap extends Component {

	constructor(props) {
		super(props);
		this.state = {
            page: 1,
            modal: false,
            params: this.props.params
		}
		this.itemOpen = this.itemOpen.bind(this);
		this.toggle = this.toggle.bind(this);		

		if(this.props.itemCall === false) {
			this.props.toggleCall(this.props.type);
			this.props.requestHotItemListHorizontalGet(this.props.params);
		}

	}

	toggle() {
		this.setState({
			modal: !this.state.modal
		});
	}

	itemOpen(product_id) {
		this.toggle();

		const params = {
			product_id: product_id
		}

		const params_2 = {
			id: product_id
		}

		this.props.requestItemDetailGet(params);
		this.props.requestItemReviewDetailGet(params_2);
	}

	static getDerivedStateFromProps(nextProps, preState) {
		if (nextProps.params !== preState.params) {
			nextProps.requestHotItemListHorizontalGet(nextProps.params);

			 return {
            	params: nextProps.params
            };
		}
		return null;
	}

	map(itemList) {
        return (
           itemList.map((item, index) =>
                <Col className='item-outside-padding' xs='6' key={index}>
                <div className="item-min-height white with-radius">
					<Row>
						<Col className='text-center' md='5'>
							<img className='product-img img-fluid pointer' alt='' src={item.image} 
								onClick={() => this.itemOpen(item.product_id)}
							/>
						</Col>
						<Col md='7'>
							<Col md='12'>
								<Row>
									<Col className='mb-3' md='12'>
										<Row>
											<Col md='8'>
												<div className='product-name-box mb-2'>
													<span className='product-name pointer'
														onClick={() => this.itemOpen(item.product_id)}
													>
														{item.name}
													</span> 
												</div>
											</Col>
											<Col className='doc-box item-ask' md='4'>
												{
													(item.type === 'Rx Product') ?
														<NeedDoctor
															img={iconDOC}
															wrapper={this.props.wrapper}
														/>
													:
														<span></span>
												}
												{
													(item.market_price !==  item.price && item.market_price > 0) ?
	                                                    <span className='product-discount'>- {item.discount}</span>
													:
														<span></span>
												}
											</Col>
										</Row>
									</Col>
									<Col className='price-con' md='12'>
										<Row>
											<Col className='price-box' md='8' xs='12'>
												{
													(item.market_price !==  item.price && item.market_price > 0) ?
														<div className='market-price'>RM {item.market_price}</div>
													:
														<div></div>
												}
												<div className='current-price'>RM {item.price}</div>
											</Col>
											<Col className='cart-out-box text-center' md='4' xs='12'>
												<div className='item-detail-cart btn btn-primary'
													onClick={() => this.itemOpen(item.product_id)}
												>
													<FontAwesomeIcon icon={faShoppingBag} />
												</div>
											</Col>
										</Row>
									</Col>
								</Row>
							</Col>
						</Col>
					</Row>
				</div>
				</Col>
            )
        );
    }

	render() {
        const itemListLoading = this.props.hotItemListHorizontalLoading;
        const itemList = lodash.get(this.props.hotItemListHorizontal, 'products', []);
        const swiperParams = {
			slidesPerView: 'auto',
			autoplay: {
				delay: 2500,
				disableOnInteraction: false
			},
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			}
        }

		return (
			<div>
				{
					(itemListLoading) ?
						<div className='text-center'>
							<div className="lds-ripple">
								<div></div>
								<div></div>
							</div>
						</div>
					:
					(itemList.length > 0) ?
					<Row>
						<Swiper {...swiperParams}>
							{this.map(itemList)}
						</Swiper>
						<ItemModal
							wrapper={this.props.wrapper}
							modal={this.state.modal}
							toggle={this.toggle}
							itemDetail={this.props.itemDetail}
							itemReviewDetail={this.props.itemReviewDetail}
							requestCreateItemReview={this.props.requestCreateItemReview}
							itemReviewCreateStatus={this.props.itemReviewCreateStatus}
							setItemReviewCreateStatus={this.props.setItemReviewCreateStatus}
							setQuantityPrice={this.props.setQuantityPrice}
						/>
					</Row>
					:
					<div className='small-title m-3 text-center'>No Result Found</div>
				}
			</div>
		)
	}
}

export default ItemMap;