import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhoneVolume  } from '@fortawesome/free-solid-svg-icons';
import { faUserMd  } from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col,
	Button, 
	ModalBody, 
	ModalFooter} from 'reactstrap';

class Symptoms extends Component {

	constructor(props) {
        super(props);
        this.state = {
            symptomsMessage: '',
            code1: '',
            code2: '',
            code3: '',
            code4: '',
            code5: '',
            code6: '',
            status: '',
            disable: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSymptomsChange = this.handleSymptomsChange.bind(this);
        this.onSubmitSymptoms = this.onSubmitSymptoms.bind(this);
    }

    handleChange(e) {
        const name = e.target.name;
        if((name === 'code1' && !this.state.code1) || !e.target.value ) {
            if(e.target.value){
                this.refs['code2'].focus();
            }
    	    this.setState({ [e.target.name]: e.target.value });
        }
        if((name === 'code2' && !this.state.code2) || !e.target.value ) {
            if(e.target.value){
                this.refs['code3'].focus();
            }
                this.setState({ [e.target.name]: e.target.value });
        }
        if((name === 'code3' && !this.state.code3) || !e.target.value ) {
            if(e.target.value){
                this.refs['code4'].focus();
            }
                this.setState({ [e.target.name]: e.target.value });
        }
        if((name === 'code4' && !this.state.code4) || !e.target.value ) {
            if(e.target.value){
                this.refs['code5'].focus();
            }
                this.setState({ [e.target.name]: e.target.value });
        }
        if((name === 'code5' && !this.state.code5) || !e.target.value ) {
            if(e.target.value){
                this.refs['code6'].focus();
            }
                this.setState({ [e.target.name]: e.target.value });
        }
        if((name === 'code6' && !this.state.code6) || !e.target.value ) {
            if(e.target.value){
        	    this.setState({ [e.target.name]: e.target.value });
            }
        }
    }

    handleSymptomsChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmitSymptoms() {
    	const params = {
			message: this.state.symptomsMessage,
    		passcode: this.state.code1 + this.state.code2 + this.state.code3 + this.state.code4 + this.state.code5 + this.state.code6,
    	}
    	this.props.onSubmitSymptoms(params);

        this.setState({
            disable: true, 
        });
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.symptoms.symptomsStatus) {
            nextProps.toggle();
            nextProps.symptoms.setSymptomsStatus(0);
            return {
                symptomsMessage: '',
                code1: '',
                code2: '',
                code3: '',
                code4: '',
                code5: '',
                code6: '',
            };
        }
        return null;
    }

	render() {

        if (this.props.symptoms.symptomsStatus) {
            return <Redirect
            to={{
                pathname: '/consultant-record',
            }}
            />
        }

		return (
			<div>
				<ModalBody>
					<Row>
						<Col className='text-center' sm='12'>
	                    	<p>Request a doctor to say your symptoms</p>
						</Col>
						<Col className='step-form' sm='12' md={{ size: 10, offset: 1 }}>
							<Col xs='12'>
								<Row>
									<Col md='3'>
										<div className='code-icon'>
											<FontAwesomeIcon icon={faUserMd} />
										</div>
									</Col>
									<Col md='9'>
										<div className='textarea-box'>
											<textarea className='form-control' placeholder='Tell us your symptoms...' name='symptomsMessage' onChange={this.handleSymptomsChange}/>
										</div>
										<div>
											<p>Please fill in 6 digits verfication code to start</p>
										</div>
                                		<div className='code-box'>
											<input className='form-control' placeholder='0' name='code1' value={this.state.code1} onChange={this.handleChange} ref='code1'/>
											<input className='form-control' placeholder='0' name='code2' value={this.state.code2} onChange={this.handleChange} ref='code2'/>
											<input className='form-control' placeholder='0' name='code3' value={this.state.code3} onChange={this.handleChange} ref='code3'/>
											<input className='form-control' placeholder='0' name='code4' value={this.state.code4} onChange={this.handleChange} ref='code4'/>
											<input className='form-control' placeholder='0' name='code5' value={this.state.code5} onChange={this.handleChange} ref='code5'/>
											<input className='form-control' placeholder='0' name='code6' value={this.state.code6} onChange={this.handleChange} ref='code6'/>
                                		</div>
									</Col>
								</Row>
							</Col>
						</Col>
					</Row>
				</ModalBody>
				<ModalFooter>
					<div>
					{
						(!this.state.disable) ?
							<Button color='primary' onClick={this.onSubmitSymptoms}>
								<FontAwesomeIcon className='spacing-right' icon={faPhoneVolume} /> Call Now
							</Button>
						:
							<Button color='danger'>
								Waiting Request
							</Button>
					}
					
					</div>
				</ModalFooter>
			</div>
		)
	}
	
}

export default Symptoms;