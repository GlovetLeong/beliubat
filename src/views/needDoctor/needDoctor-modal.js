import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhoneVolume  } from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col,
	Modal, 
	ModalHeader, 
	ModalBody} from 'reactstrap';
import Symptoms from './symptoms';

class NeedDoctorModal extends Component {

	constructor(props) {
		super(props);
        this.handleChange = this.handleChange.bind(this);
        this.onSubmitSymptoms = this.onSubmitSymptoms.bind(this);
	}

	handleChange(e) {
    	this.setState({ [e.target.name]: e.target.value });
    }

	onSubmitSymptoms(params) {
		this.props.requestnSymptomsPost(params);
	}

	render() {
		return (
			<div>
				<Modal className='modal-lg' isOpen={this.props.modal} toggle={this.props.toggle}>
					<ModalHeader toggle={this.props.toggle}>
						<FontAwesomeIcon className='spacing-right' icon={faPhoneVolume} /> 
						Need a Doctor
					</ModalHeader>

					<ModalBody>
						<Row>
							<Col className='step-bar' sm='12' md={{ size: 8, offset: 2 }}>
								<div className={this.props.symptoms_active ? 'step-content active' : 'step-content'} >
									<div className='step-circle' onClick={() => this.props.toggleClass('symptoms')}></div>
									<div className='step-name'>Symptoms</div>
								</div>
								<div className='step-line'></div>
								<div className='step-content'>
									<div className='step-circle'></div>
									<div className='step-name'>Consult</div>
								</div>
							</Col>
						</Row>
					</ModalBody>
					{
						(this.props.symptoms) ?
							<Symptoms 
								wrapper={this.props.wrapper}
								onSubmitSymptoms={this.onSubmitSymptoms}
								symptoms={this.props}
								toggle={this.props.toggle}
							/>
						:
						<div>ccc</div>
					}
				</Modal>
			</div>
		)
	}
}

export default NeedDoctorModal;