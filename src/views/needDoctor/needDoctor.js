import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import NeedDoctorContainer from '../../redux/containers/needDoctorContainer';

class NeedDoctor extends Component {

	constructor(props) {
		super(props);
		this.state = {
			modal: false,
			symptoms: true,
			symptoms_active: true,
			consult: false,
			consult_active: false
		};
		this.toggle = this.toggle.bind(this);
		this.toggleClass= this.toggleClass.bind(this);
	}

	toggle() {
		this.setState({
			modal: !this.state.modal,
			symptoms: true,
			symptoms_active: true,
			consult: false,
			consult_active: false,
		});
	}

	toggleClass(type) {
		if (type === 'symptoms') {
			this.setState({
				symptoms: !this.state.symptoms, 
				symptoms_active: !this.state.symptoms_active
			});
		}
		else {
			this.setState({
				consult: !this.state.consult,
				consult_active: !this.state.consult_active
			});
		}
	}

	render() {
		return (
			<div>
			{
				(this.props.text) ?
					<span className="pointer" onClick={this.toggle}>{this.props.text}</span>
				:
				<div tabIndex='1' className='assist-icon pointer need-doctor' onClick={this.toggle}>
					{
						(this.props.icon) ?
							<FontAwesomeIcon className='pointer' icon={this.props.icon} />
						:
						<span></span>
					}

					{
						(this.props.img) ?
							<img className='img-fluid doc_icon' alt='' src={this.props.img} />

						:
						<span></span>
					}
				</div>
			}

				<NeedDoctorContainer
					wrapper={this.props.wrapper}
					modal={this.state.modal}
					toggle={this.toggle}
					toggleClass={this.toggleClass}
					symptoms={this.state.symptoms}
					symptoms_active={this.state.symptoms_active}
					consult={this.state.consult}
					consult_active={this.state.consult_active}		
				/>
			</div>
		
		)
	}
}

export default NeedDoctor;