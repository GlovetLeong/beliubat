import React, { Component } from 'react';
import {
	Row, Col,
} from 'reactstrap';

const qs = require('qs');

class ProceedPay extends Component {

	render() {

		const values = qs.parse(this.props.location.search);

		return (
			<div>
				<Row>
					<Col className='small-title text-center' md='12'>
						<a className='btn btn-primary' href={values.payment_url} target='_blank' rel='noopener noreferrer'>Proceed To Paid</a>
					</Col>
				</Row>
			</div>
		)
	}
}

export default ProceedPay;

