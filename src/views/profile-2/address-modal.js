import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShippingFast } from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col,
	Button, 
	Modal, 
	ModalHeader, 
	ModalBody} from 'reactstrap';

var lodash = require('lodash');

class AdressModal extends Component {

	constructor(props) {
		super(props);
		this.state = {
            registerStatus: false
		};
        this.toggleChangePassword = this.toggleChangePassword.bind(this);
	}

	toggleChangePassword() {
		this.setState({modalChangePassword: !this.state.modalChangePassword});
	}

	static getDerivedStateFromProps(nextProps, prevState) {
		if(nextProps.addressStatus) {
			nextProps.forceToggle();
            nextProps.setAddressStatus(0);
		} 
        return null;
    }

	render() {
        const stateList = lodash.get(this.props.stateList, 'result', []);
		
        const shippingAddress1 = lodash.get(this.props.shipping_address, 'address_1', '');
        const shippingAddress2 = lodash.get(this.props.shipping_address, 'address_2', '');
        const shippingPostcode = lodash.get(this.props.shipping_address, 'address_postcode', '');
        const shippingState = lodash.get(this.props.shipping_address, 'address_stateid', '');

        const billingAddress1 = lodash.get(this.props.billing_address, 'address_1', '');
        const billingAddress2 = lodash.get(this.props.billing_address, 'address_2', '');
        const billingPostcode = lodash.get(this.props.billing_address, 'address_postcode', '');
        const billingState = lodash.get(this.props.billing_address, 'address_stateid', '');

		return (
			<div>
				<Modal className='modal-lg' isOpen={this.props.modal} toggle={this.props.toggle}>
					<ModalHeader toggle={this.props.toggle}>
						<FontAwesomeIcon className='spacing-right' icon={faShippingFast} /> 
						Address
					</ModalHeader>
					<ModalBody>
						<Row>
							<Col sm='6' xs='12'>
								<h4>Shipping Address</h4>
								<div className='form-group'>
									<input name='address_1' type='text' className='form-control' placeholder='Address 1' value={shippingAddress1} onChange={this.props.setShippingAddress}/>
								</div>
								<div className='form-group'>
									<input name='address_2' type='text' className='form-control' placeholder='Address 2' value={shippingAddress2} onChange={this.props.setShippingAddress}/>
								</div>
								<div className='form-group'>
									<input name='address_postcode' type='text' className='form-control' placeholder='Postcode' value={shippingPostcode} onChange={this.props.setShippingAddress}/>
								</div>
								<div className='form-group'>
                                    <select className='form-control' name='address_stateid' value={shippingState} onChange={this.props.setShippingAddress}>
                                		<option value=''>--Select A State--</option>
		                                {
		                                    stateList.map((state, index) =>
		                                    <option key={index} value={state.state_id}>{state.state_name}</option>
		                                    )
		                                }
									</select>
								</div>
								<div className='form-group'>
									<Button color='primary' block onClick={() => this.props.onSubmitChangeAddress('shipping')}>Update</Button>
								</div>
							</Col>

							<Col sm='6' xs='12'>
								<h4>Billing Address</h4>
								<div className='form-group'>
									<input name='address_1' type='text' className='form-control' placeholder='Address 1' value={billingAddress1} onChange={this.props.setBillingAddress}/>
								</div>
								<div className='form-group'>
									<input name='address_2' type='text' className='form-control' placeholder='Address 2' value={billingAddress2} onChange={this.props.setBillingAddress}/>
								</div>
								<div className='form-group'>
									<input name='address_postcode' type='text' className='form-control' placeholder='Postcode' value={billingPostcode} onChange={this.props.setBillingAddress}/>
								</div>
								<div className='form-group'>
                                    <select className='form-control' name='address_stateid' value={billingState} onChange={this.props.setBillingAddress}>
                                		<option value=''>--Select A State--</option>
                                        {
		                                    stateList.map((state, index) =>
		                                    <option key={index} value={state.state_id}>{state.state_name}</option>
		                                    )
		                                }
									</select>
								</div>
								<div className='form-group'>
									<Button color='primary' block onClick={() => this.props.onSubmitChangeAddress('billing')}>Update</Button>
								</div>
							</Col>

						</Row>
					</ModalBody>
				</Modal>
			</div>
		)
	}
}

export default AdressModal;