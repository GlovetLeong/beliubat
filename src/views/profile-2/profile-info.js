import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons';
import { faShippingFast } from '@fortawesome/free-solid-svg-icons';

import {
	Row, Col,
} from 'reactstrap';
import ProfileModal from './profile-modal';
import AddressModal from './address-modal';

var lodash = require('lodash');

class ProfileInfo extends Component {

	constructor(props) {
		super(props);
		this.state = {
            modalProfile: false,
            modalAddress: false,
		};
        this.handleChange = this.handleChange.bind(this);
        this.onSubmitUpdateProfile = this.onSubmitUpdateProfile.bind(this);
        this.toggleModalProfile = this.toggleModalProfile.bind(this);

        this.setShippingAddress = this.setShippingAddress.bind(this);
        this.setBillingAddress = this.setBillingAddress.bind(this);

        this.copyShipping = this.copyShipping.bind(this);

        this.onSubmitChangeAddress = this.onSubmitChangeAddress.bind(this);
        this.toggleModalAddress = this.toggleModalAddress.bind(this);
        this.forceToggle = this.forceToggle.bind(this);

        this.props.requestProfileDetailGet();
        this.props.getState();
	}

	handleChange(e) {
    	let profileDetail = Object.assign({}, this.props.profileDetail);
	    profileDetail[e.target.name] = e.target.value;
	    this.props.setProfileDetailData(profileDetail);
    }

	onSubmitUpdateProfile() {
		this.props.requestProfileUpdatePost(this.props.profileDetail);
	}

	toggleModalProfile() {
		this.setState({modalProfile: !this.state.modalProfile});
	}

	setShippingAddress(e) {
        let shippingAddress = Object.assign({}, this.props.profileDetail.shipping_address);
        shippingAddress[e.target.name] = e.target.value;
        this.props.setAddressData(shippingAddress, this.props.profileDetail, 1);
    }

    setBillingAddress(e) {
        let billingAddress = Object.assign({}, this.props.profileDetail.billing_address);
        billingAddress[e.target.name] = e.target.value;
        this.props.setAddressData(billingAddress, this.props.profileDetail, 2);
    }

    copyShipping(billingAddress) {
        var params = this.props.profileDetail.shipping_address;
        this.props.postChangeAddress(params, 2);
    }

    toggleModalAddress() {
		this.setState({modalAddress: !this.state.modalAddress});
	}

	forceToggle() {
		this.setState({modalAddress: false});
	}

    onSubmitChangeAddress(type){
        var params = {};
        if(type === 'shipping') {
            params = this.props.profileDetail.shipping_address;
            this.props.postChangeAddress(params, 1);
        }
        else if(type === 'billing') {
            params = this.props.profileDetail.billing_address;
            this.props.postChangeAddress(params, 2);
        }
    }

	render() {

		// console.log(this.props.addressStatus);

        const profileDetailLoading = this.props.profileDetailLoading;

		const readOnly = lodash.get(this.props.profileDetail, 'readOnlys', '');

		const email = lodash.get(readOnly, 'email', '');
        const firstname = lodash.get(readOnly, 'firstname', '');
        const lastname = lodash.get(readOnly, 'lastname', '');
        const mobile = lodash.get(readOnly, 'mobile', '');
        const dob = lodash.get(readOnly, 'dob', '');
        const ic_no = lodash.get(readOnly, 'ic_no', '');
        const gender = lodash.get(readOnly, 'gender', '');

        const shippingAddress1 = lodash.get(readOnly.shipping_address, 'address_1', '');
        const shippingAddress2 = lodash.get(readOnly.shipping_address, 'address_2', '');
        const shippingPostcode = lodash.get(readOnly.shipping_address, 'address_postcode', '');
        const shippingState = lodash.get(readOnly.shipping_address, 'address_state', '');

        const billingAddress1 = lodash.get(readOnly.billing_address, 'address_1', '');
        const billingAddress2 = lodash.get(readOnly.billing_address, 'address_2', '');
        const billingPostcode = lodash.get(readOnly.billing_address, 'address_postcode', '');
        const billingState = lodash.get(readOnly.billing_address, 'address_state', '');

		return (
			<div>
				{
					(profileDetailLoading) ?
						<div className='text-center'>
							<div className="lds-ripple">
								<div></div>
								<div></div>
							</div>
						</div>
					:
					<Row>
						<Col md='6'>
							<div className="white with-radius px-3 py-3">
								<div className="small-title mb-3">
									<FontAwesomeIcon className='spacing-right' icon={faUserAlt} /> 
									Profile | <small><button className='link-button' onClick={this.toggleModalProfile}>Edit</button></small>
								</div>
								<div><b>Name</b> - {firstname} {lastname}</div>
								<div><b>Email</b> - {email}</div>
								<div><b>Mobile</b> - {mobile}</div>
								<div><b>DOB</b> - {dob}</div>
								<div><b>IC NO</b> - {ic_no}</div>
								<div><b>Gender</b> - {gender}</div>
							</div>
						</Col>

						<Col md='6'>
							<div className="white with-radius px-3 py-3">
								<div className="small-title mb-3">
								<FontAwesomeIcon className='spacing-right' icon={faShippingFast} /> 
								Address | <small><button className='link-button' onClick={this.toggleModalAddress}>Edit</button></small>
								</div>
								<div><b>Shipping</b></div>
								<div>
									{
										(shippingAddress1) ?
											<span>{shippingAddress1}, {shippingAddress2}, {shippingPostcode}, {shippingState}</span>
										:
											<span>-</span>
									}
								</div>
								<br/>
								<div><b>Billing</b> | <small><button className='link-button' onClick={() => this.copyShipping(this.props.profileDetail.shipping_address)}>copy shipping</button></small></div>
								<div>
									{
										(billingAddress1) ?
											<span>{billingAddress1}, {billingAddress2}, {billingPostcode}, {billingState}</span>
										:
											<span>-</span>
									}
								</div>
							</div>
						</Col>

						<ProfileModal 
							wrapper={this.props.wrapper}
							modal={this.state.modalProfile}
							toggle={this.toggleModalProfile}
							profileDetail={this.props.profileDetail}
							handleChange={this.handleChange}
							onSubmitUpdateProfile={this.onSubmitUpdateProfile}
						/>

						<AddressModal 
							wrapper={this.props.wrapper}
							modal={this.state.modalAddress}
							toggle={this.toggleModalAddress}
							forceToggle={this.forceToggle}
							stateList={this.props.stateList}
							shipping_address={this.props.profileDetail.shipping_address}
							billing_address={this.props.profileDetail.billing_address}
							addressStatus={this.props.addressStatus}
							setAddressStatus={this.props.setAddressStatus}
							setShippingAddress={this.setShippingAddress}
							setBillingAddress={this.setBillingAddress}
							onSubmitChangeAddress={this.onSubmitChangeAddress}
						/>
					</Row>
				}
			</div>
		)
	}
}

export default ProfileInfo;