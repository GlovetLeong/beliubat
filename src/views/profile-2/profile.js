import React, { Component } from 'react';
import ProfileContainer from '../../redux/containers/profileContainer';

class Profile extends Component {

	render() {
		return (
			<div className="mb-3">
				<div className='small-title mb-3'>
					Manage Account
				</div>
				<ProfileContainer
					wrapper={this.props.wrapper}
				/>
			</div>
		)
	}
}

export default Profile;