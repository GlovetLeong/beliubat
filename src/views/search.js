import React, { Component } from 'react';
import {
	Row, Col,
} from 'reactstrap';
import ItemList from './item/item-list';

class Search extends Component {

	constructor(props) {
		super(props);
		this.state = {
            params: {
            	'search_title': this.props.search_key
            },
            onSearch: false
		};
		this.searchKeyHandle = this.searchKeyHandle.bind(this);
	}

	searchKeyHandle(search_key) {
		this.setState({ params: 
			{
				'search_title': search_key
			},
			search: true
		});
	}

	static getDerivedStateFromProps(nextProps, preState) {
		if(nextProps.search_key && nextProps.onSearch) {
			try {
            	nextProps.onSearchHandle();
            } catch(e) {

            }
            return {
            	params: {
					'search_title': nextProps.search_key
				}
            };
		} 
        return null;
    }


	render() {

		const search_key = this.props.search_key;

		return (
			<div>
				<Row>
					<Col className='mb-3' sm='12'>
						<div className='small-title'>Search Result : {search_key}</div>
					</Col>

					<Col sm='12'>
						<ItemList 
							wrapper={this.props.wrapper}
							params={this.state.params}
							itemCall={this.props.itemCall}
							toggleCall={this.props.toggleCall}
							type={this.props.type}
						/>
					</Col>
				</Row>
			</div>
		)
	}
}

export default Search;