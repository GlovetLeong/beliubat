import { toast } from 'react-toastify';
import React, { Component } from 'react';
import { Redirect } from 'react-router';
import Axios  from '../utils/axios';
import Constant  from '../utils/constant';
import {
	Row, Col,
} from 'reactstrap';

const qs = require('query-string');

class Verify extends Component {
constructor(props) {
		super(props);
		const values = qs.parse(this.props.location.search);

		this.state = {
            verify: false,
            email:  values.email,
            code: values.code
		};


        this.onSubmitVerify()
	}

	onSubmitVerify() {
		const formData = new FormData();
		let _this = this;
        formData.append('email', _this.state.email);
        formData.append('code', _this.state.code);

		Axios({
            method: 'Post',
            url: Constant.api_url + 'member/verify_email',
            data: formData
        })
        .then(function(response){

        	if(response.data.result.status === 'success') {
    			_this.setState({ verify: true });
    			toast.success('Successful Activated', {
	                position: toast.POSITION.TOP_CENTER
	            });
            }
            else if(response.data.result.status === 'fail') {
    			_this.setState({ verify: false });
	            toast.error('Error Verify Code', {
	                position: toast.POSITION.TOP_CENTER
	            });
            }
	    });
	}


	render() {
		if (this.props.location.search === '') {
    		return <Redirect
			to={{
				pathname: '/',
			}}
			/>
		}


		return (
			<Row>
				<Col className='mission text-center white' xs='12'>
					<div className='small-title mb-3'>Verify</div>
					<div>{ this.state.email }</div>
					<div>
						{
							(this.state.verify) ?
								<div>
									Your account have been successful activated,
									please proceed to login.
								</div>
							:
								<span></span>
						}
					</div>
				</Col>
			</Row>
		)
	}
}

export default Verify;
