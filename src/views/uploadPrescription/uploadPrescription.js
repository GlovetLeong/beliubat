import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import UploadPrescriptionContainer from '../../redux/containers/uploadPrescriptionContainer';

class UploadPrescription extends Component {

	constructor(props) {
		super(props);
		this.state = {
			modal: false
		};
		this.toggle = this.toggle.bind(this);
	}

	toggle() {
		this.setState({
			modal: !this.state.modal
		});
	}

	render() {
		return (
			<div>
				{
					(this.props.text) ?
						<span className="pointer" onClick={this.toggle}>{this.props.text}</span>
					:
					<div tabIndex='1' className='assist-icon pointer need-prescription' onClick={this.toggle}>
						<FontAwesomeIcon className='pointer' icon={this.props.icon} />
					</div>
				}
				<UploadPrescriptionContainer
					wrapper={this.props.wrapper}
					modal={this.state.modal}
					toggle={this.toggle}
				/>
			</div>
		)
	}
}

export default UploadPrescription;