import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPrescription } from '@fortawesome/free-solid-svg-icons';
import { faCloudDownloadAlt  } from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col,
	Button, 
	Modal, 
	ModalHeader, 
	ModalBody, 
	ModalFooter} from 'reactstrap';

import icon_upload from '../../images/icon_uploadprescription_popup.png';

class UploadPrescriptionModal extends Component {

	constructor(props) {
		super(props);
		this.state = {
		  	previewVisible: false,
		  	prescriptionMessage: '',
            previewImage: '',
            fileList: [],
            files: '',
            preview: '',
            disable: false
		};
        this.handleChange = this.handleChange.bind(this);
        this.handlePrescriptionChange = this.handlePrescriptionChange.bind(this);
        this.onSubmitUpload = this.onSubmitUpload.bind(this);
	}

	handleChange(e) {
        this.setState({
			files: e.target.files[0], 
			preview: URL.createObjectURL(e.target.files[0]) 
     	});
    }

    handlePrescriptionChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

	onSubmitUpload() {
    	const params = {
			message: this.state.prescriptionMessage,
    		files: this.state.files
        }
        this.props.requestUploadPrescriptionPost(params);

        this.setState({
			disable: true, 
     	});
	}

	static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.uploadPrescriptionStatus) {
            nextProps.toggle();
            nextProps.setuploadPrescriptionStatus(0);
            return {
				prescriptionMessage: '',
				files: '',
				preview: ''
			};
        }
        return null;
    }

	render() {

		if (this.props.uploadPrescriptionStatus) {
    		return <Redirect
			to={{
				pathname: '/prescription-record',
			}}
			/>
		}

		return (
			<div>
				<Modal className='modal-lg' isOpen={this.props.modal} toggle={this.props.toggle}>
					<ModalHeader toggle={this.props.toggle}>
						<FontAwesomeIcon className='spacing-right' icon={faPrescription} /> 
						Upload a Prescription
					</ModalHeader>
					<ModalBody>
						<Row>
							<Col className='mb-3' sm='12' md={{ size: 10, offset: 1 }}>
								<Row>
									<Col className='text-center' sm='3'>
										<img className='icon-upload img-fluid' alt='' src={icon_upload} />
									</Col>
									<Col sm='9'>
										<div className='small-title prescription-title'>Upload a Prescription</div>
										<div className='doc-description'>"DOC" icon is mean this medical neeed a approvement from Professional Doctor</div>
									</Col>
								</Row>
							</Col>

							<Col className='text-center p-0' sm='12' md={{ size: 10, offset: 1 }}>
								<div className='textarea-box'>
									<textarea className='form-control' placeholder='Tell us your details...' name='prescriptionMessage' onChange={this.handlePrescriptionChange}/>
								</div>
							</Col>

							<Col className='upload-form text-center' sm='12' md={{ size: 10, offset: 1 }}>
								<div className='my-3'><small>Please upload a prescription for medical approvement (e.g Image of prescription)</small></div>
								<div className="uploadBox">
                                    <div className="fileContainer">
                                        <FontAwesomeIcon icon={faCloudDownloadAlt} /> 
                                        <input type='file' onChange={this.handleChange} />
                                    </div>
                                    <div className="uploadImg">
                                        <img className='img-fluid' alt='' src={this.state.preview} />
                                    </div>
                                </div>
							</Col>
						</Row>
					</ModalBody>
					<ModalFooter>
						{
							(!this.state.disable) ?
								<Button color='primary' onClick={this.onSubmitUpload}>
									<FontAwesomeIcon className='spacing-right' icon={faCloudDownloadAlt} /> Upload
								</Button>
							:
								<Button color='danger'>
									Waiting Request
								</Button>
						}

					</ModalFooter>
				</Modal>
			</div>
		)
	}
}

export default UploadPrescriptionModal;