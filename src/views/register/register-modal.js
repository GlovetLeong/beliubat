import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col,
	Button, 
	Modal, 
	ModalHeader, 
	ModalBody, 
	ModalFooter} from 'reactstrap';

class RegisterModal extends Component {

	constructor(props) {
		super(props);
		this.state = {
			email: '',
            firstname: '',
            lastname: '',
            password: '',
            confirm_password: '',
            mobile: '',
            dob: '',
            ic_no: '',
            gender: '',
            modalThankYou: false
		};
        this.handleChange = this.handleChange.bind(this);
        this.handleIcChange = this.handleIcChange.bind(this);
        this.onSubmitRegister = this.onSubmitRegister.bind(this);
        this.toggleThankYou = this.toggleThankYou.bind(this);
	}

	toggleThankYou() {
		this.setState({modalThankYou: !this.state.modalThankYou});
	}

	handleChange(e) {
    	this.setState({ [e.target.name]: e.target.value });
    }

    handleIcChange(e) {
    	if (/^\d+$/.test(e.target.value)) {
    		this.setState({ [e.target.name]: e.target.value });
    	}

    	if (e.target.value.length === 6 ) {
    		let date = e.target.value;
    		let d = new Date(date[0] + date[1], date[2] + date[3], date[4] + date[5]);
            let year = d.getFullYear();
            let month = date[2] + date[3];
            let day = date[4] + date[5];
            let dob = year + '-' + month + '-' + day;
            this.setState({ dob: dob });
    	}
    }

	onSubmitRegister() {
		const params = {
		 	email: this.state.email,
    		firstname: this.state.firstname,
            lastname: this.state.lastname,
    		password: this.state.password,
    		confirm_password: this.state.confirm_password,
    		mobile: this.state.mobile,
    		dob: this.state.dob,
            ic_no: this.state.ic_no,
    		gender: this.state.gender
		}

		this.props.requestRegisterPost(params);
	}
	
	static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.registerStatus) {
            nextProps.toggle();
            nextProps.setStatus(0);
            return { modalThankYou: true};
        }
        return null;
    }

	render() {
		return (
			<div>
				<Modal className='modal-lg' isOpen={this.props.modal} toggle={this.props.toggle}>
					<ModalHeader>
						<FontAwesomeIcon className='spacing-right' icon={faUserAlt} /> 
						Register
					</ModalHeader>
					<ModalBody>
						<Row>
							<Col sm='6' xs='12'>
								<h4>Account Data</h4>
								<div className='form-group'>
									<input name='email' type='text' className='form-control' placeholder='Email' onChange={this.handleChange}/>
								</div>
								<div className='form-group'>
									<input name='firstname' type='text' className='form-control' placeholder='First Name' onChange={this.handleChange}/>
								</div>
								<div className='form-group'>
									<input name='lastname' type='text' className='form-control' placeholder='Last Name' onChange={this.handleChange}/>
								</div>
								<div className='form-group'>
									<input name='password' type='password' className='form-control' placeholder='Password' onChange={this.handleChange}/>
								</div>
								<div className='form-group'>
									<input name='confirm_password' type='password' className='form-control' placeholder='Confirm Password' onChange={this.handleChange}/>
								</div>
							</Col>

							<Col sm='6' xs='12'>
								<h4>Personal Data</h4>
								<div className='form-group'>
									<input name='mobile' type='text' className='form-control' placeholder='Mobile' onChange={this.handleChange}/>
								</div>
								<div className='form-group'>
									<input name='ic_no' type='text' className='form-control' placeholder='IC No' onChange={this.handleIcChange} value={this.state.ic_no}/>
								</div>
								<div className='form-group'>
									<input name='dob' type='text' className='form-control' placeholder='DOB' value={this.state.dob} disabled />
								</div>
								<div className='form-group'>
                                    <select className='form-control' name='gender' onChange={this.handleChange}>
                                		<option value=''>Gender</option>
                                        <option value='M'>Male</option>
                                        <option value='F'>Female</option>
									</select>
								</div>
							</Col>
						</Row>
					</ModalBody>
					<ModalFooter>
						<Button color='primary' onClick={this.onSubmitRegister}>Submit</Button>{' '}
					</ModalFooter>
				</Modal>

				<Modal className='modal-lg' isOpen={this.state.modalThankYou} toggle={this.toggleThankYou}>
					<ModalHeader>
						<FontAwesomeIcon className='spacing-right' icon={faUserAlt} /> 
						Registered Successful 
					</ModalHeader>
					<ModalBody>
						<Row>
							<Col xs='12'>
								<div className='text-center'>
									We have send an verification email to your registered email, please verify your email to activate your account
								</div>
							</Col>
						</Row>
					</ModalBody>
				</Modal>
			</div>
		)
	}
}

export default RegisterModal;