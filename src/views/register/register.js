import React, { Component } from 'react';
import RegisterContainer from '../../redux/containers/registerContainer';

class Register extends Component {
	render() {
		return (
			<div className='icon-item'>
				<RegisterContainer
					wrapper={this.props.wrapper}
					modal={this.props.modal}
					toggle={this.props.toggle}
				/>
			</div>
		)
	}
}

export default Register;