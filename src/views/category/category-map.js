import React, { Component } from 'react';

const lodash = require('lodash');

class CategoryMap extends Component {

	constructor(props) {
		super(props);
		this.state = {
			category_id: '',
			active: false,
		}
		this.categoryClickActive = this.categoryClickActive.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.props.requestCategoryGet();
	}

	handleChange(e) {
    	this.setState({ [e.target.name]: e.target.value });
    	this.categoryClickActive(e.target.value);
    }

	categoryClickActive(id) {
		this.props.categorySelect(id);
		// this.props.categoryClickActive(this.props.categoryList, id);
	}

	render() {

        const categoryList = lodash.get(this.props.categoryList, 'result', []);

		return (
			<div className='select'>
				<select name="category_id" onChange={this.handleChange}>
					{
						categoryList.map((data, index) =>
	                    	<option key={index} value={data.id}>{data.name_en}</option>
	                    )
					}
				</select>
     		</div>
		)
	}
}

export default CategoryMap;
