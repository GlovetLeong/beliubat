import React, { Component } from 'react';
import {
	Row, Col} from 'reactstrap';
import ItemList from '../item/item-list';
import CategoryContainer from '../../redux/containers/categoryContainer';

class Category extends Component {

	constructor(props) {
		super(props);
		this.state = {
			active: false,
			category: '',
			params: {}
		}
		this.categorySelect = this.categorySelect.bind(this)
	}

	categorySelect(category_id) {
		this.setState({
			params: 
			{
				'category_id': category_id
			},
			category: category_id
		});
	}

	render() {
		return (
			<Row>
				<Col md='12'>
					<div className='small-title mb-3'>
						Search By Category 
					</div>
				</Col>
				<Col className='mb-3' lg='3' md='12' sm='12'>
					<CategoryContainer 
						categorySelect={this.categorySelect}
						wrapper={this.props.wrapper}
					/>
				</Col>
				<Col className='category-item' lg='12' md='12' sm='12'>
					<ItemList 
						wrapper={this.props.wrapper}
						params={this.state.params}
						itemCall={this.props.itemCall}
						toggleCall={this.props.toggleCall}
						type={this.props.type}
					/>
				</Col>
			</Row>
		)
	}
}

export default Category;