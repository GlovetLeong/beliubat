import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col,
	Button, 
	Modal, 
	ModalHeader, 
	ModalBody, 
	ModalFooter} from 'reactstrap';

class LoginModal extends Component {

	constructor(props) {
		super(props);
		this.state = {
			old_password: '',
			new_password: '',
			confirm_password: '',
		};
        this.handleChange = this.handleChange.bind(this);
        this.onSubmitChangePassword = this.onSubmitChangePassword.bind(this);
	}

	handleChange(e) {
    	this.setState({ [e.target.name]: e.target.value });
    }

	onSubmitChangePassword() {
		const params = {
			old_password: this.state.old_password,
			new_password: this.state.new_password,
			confirm_password: this.state.confirm_password
		}
		this.props.requestChangePasssword(params);
	}

	static getDerivedStateFromProps(nextProps) {
        if(nextProps.changePasswordStatus) {
            nextProps.toggle();
            nextProps.setStatus(0);
        }
        return null;
    }

	render() {
		return (
			<div>
				<Modal isOpen={this.props.modal} toggle={this.props.toggle} className='loginModal'>
					<ModalHeader toggle={this.props.toggle}>
						<FontAwesomeIcon className='spacing-right' icon={faUserAlt} /> 
						Change Password
					</ModalHeader>
					<ModalBody>
						<Row>
							<Col xs='12'>
								<div className="form-group">
									<input name='old_password' type='password' className='form-control' placeholder='Old Password' onChange={this.handleChange}/>
								</div>
								<div className="form-group">
									<input name='new_password' type='password' className='form-control' placeholder='New Password' onChange={this.handleChange}/>
								</div>
								<div className="form-group">
									<input name='confirm_password' type='password' className='form-control' placeholder='Confirm Password' onChange={this.handleChange}/>
								</div>
							</Col>
						</Row>

					</ModalBody>
					<ModalFooter>
						<Button color='primary' block onClick={this.onSubmitChangePassword}>Update</Button>
					</ModalFooter>
				</Modal>
			</div>
		)
	}
}

export default LoginModal;