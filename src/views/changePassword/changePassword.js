import React, { Component } from 'react';
import ChangePasswordContainer from '../../redux/containers/changePasswordContainer';

class ChangePassword extends Component {

	render() {
		return (
			<div className='icon-item'>
				<ChangePasswordContainer
					wrapper={this.props.wrapper}
					modal={this.props.modal}
					toggle={this.props.toggle}
				/>
			</div>
		)
	}
}

export default ChangePassword;