import React, { Component } from 'react';
import ConsultantRecordContainer from '../../redux/containers/consultantRecordContainer';

class ConsultantRecord extends Component {
	render() {
		return (
			<ConsultantRecordContainer
				wrapper={this.props.wrapper}
			/>
		)
	}
}

export default ConsultantRecord;