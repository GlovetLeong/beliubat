import React, { Component } from 'react';
import { Row, Col, Button } from 'reactstrap';

import ConsultantDetailModal from './consultantDetail-modal';

var lodash = require('lodash');

class consultantRecordMap extends Component {

	constructor(props) {
		super(props);
        this.state = {
            modal: false
        };
		this.props.requestConsultantRecordGet();
        this.toggle = this.toggle.bind(this);
        this.viewDetail = this.viewDetail.bind(this);
	}

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    viewDetail(consultation_id) {
        this.props.requestConsultantRecordDetailGet(consultation_id);
        this.toggle();
    }

	render() {
        const consultantRecordLoading = this.props.consultantRecordLoading;
		const consultantRecordList = lodash.get(this.props.consultantRecordList, 'result', []);
        const consultantRecordDetailList = lodash.get(this.props.consultantRecordDetailList, 'result', []);

		return (

			<div className="mb-3">
                <div className='small-title mb-3'>
                    Consultant Records
                </div>

                <div>
                {
                    (consultantRecordLoading) ?
                        <div className='text-center'>
                            <div className="lds-ripple">
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    :
                    <div>
                      {
                            (consultantRecordList > 0) ?
                        <Row>
                            <Col className='cart-list-box' md='12'>
                                <Col className='cart-list-header' md='12'>
                                    <Row>
                                        <Col md='1'>
                                            No
                                        </Col>
                                        <Col md='4'>
                                            Symptoms
                                        </Col>
                                        <Col md='4'>
                                            Doctor Message
                                        </Col>
                                        <Col md='2'>
                                            Date
                                        </Col>
                                        <Col md='1'>
                                            Status
                                        </Col>
                                    </Row>
                                </Col>
                                {
                                    consultantRecordList.map((data, index) =>
                                        <Col className='cart-list-content white my-2 px-2' md='12' key={index++}>
                                            <Row>
                                                <Col className='mb-3' md='1'>
                                                    <span className="mobile-view spacing-right"><b>No</b> : </span>
                                                    { index }
                                                </Col>
                                                <Col className='mb-3' md='4'>
                                                    <span className="mobile-view spacing-right"><b>Message</b> : </span>
                                                    { data.message }
                                                </Col>
                                                <Col className='mb-3' md='4'>
                                                    <span className="mobile-view spacing-right"><b>Doctor Message</b> : </span>
                                                    { data.doctor_message ? data.doctor_message : '-' }
                                                </Col>
                                                <Col className='mb-3' md='2'>
                                                    <span className="mobile-view spacing-right"><b>Date</b> : </span>
                                                    { data.date }
                                                </Col>
                                                <Col md='1'>
                                                    <span className="mobile-view spacing-right"><b>Status</b> : </span>
                                                    <b>{ data.status }</b>
                                                    {
                                                        (data.status === 'Approved') ?

                                                        <Button color='primary' block onClick={() => this.viewDetail(data.consultation_id)}>View</Button>

                                                        :

                                                        <span></span>
                                                    }
                                                </Col>
                                            </Row>
                                        </Col>
                                    )
                                }
                            </Col>
                        </Row>
                        :
                        <Col className='text-center'>
                            Empty
                        </Col>
                    }
                    </div>
                    }
                </div>

                <ConsultantDetailModal
                    wrapper={this.props.wrapper}
                    modal={this.state.modal}
                    toggle={this.toggle}
                    consultantRecordDetailList={consultantRecordDetailList}
                    consultantRecordDetailLoading={this.props.consultantRecordDetailLoading}
                />
			</div>
		)
	}
}

export default consultantRecordMap;