import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col,
	Button, 
	Modal, 
	ModalHeader, 
	ModalBody, 
	ModalFooter} from 'reactstrap';
import ChangePassword from '../changePassword/changePassword';

var lodash = require('lodash');

class ProfileModal extends Component {

	constructor(props) {
		super(props);
		this.state = {
            registerStatus: false
		};
        this.handleChange = this.handleChange.bind(this);
        this.onSubmitUpdateProfile = this.onSubmitUpdateProfile.bind(this);
        this.toggleChangePassword = this.toggleChangePassword.bind(this);

        this.props.requestProfileDetailGet();
	}

	handleChange(e) {
    	let profileDetail = Object.assign({}, this.props.profileDetail);
	    profileDetail[e.target.name] = e.target.value;
	    this.props.setProfileDetailData(profileDetail);
    }

	onSubmitUpdateProfile() {
		this.props.requestProfileUpdatePost(this.props.profileDetail);
	}

	toggleChangePassword() {
		this.setState({modalChangePassword: !this.state.modalChangePassword});
	}

	static getDerivedStateFromProps(nextProps) {
		if(nextProps.loginStatus) {
			nextProps.wrapper.getPatientSession();
			nextProps.wrapper.checkPatientSession();
		} 
        return null;
    }

	render() {

		const email = lodash.get(this.props.profileDetail, 'email', '');
        const firstname = lodash.get(this.props.profileDetail, 'firstname', '');
        const lastname = lodash.get(this.props.profileDetail, 'lastname', '');
        const mobile = lodash.get(this.props.profileDetail, 'mobile', '');
        const dob = lodash.get(this.props.profileDetail, 'dob', '');
        const ic_no = lodash.get(this.props.profileDetail, 'ic_no', '');
        const gender = lodash.get(this.props.profileDetail, 'gender', '');

		return (
			<div>
				<Modal className='modal-lg' isOpen={this.props.modal} toggle={this.props.toggle}>
					<ModalHeader toggle={this.props.toggle}>
						<FontAwesomeIcon className='spacing-right' icon={faUserAlt} /> 
						Profile
					</ModalHeader>
					<ModalBody>
						<Row>
							<Col sm='6' xs='12'>
								<h4>Account Data</h4>
								<div className='form-group'>
									<input name='email' type='text' className='form-control' placeholder='Email' value={email} disabled />
								</div>
								<div className='form-group'>
									<input name='firstname' type='text' className='form-control' placeholder='First Name' value={firstname} onChange={this.handleChange} />
								</div>
								<div className='form-group'>
									<input name='lastname' type='text' className='form-control' placeholder='Last Name' value={lastname} onChange={this.handleChange} />
								</div>
							</Col>
							<Col sm='6' xs='12'>
								<h4>Personal Data</h4>
								<div className='form-group'>
									<input name='mobile' type='text' className='form-control' placeholder='Mobile' value={mobile} onChange={this.handleChange} />
								</div>
								<div className='form-group'>
									<input name='dob' type='text' className='form-control' placeholder='Dob' value={dob} onChange={this.handleChange} />
								</div>
								<div className='form-group'>
									<input name='ic_no' type='text' className='form-control' placeholder='IC No' value={ic_no} onChange={this.handleChange}/>
								</div>
								<div className='form-group'>
                                    <select className='form-control' name='gender' value={gender} onChange={this.handleChange}>
                                		<option value=''>Gender</option>
                                        <option value='M'>Male</option>
                                        <option value='F'>Female</option>
									</select>
								</div>
								<div className='form-group'>
								</div>
							</Col>
						</Row>
					</ModalBody>
					<ModalFooter>
						<Button color='primary' block onClick={this.onSubmitUpdateProfile}>Update</Button>
					</ModalFooter>
					<div className='text-center mb-2'>
						<small><button className='link-button' onClick={this.toggleChangePassword}>Change Password</button ></small>
					</div>
				</Modal>
				<ChangePassword 
					wrapper={this.props.wapper}
					modal={this.state.modalChangePassword}
					toggle={this.toggleChangePassword}
				/>
			</div>
		)
	}
}

export default ProfileModal;