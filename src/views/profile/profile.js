import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ProfileContainer from '../../redux/containers/profileContainer';

class Profile extends Component {

	constructor(props) {
		super(props);
		this.state = {
			modal: false
		};
		this.toggle = this.toggle.bind(this);
	}

	toggle() {
		this.setState({
			modal: !this.state.modal
		});
	}

	render() {
		return (
			<div className='icon-item'>
				<FontAwesomeIcon className='pointer' icon={this.props.icon} onClick={this.toggle} />
				<ProfileContainer
					wrapper={this.props.wrapper}
					modal={this.state.modal}
					toggle={this.toggle}
				/>
			</div>
		)
	}
}

export default Profile;