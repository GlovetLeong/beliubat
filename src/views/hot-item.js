import React, { Component } from 'react';
import {
	Row, Col,
} from 'reactstrap';
import ItemList from './item/item-list';
import hotItem_banner from '../images/promotion_bg1.png';

class HotItem extends Component {

	constructor(props) {
		super(props);
		this.state = {
            hotItem_params : {
            	'hot_item': 1
            },
		};
	}

	render() {
		return (
			<div>
				<Row>
					<Col className='banner' sm='12'>
						<img className='img-fluid' alt='' src={hotItem_banner} />
					</Col>

					<Col sm='12'>
						<ItemList 
							wrapper={this.props.wrapper}
							params={this.state.discount_params}
							itemCall={this.props.itemCall}
							toggleCall={this.props.toggleCall}
							type={this.props.type}
						/>
					</Col>
				</Row>
			</div>
		)
	}
}

export default HotItem;