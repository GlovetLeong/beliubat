import React, { Component } from 'react';
import { Row, Col, Button } from 'reactstrap';

import PrescriptionDetailModal from './prescriptionDetail-modal';

var lodash = require('lodash');

class prescriptionRecordMap extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };
        this.props.requestPrescriptionRecordGet();
        this.toggle = this.toggle.bind(this);
        this.viewDetail = this.viewDetail.bind(this);
    }


    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    viewDetail(consultation_id) {
        this.props.requestPrescriptionRecordDetailGet(consultation_id);
        this.toggle();
    }

    render() {
        const prescriptionRecordLoading = this.props.prescriptionRecordLoading;
        const prescriptionRecordList = lodash.get(this.props.prescriptionRecordList, 'result', []);
        const prescriptionRecordDetailList = lodash.get(this.props.prescriptionRecordDetailList, 'result', []);
        console.log(prescriptionRecordDetailList);

        return (

            <div className="mb-3">
                <div className='small-title mb-3'>
                    Prescription Record
                </div>

                <div>
                {
                    (prescriptionRecordLoading) ?
                        <div className='text-center'>
                            <div className="lds-ripple">
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    :

                    <div>
                    {
                        (prescriptionRecordList > 0) ?
                            <Row>
                                <Col className='cart-list-box' md='12'>
                                    <Col className='cart-list-header' md='12'>
                                        <Row>
                                            <Col md='1'>
                                                No
                                            </Col>
                                            <Col md='2'>
                                                Image
                                            </Col>
                                            <Col md='2'>
                                                Message
                                            </Col>
                                             <Col md='2'>
                                                Doctor Message
                                            </Col>
                                            <Col md='2'>
                                                Date
                                            </Col>
                                            <Col md='1'>
                                                Status
                                            </Col>
                                        </Row>
                                    </Col>
                                    {
                                        prescriptionRecordList.map((data, index) =>
                                            <Col className='cart-list-content white my-2 px-2' md='12' key={index++}>
                                                <Row>
                                                    <Col className='mb-3' md='1'>
                                                        <span className="mobile-view spacing-right"><b>No</b> : </span>
                                                        { index }
                                                    </Col>
                                                    <Col className='mb-3' md='2'>
                                                        <img className='img-fluid' alt='' src={ data.attachment ? data.attachment : '-' } />
                                                    </Col>
                                                    <Col className='mb-3' md='2'>
                                                        <span className="mobile-view spacing-right"><b>Message</b> : </span>
                                                        { data.message ? data.message : '-' }
                                                    </Col>
                                                    <Col className='mb-3' md='2'>
                                                        <span className="mobile-view spacing-right"><b>Doctor Message</b> : </span>
                                                        { data.doctor_message ? data.doctor_message : '-' }
                                                    </Col>
                                                    <Col className='mb-3' md='2'>
                                                        <span className="mobile-view spacing-right"><b>Date</b> : </span>
                                                        { data.date }
                                                    </Col>
                                                    <Col md='1'>
                                                        <span className="mobile-view spacing-right"><b>Status</b> : </span>
                                                        <b>{ data.status }</b>
                                                        {
                                                            (data.status === 'Approved') ?

                                                            <Button color='primary' block onClick={() => this.viewDetail(data.consultation_id)}>View</Button>

                                                            :

                                                            <span></span>
                                                        }
                                                    </Col>
                                                </Row>
                                            </Col>
                                        )
                                    }
                                </Col>
                            </Row>
                        :
                        <Col className='text-center'>
                            Empty
                        </Col>
                    }
                    </div>
                    }
                </div>

                 <PrescriptionDetailModal
                    wrapper={this.props.wrapper}
                    modal={this.state.modal}
                    toggle={this.toggle}
                    prescriptionRecordDetailList={prescriptionRecordDetailList}
                />
            </div>
        )
    }
}

export default prescriptionRecordMap;