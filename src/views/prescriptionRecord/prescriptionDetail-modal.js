import React, { Component } from 'react';
import {
	Row, Col,
	Modal, 
	ModalHeader, 
	ModalBody, 
} from 'reactstrap';

class ConsultantDetailModal extends Component {

	render() {

		const prescriptionRecordDetailList = this.props.prescriptionRecordDetailList;

		return (
			<div>
				<Modal isOpen={this.props.modal} toggle={this.props.toggle}>
					<ModalHeader toggle={this.props.toggle}>
						Prescription Details
					</ModalHeader>
					<ModalBody>
						<div>
						{
							(prescriptionRecordDetailList.length > 0) ?
								<Row>
									{
			                            prescriptionRecordDetailList.map((data, index) =>
											<Col xs='12' key={index++}>
												<div className='text-center'><b>{data.product_name_en}</b></div>
												<div className='text-center'><b>Timing</b></div>
												<div className='text-center'>
													<div className='day-box'>
														<div><b>Morning</b></div>
														<div>{data.timing_morning} Pills</div>
													</div>

													<div className='day-box'>
														<div><b>Afternoon</b></div>
														<div>{data.timing_afternoon} Pills</div>
													</div>

													<div className='day-box'>
														<div><b>Night</b></div>
														<div>{data.timing_night} Pills</div>
													</div>
												</div>
												<div className='text-center'><b>{data.whentoeat === 'before' ? 'Before Meal' : 'After Meal'}</b></div>

												<div className='line'></div>
											</Col>
										)
									}
								</Row>
							:

							<div className='text-center'>Empty</div>
						}
						</div>

					</ModalBody>
				</Modal>
				
			</div>
		)
	}
}

export default ConsultantDetailModal;