import React, { Component } from 'react';
import PrescriptionRecordContainer from '../../redux/containers/prescriptionRecordContainer';

class ConsultantRecord extends Component {
	render() {
		return (
			<PrescriptionRecordContainer
				wrapper={this.props.wrapper}
			/>
		)
	}
}

export default ConsultantRecord;