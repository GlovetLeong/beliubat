import React, { Component } from 'react';
import {
	Row, Col,
} from 'reactstrap';
import ItemList from './item/item-list';
import HotItemListHorizontal  from './item/item-list-horizontal';
import '../Swiper.css';

import discount_banner from '../images/promotion_bg2.jpg';
import hotItem_banner from '../images/promotion_bg1.png';

class Home extends Component {

	constructor(props) {
		super(props);
		this.state = {
            discount_params : {},
            hotItem_params : {
            	'hot_item': 1
            }
		};
	}

	render() {
		return (
			<div>
				<Row>
					<Col className='banner' sm='12'>
						<img className='img-fluid' alt='' src={discount_banner} />
					</Col>

					<Col sm='12'>
						<ItemList 
							wrapper={this.props.wrapper}
							params={this.state.discount_params}
							itemCall={this.props.itemCall}
							toggleCall={this.props.toggleCall}
							type={this.props.type}
						/>
					</Col>


					<Col className='banner' sm='12'>
						<img className='img-fluid' alt='' src={hotItem_banner} />
					</Col>

					<Col className='mb-3' sm='12'>
						<HotItemListHorizontal 
							wrapper={this.props.wrapper}
							params={this.state.hotItem_params}
							itemCall={this.props.itemCall2}
							toggleCall={this.props.toggleCall}
							type={this.props.type2}
						/>
					</Col>
				</Row>
			</div>
		)
	}
}

export default Home;