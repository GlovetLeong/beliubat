import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col,
	Modal, 
	ModalHeader, 
	ModalBody, 
	TabContent, 
	TabPane, 
	// Nav, 
	// NavItem, 
	// NavLink, 
	Card, 
	Button, 
	CardTitle, 
	CardText} from 'reactstrap';
// import classnames from 'classnames';
import CartMap from './cart-map';
import CartPriceMap from './cartPrice-map';


class CartListModal extends Component {

	constructor(props) {
		super(props);
		this.state = {
			activeTab: '1'
		};
        this.tabToggle = this.tabToggle.bind(this);

        this.props.requestCartListGet();
	}

	tabToggle(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeTab: tab
			});
		}
	}


	static getDerivedStateFromProps(nextProps) {
		if(nextProps.loginStatus) {
		
		} 
        return null;
    }

	render() {

        const cartList = this.props.cartList;

		return (
			<div>
				<Modal className='modal-lg' isOpen={this.props.modal} toggle={this.props.toggle}>
					<ModalHeader toggle={this.props.toggle}>
						<FontAwesomeIcon className='spacing-right' icon={faShoppingBag} /> 
						My Cart
					</ModalHeader>
					<ModalBody>

					{/*	<Nav className='cart-list' tabs>
							<NavItem className='tab-item pointer'>
								<NavLink
									className={classnames({ active: this.state.activeTab === '1' })}
									onClick={() => { this.tabToggle('1'); }}
								>
									Current Cart
								</NavLink>
							</NavItem>
							<NavItem className='tab-item pointer'>
								<NavLink
									className={classnames({ active: this.state.activeTab === '2' })}
									onClick={() => { this.tabToggle('2'); }}
								>
									History of Purchase
								</NavLink>
							</NavItem>
						</Nav>*/}

						<TabContent activeTab={this.state.activeTab}>
							<TabPane tabId='1'>
									{
										(this.props.wrapper.totalCart > 0) ?
											<Row>
												<CartMap 
													cartList={cartList}
													setCartQuantityPrice={this.props.setCartQuantityPrice}
												/>
												<CartPriceMap
													cartList={cartList}
													cartToggle={this.props.toggle}
													requestClickCheckOutGet={this.props.requestClickCheckOutGet}
												/>
											</Row>
										:
										<Row>
											<Col className='text-center'>
												Empty
											</Col>
										</Row>

									}
							</TabPane>
							<TabPane tabId='2'>
								<Row>
									<Col sm='6'>
										<Card body>
										<CardTitle>Special Title Treatment</CardTitle>
										<CardText>With supporting text below as a natural lead-in to additional content.</CardText>
										<Button>Go somewhere</Button>
										</Card>
									</Col>
									<Col sm='6'>
										<Card body>
										<CardTitle>Special Title Treatment</CardTitle>
										<CardText>With supporting text below as a natural lead-in to additional content.</CardText>
										<Button>Go somewhere</Button>
										</Card>
									</Col>
								</Row>
							</TabPane>
						</TabContent>

					</ModalBody>

					
				</Modal>
				
			</div>
		)
	}
}

export default CartListModal;