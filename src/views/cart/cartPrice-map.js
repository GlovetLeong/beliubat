import React, { Component } from 'react';
import {
	Row, Col, Button} from 'reactstrap';

import CheckOut from '../checkOut/checkOut';

var lodash = require('lodash');

class CartPriceMap extends Component {

	constructor(props) {
		super(props);
		this.state = {
			modalCheckOut: false
		};
		this.toggleCheckOut = this.toggleCheckOut.bind(this);
	}

	toggleCheckOut() {
		this.props.requestClickCheckOutGet();
		this.setState({
			modalCheckOut: !this.state.modalCheckOut
		});
	}

	render() {
        const cartList = lodash.get(this.props.cartList, 'result', []);

		return (
			<Col className='checkout-box' md='3'>
				<div className='checkout-header'>Items List</div>
				{
					cartList.map((cart, index) =>
						<Col className='my-2' md='12' key={index}>
							<Row>
								<Col className='checkout-name' md='7'>
									{cart.name}
								</Col>
								<Col className='checkout-price' md='5'>
									RM {cart.quantity_price}
								</Col>
							</Row>
							<Col md='12' className='line'></Col>
						</Col>
					)
				}

				<Col className='my-2' md='12'>
					<Row>
						<Col className='checkout-name' md='7'>
							Total
						</Col>
						<Col className='checkout-price' md='5'>
							RM {cartList.total_amount}
						</Col>
					</Row>
					<Col md='12' className='line'></Col>
				</Col>

				<Col className='my-2' md='12'>
					<Button color='primary' block onClick={this.toggleCheckOut}>Check Out</Button>
				</Col>

				<CheckOut 
					wrapper={this.props.wapper}
					modal={this.state.modalCheckOut}
					toggle={this.toggleCheckOut}
					cartToggle={this.props.cartToggle}
				/>
			</Col>

			
		)
	}
}

export default CartPriceMap;