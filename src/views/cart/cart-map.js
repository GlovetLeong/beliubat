import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faMinus } from '@fortawesome/free-solid-svg-icons';
import { faTrashAlt} from '@fortawesome/free-solid-svg-icons';

import {
	Row, Col} from 'reactstrap';

var lodash = require('lodash');

class CartMap extends Component {

	setQuantity(type, product_id) {
        this.props.setCartQuantityPrice(type, product_id, this.props.cartList);
    }

	render() {
        const cartList = lodash.get(this.props.cartList, 'result', []);

		return (
			<Col className='cart-list-box' md='9'>
				<Col className='cart-list-header' md='12'>
					<Row>
						<Col md='7'>
							Item
						</Col>
						<Col md='3'>
							Quantity
						</Col>
						<Col md='2'>
							Price
						</Col>
					</Row>
				</Col>

				{
					cartList.map((cart, index) =>
						<Col className='cart-list-content my-2' md='12' key={index}>
							<Row>
		       					<Col md='7'>
		       						<Row>
		       							<Col md='4' className='text-center'>
											<img className='img-fluid cart-img' alt='' src={cart.image} />
		       							</Col>
		       							<Col md='8'>
	       									<Col md='12' className='product-name-box mb-2'>
												<span className='product-name'>
													{cart.name}
												</span>
											</Col>
											<Col md='12' className='product-category mb-3'>
												<div className='category'>
													<span className='spacing-right'>{cart.type}</span>
													{
														(cart.market_price !==  cart.price) ?
				                                            <span className='product-discount'>- {cart.discount}</span>
														:
															<span></span>
													}
												</div>
											</Col>
											<Col md='12' className='line'></Col>
											<div className='mb-3'>
												<Row>
													<Col className='mb-2' md='12'>
														<div className='volume-code'>
															Volume: <span className='volume-value'>{cart.sub_uom}</span>
														</div>
													</Col>
													<Col className='mb-2' md='12'>
														<div className='volume-code'>
															Code: <span className='volume-value'>{cart.uom}</span>
														</div>
													</Col>
												</Row>
											</div>
		       							</Col>
		       						</Row>
								</Col>
								<Col className='mb-3' md='3'>
									<Row>
										<Col className='px-0 text-center' md='3' xs='3'>

											<div className='quantity-icon btn btn-primary'
												onClick={() => this.setQuantity('less', cart.product_id)}
											>
												<FontAwesomeIcon icon={faMinus} />
											</div>
										</Col>

										<Col md='6' xs='6'>
											<input name='quantity' type='text' className='quantity-txt form-control' value={cart.quantity} disabled/>
										</Col>

										<Col className='px-0 text-center' md='3' xs=''>
											<div className='quantity-icon btn btn-primary'
												onClick={() => this.setQuantity('more', cart.product_id)}
											>
												<FontAwesomeIcon icon={faPlus} />
											</div>

										</Col>
									</Row>
								</Col>
								<Col className='text-center' md='2'>
									{
										(cart.market_price !==  cart.price) ?
											<div className='market-price'>RM {cart.quantity_market_price}</div>
										:
											<div></div>
									}
									<div className='current-price'>RM {cart.quantity_price}</div>

									<div
										className='btn btn-danger'
										onClick={() => this.setQuantity('delete', cart.product_id)}
									>
										<FontAwesomeIcon icon={faTrashAlt} />
									</div>
								</Col>

							</Row>
						</Col>
					)
				}
			</Col>
		)
	}
}

export default CartMap;