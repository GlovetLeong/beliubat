import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class AddCartt extends Component {

	constructor(props) {
		super(props);
		this.addCartByDetail = this.addCartByDetail.bind(this);
	}

	addCartByDetail() {
        this.props.addCartByDetail(this.props.itemDetail);
        this.props.toggle();
        this.props.setDefault();
	}

	render() {
		return (
			<div className='add-cart btn btn-primary' onClick={this.addCartByDetail} >
				<FontAwesomeIcon icon={this.props.icon} />
			</div>
		)
	}
}

export default AddCartt;