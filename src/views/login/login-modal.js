import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignInAlt } from '@fortawesome/free-solid-svg-icons';
import {
	Row, Col,
	Button, 
	Modal, 
	ModalHeader, 
	ModalBody, 
	ModalFooter} from 'reactstrap';
import Register from '../register/register';

class LoginModal extends Component {

	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: '',
			modalRegister: false
		};
        this.handleChange = this.handleChange.bind(this);
        this.onSubmitLogin = this.onSubmitLogin.bind(this);
        this.toggleRegister = this.toggleRegister.bind(this);
	}

	handleChange(e) {
    	this.setState({ [e.target.name]: e.target.value });
    }

	onSubmitLogin() {
		const params = {
			email: this.state.email,
			password: this.state.password,
		}
		this.props.requestLoginPost(params);
	}

	toggleRegister() {
		this.setState({modalRegister: !this.state.modalRegister});
	}

	static getDerivedStateFromProps(nextProps) {
		if(nextProps.loginStatus) {
			nextProps.setStatus(0);
			nextProps.wrapper.getPatientSession();
			nextProps.wrapper.checkPatientSession();
		} 
        return null;
    }

	render() {
		return (
			<div>
				<Modal isOpen={this.props.modal} toggle={this.props.toggle}>
					<ModalHeader toggle={this.props.toggle}>
						<FontAwesomeIcon className='spacing-right' icon={faSignInAlt} /> 
						Login
					</ModalHeader>
					<ModalBody>
						<Row>
							<Col xs='12'>
								<div className="form-group">
									<input name='email' type='text' className='form-control' placeholder='Email' onChange={this.handleChange}/>
								</div>
								<div className="form-group">
									<input name='password' type='password' className='form-control' placeholder='Password' onChange={this.handleChange}/>
								</div>
							</Col>
						</Row>

					</ModalBody>
					<ModalFooter>
						<Button color='primary' block onClick={this.onSubmitLogin}>Login</Button>
					</ModalFooter>

					<div className='text-center mb-2'>
						<small>No register yet? <button className='link-button' onClick={this.toggleRegister}>create an account</button ></small>
					</div>
					
				</Modal>
				<Register 
					wrapper={this.props.wapper}
					modal={this.state.modalRegister}
					toggle={this.toggleRegister}
				/>
			</div>
		)
	}
}

export default LoginModal;