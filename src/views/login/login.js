import React, { Component } from 'react';
import LoginContainer from '../../redux/containers/loginContainer';

class Login extends Component {

	constructor(props) {
		super(props);
		this.state = {
			modal: false
		};
		this.toggle = this.toggle.bind(this);
	}

	toggle() {
		this.setState({
			modal: !this.state.modal
		});
	}

	render() {
		return (
			<div className='icon-item'>
				<span className="pointer small" onClick={this.toggle}>{this.props.text}</span>
				<LoginContainer
					wrapper={this.props.wrapper}
					modal={this.state.modal}
					toggle={this.toggle}
				/>
			</div>
		)
	}
}

export default Login;