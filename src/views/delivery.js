import React, { Component } from 'react';
import {
	Row, Col,
} from 'reactstrap';

class Sample extends Component {

	render() {
		return (
			<Row>
				<Col className='mission text-center' md='12'>
					<div className='small-title mb-3'>Delivery Policy</div>
					BeliUbat shall use reasonable endeavors to deliver Products of acceptable quality to the delivery address specified and keyed in by the 
					<br/>
					Customer but BeliUbat cannot guarantee any firm delivery time and BeliUbat shall not be liable for any delay in its delivery services, 
					<br/>
					if the delay has been due to causes beyond the control of BeliUbat.

					<br/>
					<br/>

					Your Order will be delivered to you via the service of a courier service company of our choice. 
					<br/>
					At this juncture, delivery service is extended to Malaysia only. For order placed before 12 noon, please allow <b>three (3) working days</b> for
					<br/>
					Peninsular Malaysia and <b>five (5) working days</b> for East Malaysia including Labuan for processing from date of 
					<b/>
					BeliUbat's confirmation of acceptance of an Order within the aforesaid areas unless otherwise notified by BeliUbat. 

					<br/>
					<br/>

					Please note that we will not accept delivery to a P.O. Box address and it might not be 
					<b/>
					possible for delivery to some locations within the aforesaid areas in certain circumstances. 
					<b/>
					If delivery to some locations is not possible, we may contact you to arrange for delivery to an alternative address.

					<br/>
					<br/>
					No delivery services shall be available on Sunday & public holidays.

					<br/>
					<br/>

					The Customer is to notify BeliUbat immediately of a change to his delivery address and contact number.
				</Col>
			</Row>
		)
	}
}

export default Sample;