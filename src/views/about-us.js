import React, { Component } from 'react';
import {
	Row, Col,
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faShoppingBag  } from '@fortawesome/free-solid-svg-icons';
import { faPhoneVolume  } from '@fortawesome/free-solid-svg-icons';
import { faAmbulance  } from '@fortawesome/free-solid-svg-icons';
import { faTag } from '@fortawesome/free-solid-svg-icons';
import { faSyncAlt} from '@fortawesome/free-solid-svg-icons';
import { faPrescription } from '@fortawesome/free-solid-svg-icons';
import { faQuoteLeft } from '@fortawesome/free-solid-svg-icons';

import logo_aboutUs from '../images/logo_aboutUs.png';

class AboutUs extends Component {

	render() {
		return (
			<Row>
				<Col md='12'>
					<Row>
						<Col className='text-center' md='3'>
							<img className='about-img img-fluid' alt='' src={logo_aboutUs} />
						</Col>
						<Col className='text-center' md='9'>
							A Platform Backed By A Team Of Passionate Certified Pharmacists with A Mission To Make Healthcare Virtually Effortless
						</Col>

						<Col className='mission text-center white' md='12'>
							<div className='small-title mb-3'>Our Mission</div>
							To make healthcare virtually effortless with the comfort and expert of our certified pharmacists.
							Beliubat ensures customers gain easier access to healthcare, 
							wellness and personal care products. By leveraging on today's technology, 
							benefit from our on-demand pharmacy services.
						</Col>

						<Col className='service text-center white' md='12'>
							<div className='small-title mb-3'>Our Service</div>
							<Row>
								<Col className='text-center mb-3' md='4' sm='6'>
									<div className='service-icon shop'><FontAwesomeIcon icon={faShoppingBag} /></div>
									<div>Shop</div>
								</Col>
								<Col className='text-center mb-3' md='4' sm='6'>
									<div className='service-icon telepharmacy'><FontAwesomeIcon icon={faPhoneVolume} /></div>
									<div>Telepharmacy</div>
								</Col>
								<Col className='text-center mb-3' md='4' sm='6'>
									<div className='service-icon hours'><FontAwesomeIcon icon={faAmbulance} /></div>
									<div>24 Hours Oncall</div>
								</Col>
								<Col className='text-center mb-3' md='4' sm='6'>
									<div className='service-icon hot'><FontAwesomeIcon icon={faTag} /></div>
									<div>Hot Items</div>
								</Col>
								<Col className='text-center mb-3' md='4' sm='6'>
									<div className='service-icon repeat'><FontAwesomeIcon icon={faSyncAlt} /></div>
									<div>Repeat Medical</div>
								</Col>
								<Col className='text-center mb-3' md='4' sm='6'>
									<div className='service-icon prescription'><FontAwesomeIcon icon={faPrescription} /></div>
									<div>Upload Prescription</div>
								</Col>
							</Row>	
						</Col>

						<Col className='service white' md='12'>
							<div>Dr Khoo:</div>
							<div>
								<div><FontAwesomeIcon icon={faQuoteLeft} /></div>
								With Beliubat, things are so much easier! 
								Just ordered my things online and got my items just a few hours later. 
								I could even consult a pharmacist when I'm at home
							</div>

						</Col>

					</Row>
				</Col>
			</Row>
		)
	}
}

export default AboutUs;