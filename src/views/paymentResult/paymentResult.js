import React, { Component } from 'react';
import PaymentResultContainer from '../../redux/containers/paymentResultContainer';

class PaymentResult extends Component {
	render() {
		return (
			<PaymentResultContainer
				wrapper={this.props.wrapper}
			/>
		)
	}
}

export default PaymentResult;