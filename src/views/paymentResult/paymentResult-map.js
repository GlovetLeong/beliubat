import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { Row, Col, Button } from 'reactstrap';

var lodash = require('lodash');

class Sample extends Component {

	constructor(props) {
		super(props);

        this.state = {
            proceedStatus: false,
        };
		this.props.requestPaymentResultGet();

        this.onSubmitProceedCheckOut = this.onSubmitProceedCheckOut.bind(this);
	}

    onSubmitProceedCheckOut(order_id){
        this.props.postProceedCheckOut(order_id);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.proceedStatus) {
            nextProps.setProceedStatus(0);
        }
        return null;
    }


	render() {
        const paymentResultLoading = this.props.paymentResultLoading;
		const paymentResultList = lodash.get(this.props.paymentResultList, 'result', []);
        console.log(paymentResultList);

        if (this.props.proceedStatus) {
            const order_id = lodash.get(this.props.proceedList.result, 'order_id', '');
            const payment_url = lodash.get(this.props.proceedList.result, 'payment_url', '');
            return <Redirect
            to={{
                pathname: '/proceed-pay',
                search: '?order_id=' + order_id + '&payment_url=' + encodeURIComponent(payment_url),
            }}
            />
        }

		return (

			<div className="mb-3">
                <div className='small-title mb-3'>
                    Payment Result
                </div>

                <div>
                {
                    (paymentResultLoading) ?
                        <div className='text-center'>
                            <div className="lds-ripple">
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    :

                    <div>
                        {
                            (paymentResultList > 0) ?
                                <Row>

                                    <Col className='cart-list-box' md='12'>
                                        <Col className='cart-list-header' md='12'>
                                            <Row>
                                                <Col md='6'>
                                                    Order Detail
                                                </Col>
                                                <Col md='2'>
                                                    Address
                                                </Col>
                                                <Col md='2'>
                                                    Date
                                                </Col>
                                                <Col md='1'>
                                                    Tracking ID
                                                </Col>
                                                <Col md='1'>
                                                    Status
                                                </Col>
                                            </Row>
                                        </Col>
                                        {
                                            paymentResultList.map((data, index) =>

                                                <Col className='cart-list-content white my-2 px-2' md='12' key={index}>
                                                    <Row>
                                                        <Col className='mb-3' md='6'>
                                                            <Row>
                                                                <Col md='2'>
                                                                    <div><b>ID</b>: {data.order.order_id}</div>
                                                                </Col>
                                                                <Col md='10'>
                                                                    {
                                                                        data.order_details.map((itemData, index) =>
                                                                            <div key={index}>
                                                                                <div>
                                                                                    <Row>
                                                                                        <Col md='4'>
                                                                                            <div>
                                                                                                <img className='product-img img-fluid' alt='' src={itemData.image} 
                                                                                                />
                                                                                            </div>
                                                                                        </Col>
                                                                                        <Col md='8'>
                                                                                            <div>{itemData.name}</div>
                                                                                            <div><b>Type</b>: {itemData.type}</div>
                                                                                            <div><b>Volume</b>: {itemData.sub_uom}</div>
                                                                                            <div><b>Quantity</b>: {Number(itemData.quantity)}</div>
                                                                                            <div><b>Total</b>: RM {Number(itemData.total)}</div>
                                                                                        </Col>
                                                                                    </Row>
                                                                                </div>
                                                                                <br/>
                                                                            </div>
                                                                         )
                                                                    }
                                                                    <div>
                                                                        <Row>
                                                                            <Col md='4'>
                                                                            </Col>
                                                                            <Col md='8'>
                                                                                {
                                                                                    (Number(data.order.order_tax).toFixed(2) > 0) ?
                                                                                        <div><b>Tax</b>: RM {Number(data.order.order_tax).toFixed(2)}</div>
                                                                                    :
                                                                                        <div></div>
                                                                                }
                                                                               
                                                                                <div><b>Delivery</b>: RM {Number(data.order.delivery_cost).toFixed(2)}</div>


                                                                                <div>
                                                                                    {
                                                                                        (data.order.consultant_fee) ? 
                                                                                        <div><b>Consultant Fee</b>: RM {Number(data.order.consultant_fee).toFixed(2)}</div>
                                                                                        :
                                                                                        <span></span>
                                                                                    }
                                                                                </div>
                                                                                {
                                                                                    (Number(data.order.coupon_discount_total).toFixed(2) > 0) ?
                                                                                        <div><b>Coupon</b>: -RM {Number(data.order.coupon_discount_total).toFixed(2)}</div>
                                                                                    :
                                                                                        <div></div>
                                                                                }
                                                                                <div><b>Total</b>: RM {Number(data.order.order_total_amount).toFixed(2)}</div>
                                                                            </Col>
                                                                        </Row>
                                                                    </div>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                        <Col className='mb-3' md='2'>
                                                                <div><b>Shipping</b></div>
                                                                <div>{data.order.shipping_address.address_1} {data.order.shipping_address.address_2} {data.order.shipping_address.address_postcode} {data.order.shipping_address.address_state}</div>
                                                                <br/>
                                                                <div><b>Billing</b></div>
                                                                <div>{data.order.billing_address.address_1} {data.order.billing_address.address_2} {data.order.billing_address.address_postcode} {data.order.billing_address.address_state}</div>
                                                        </Col>
                                                        <Col className='mb-3' md='2'>
                                                            <b>{data.order.created_date}</b>
                                                        </Col>
                                                        <Col className='mb-3' md='1'>
                                                            <div><b>{data.order.tracking_no ? data.order.tracking_no : '-'}</b></div>
                                                            <div><b>{data.order.tracking_option ? data.order.tracking_option : ''}</b></div>

                                                        </Col>
                                                        <Col className='text-center' md='1'>
                                                            <b>{data.order.order_status}</b>
                                                            {
                                                                (data.order.order_status === 'Pending') ?

                                                                <Button color='primary' block onClick={() => this.onSubmitProceedCheckOut(data.order.order_id)}>Pay</Button>

                                                                :
                                                                <span></span>
                                                            }
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            )
                                        }
                                    </Col>
                                </Row>
                            :
                            <Col className='text-center'>
                                Empty
                            </Col>
                        }
                    </div>
                }
                </div>
			</div>
		)
	}
}

export default Sample;